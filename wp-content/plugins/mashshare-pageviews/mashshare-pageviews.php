<?php
/**
 * Plugin Name: Mashshare - Pageviews Add-On
 * Plugin URI: https://www.mashshare.net
 * Description: The Pageviews Add-On shows you the total page impressions.
 * Author: René Hermenau
 * Author URI: https://www.mashshare.net
 * Version: 1.1.2
 * Text Domain: mashpv
 * Domain Path: languages
 * 
 *
 * @package MASHPV
 * @category Add-On
 * @author René Hermenau
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'MashsharePageviews' ) ) :
    
// Plugin version
if (!defined('MASHPV_VERSION')) {
    define('MASHPV_VERSION', '1.1.2');
}

/**
 * Main mashpv Class
 *
 * @since 1.0.0
 */
class MashsharePageviews {
	/** Singleton *************************************************************/

	/**
	 * @var MashsharePageviews The one and only MashsharePageviews
	 * @since 1.0.0
	 */
	private static $instance;
        
        
        /**
	 * MASHPV AJAX Object
	 *
	 * @var object
	 * @since 1.0.0
	 */
	//public $_ajaxflow;
	
	
	/**
	 * Main Instance
	 *
	 * Insures that only one instance of this Add-On exists in memory at any one
	 * time. Also prevents needing to define globals all over the place.
	 *
	 * @since 1.0.0
	 * @static
	 * @staticvar array $instance
	 * @uses MashsharePageviews::setup_constants() Setup the constants needed
	 * @uses MashsharePageviews::includes() Include the required files
	 * @uses MashsharePageviews::load_textdomain() load the language files
	 * @see MASHPV()
	 * @return The one true Add-On
	 */
	public static function instance() {
		if ( ! isset( self::$instance ) && ! ( self::$instance instanceof MashsharePageviews ) ) {
			self::$instance = new MashsharePageviews;
			self::$instance->setup_constants();
			self::$instance->includes();
			self::$instance->load_textdomain();
                        //self::$instance->_ajaxflow = new AjaxFlow();
                        self::$instance->hooks();
		}
		return self::$instance;
        }

	/**
	 * Throw error on object clone
	 *
	 * The whole idea of the singleton design pattern is that there is a single
	 * object therefore, we don't want the object to be cloned.
	 *
	 * @since 1.0.0
	 * @access protected
	 * @return void
	 */
	public function __clone() {
		// Cloning instances of the class is forbidden
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'MASHPV' ), '1.0.0' );
	}

	/**
	 * Disable unserializing of the class
	 *
	 * @since 1.0.0
	 * @access protected
	 * @return void
	 */
	public function __wakeup() {
		// Unserializing instances of the class is forbidden
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'MASHPV' ), '1.0.0' );
	}
        
        /**
	 * Constructor Function
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	public function __construct() {
            //self::$instance = $this;
        }
        


	/**
	 * Setup plugin constants
	 *
	 * @access private
	 * @since 1.0.0
	 * @return void
	 */
	private function setup_constants() {
		global $wpdb, $mashpv_options; 

		// Plugin Folder Path
		if ( ! defined( 'MASHPV_PLUGIN_DIR' ) ) {
			define( 'MASHPV_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
		}

		// Plugin Folder URL
		if ( ! defined( 'MASHPV_PLUGIN_URL' ) ) {
			define( 'MASHPV_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
		}

		// Plugin Root File
		if ( ! defined( 'MASHPV_PLUGIN_FILE' ) ) {
			define( 'MASHPV_PLUGIN_FILE', __FILE__ );
		}
                
	}

	/**
	 * Include required files
	 *
	 * @access private
	 * @since 1.0.0
	 * @return void
	 */
	private function includes() {
            require_once MASHPV_PLUGIN_DIR . 'includes/scripts.php';
            require_once MASHPV_PLUGIN_DIR . 'includes/mashpv-ajax.php';
            require_once MASHPV_PLUGIN_DIR . 'includes/template-functions.php';
            
		if ( is_admin() || ( defined( 'WP_CLI' ) && WP_CLI ) ) {
                        require_once MASHPV_PLUGIN_DIR . 'includes/admin/welcome.php';
                        require_once MASHPV_PLUGIN_DIR . 'includes/admin/plugins.php';
                        require_once MASHPV_PLUGIN_DIR . 'includes/admin/settings.php';
                        require_once MASHPV_PLUGIN_DIR . 'includes/admin/dashboard.php';
                        
		}
	}
        
        /**
         * Run action and filter hooks
         *
         * @access      private
         * @since       1.0.0
         * @return      void
         *
         */
        private function hooks() {

             /* Instantiate class MASHPV_licence 
             * Create 
             * @since 1.0.0
             * @return apply_filter mashsb_settings_licenses and create licence key input field in core mashsbs
             */
            if (class_exists('MASHSB_License')) {
                $mashsb_sl_license = new MASHSB_License(__FILE__, 'Mashshare Pageviews', MASHPV_VERSION, 'Rene Hermenau', 'edd_sl_license_key'); 
            }
        }

	/**
	 * Loads the plugin language files
	 *
	 * @access public
	 * @since 1.4
	 * @return void
	 */
	public function load_textdomain() {
		load_plugin_textdomain( 'mashpv', false, dirname( plugin_basename( MASHPV_PLUGIN_FILE ) ) . '/languages/' );
	}
        
        /**
	 * Activation function fires when the plugin is activated.
	 * This function is fired when the activation hook is called by WordPress.
	 *
	 * @since 1.0
	 * @access public
	 *
	 * @return void
	 */
	public static function activation() {

            // Add Upgraded From Option
            $current_version = get_option('mashpv_version');
            if ($current_version) {
                update_option('mashpv_version_upgraded_from', $current_version);
            }
            
            // current settings:
            $settings = get_option('mashsb_settings');
            
            // Add default settings on first time installation 
            // or when current version is lower than 1.0.6
            if (!$current_version || version_compare( '1.0.6', $current_version, '<=' )){
                $settings['mashpv_count_admin'] = '1';
                update_option( 'mashsb_settings', $settings ) ;
            }
            
            // Bail if activating from network, or bulk
             if ( is_network_admin() || isset( $_GET['activate-multi'] ) ) {
              return;
              } 
            // Add the current version
            update_option('mashpv_version', MASHPV_VERSION);
            // Add the transient to redirect
            set_transient('_mashpv_activation_redirect', true, 30);
        }   
}




/**
 * The main function responsible for returning the one true Add-On
 * Instance to functions everywhere.
 *
 * Use this function like you would a global variable, except without needing
 * to declare the global.
 *
 * Example: <?php $MASHPV = MASHPV(); ?>
 *
 * @since 1.0.0
 * @return object The one true MashsharePageviews Instance
 *
 * @todo        Inclusion of the activation code below isn't mandatory, but
 *              can prevent any number of errors, including fatal errors, in
 *              situations where this extension is activated but MASHSB is not
 *              present.
 */

function MASHPV() {
	if( ! class_exists( 'Mashshare' ) ) {
        if( ! class_exists( 'MASHPV_Extension_Activation' ) ) {
            require_once 'includes/class.extension-activation.php';
        }
        $activation = new MASHPV_Extension_Activation( plugin_dir_path( __FILE__ ), basename( __FILE__ ) );
        $activation = $activation->run();
        return MashsharePageviews::instance();
    } else {
        return MashsharePageviews::instance();
    }
}

/**
 * The activation hook is called outside of the singleton because WordPress doesn't
 * register the call from within the class hence, needs to be called outside and the
 * function also needs to be static.
 */

register_activation_hook( __FILE__, array( 'MashsharePageviews', 'activation' ) );




// Get MASHPV Running after other plugins loaded
add_action( 'plugins_loaded', 'MASHPV' );

endif; // End if class_exists check