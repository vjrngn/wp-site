<?php

/**
 * Template Functions
 *
 * @package     MASHPV
 * @subpackage  Functions/Templates
 * @copyright   Copyright (c) 2014, René Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0.0
 */



/* Create HTML for PageView Counter
 * Deprecated
 * 
 * @return string
 * @since 1.0
 */

function mashpvVisits ($output){
    global $mashsb_options, $post;
    isset($mashsb_options['mashpv_sharetext']) ? $sharetext = $mashsb_options['mashpv_sharetext'] : $sharetext = 'Visits';
    isset ($mashsb_options['mashpv_tooltip']) ? $mashpvTooltip = $mashsb_options['mashpv_tooltip'] : $mashpvTooltip = 'This number shows the total page impressions since publishing';
    isset($mashsb_options['mashpv_float']) ? $float = $mashsb_options['mashpv_float'] : $float = '';
    
    // Get class names for buttons size
    $class_size = isset($mashsb_options['buttons_size']) ? ' ' . $mashsb_options['buttons_size'] : '';
    
    $postid = isset($post->ID) ? $post->ID : '';

    if ($float === 'right') {
    return $output . '<div class="mashpv mashsb-count' .$class_size . '" data-mashpv-postid="'.$postid.'" data-mashtip="' . $mashpvTooltip . '"><div class="count">' . mashpv_round(mashpv_counter()) . '</div><span class="mashsb-sharetext">' . $sharetext . '</span></div>'; 
    } else {
    return '<div class="mashpv mashsb-count' .$class_size . '" data-mashpv-postid="'.$postid.'" data-mashtip="' . $mashpvTooltip . '"><div class="count">' . mashpv_round(mashpv_counter()) . '</div><span class="mashsb-sharetext">' . $sharetext . '</span></div>' . $output;  
    }
}
add_filter( 'return_networks', 'mashpvVisits', 10000 );


/* 
 * Count pageviews and stores them in post_meta
 * AJAX function
 * 
 * @since 1.0.0
 * @return void
 * 
 */

function mashpv_setPageviews($postID) {
    global $mashsb_options;
    
    // Do not count views
    if( !isset( $mashsb_options['mashpv_count_admin'] ) && current_user_can( 'install_plugins' ) ) {
        return false;
    }
    // get postid
    $postID = ( isset($_GET['id']) ) ? esc_html($_GET['id']) : false;
    
    if (!$postID){
       return false;
    }
    
    $key = 'mashpv_pageviews_count';
    $pageviews = get_post_meta($postID, $key, true);
    
    if (empty($pageviews)){
       $pageviews = 1;
    } else {
       $pageviews++;
    }
    update_post_meta($postID, $key, $pageviews);
    //wp_die($pageviews . ' ' . $postID);
    die();

}
add_action ('wp_ajax_nopriv_mashpv_set_views', 'mashpv_setPageviews');
add_action ('wp_ajax_mashpv_set_views', 'mashpv_setPageviews');

/* 
 * Get pageviews from post_meta
 * Ajax Function 
 * 
 * @since 1.0.0
 * @return integer
 * 
 */

function mashpv_getPageviews() {
    global $mashsb_options;

    $mashpv['id'] = ( isset($_GET['id']) ) ? esc_html($_GET['id']) : '';
    isset($mashsb_options['mashpv_realtime']) ? $realtime = "1" : $realtime = "0";
    !empty($mashsb_options['mashpv_cache_expire']) ? $cacheexpire = $mashsb_options['mashpv_cache_expire']  : $cacheexpire = 60;
    $count_key = 'mashpv_pageviews_count'; 
    $count = get_post_meta($mashpv['id'], $count_key, true);
    $transient = 'mashpv_cache_' . $mashpv['id'];
    
    //delete_transient('mashpv_cache_' . $mashpv['id']); //uncomment for debug
    
        mashdebug()->info("getPageviews() realtime: " . $realtime . " mashpv_highload(): " . mashpv_highload()); 
    
    if (mashpv_highload() !== true && $realtime === "1"){
        mashdebug()->info("no Highload"); 

        if ($count == '') {
            delete_post_meta($mashpv['id'], $count_key);
            add_post_meta($mashpv['id'], $count_key, '0');
        }
        $result = $count + mashpv_getFakecount($mashpv['id']);
    }
    
    /* TRANSIENT CACHE USED HERE */
    If (mashpv_highload() === true && $realtime === "1"){
        $cache = get_transient($transient);
        mashdebug()->info("Start mashpv transient - cacheexpire: " . $cacheexpire . " cache: " . $cache); 
        
        if ($cache === false) {
            mashdebug()->info("not cached in mashpv transient"); 
            //$count = get_post_meta($mashpv['id'], $count_key, true);
            $cache = set_transient( $transient, $count+1, $cacheexpire );
            $cache = get_transient($transient);
            mashdebug()->info("debug1 - cache: " . $cache . " count: " . $count); 
            $result = $cache + mashpv_getFakecount($mashpv['id']);

        } else {
            mashdebug()->info("mashpv transient exist");
            mashdebug()->info("debug2 - cache: " . $cache . " count: " . $count); 
            $result = $cache + mashpv_getFakecount($mashpv['id']);
        }
    } else {
       $result = $count + mashpv_getFakecount($mashpv['id']); 
    }
    echo $result;
die(); 
}
add_action ('wp_ajax_nopriv_mashpv_get_views', 'mashpv_getPageviews');
add_action ('wp_ajax_mashpv_get_views', 'mashpv_getPageviews');

/* Get pageviews from post_meta
 * NON AJAX function
 * 
 * @since 1.0.0
 * @return integer
 * 
 */

function mashpv_getPageviewsNonAjax() {
    global $mashsb_options, $post;
    
    if (!isset($post->ID)){
        return 0;
    }
   
    $count = (false !== ($value = get_post_meta($post->ID, 'mashpv_pageviews_count', true))) ? $value : 0 ;

//    if (empty($count)) {
//        //delete_post_meta($mashpv['id'], $count_key);
//        add_post_meta($id, $count_key, '0');
//        $count = 0;
//    }
    return $count + mashpv_getFakecount($post->ID);
}


/* 
 * Count Pagevisits and return them when ajax is disabled
 * 
 * @since 1.0.0
 * @return int
 * 
 */
//function mashpv_counter_old() {
//    global $mashsb_options, $post;
//    isset ($mashsb_options['mashpv_ajax']) ? $mashpv_ajax = '1' : $mashpv_ajax = '0';
//    mashdebug()->info("mashpv_ajax: " . $mashpv_ajax); 
//    if ($mashpv_ajax === '1') {
//        return mashpv_getPageviewsNonAjax();
//    }
//        return '&nbsp;';
//     
//}
function mashpv_counter() {
        return mashpv_getPageviewsNonAjax();     
}

/**
 * Creat a factor for calculating individual fake pageimpressions 
 * based on the number of words of a page title
 *
 * @since 1.0
 * @return int
 */
function mashpv_get_fake_factor($postid) {
    // str_word_count not working for hebraic and arabic languages
    //$wordcount = str_word_count(the_title_attribute(array('echo' => 0, 'post'=>$postid))); //Gets title to be used as a basis for the count
    $wordcount = count( explode( ' ', the_title_attribute(array('echo' => 0, 'post'=>$postid)) ) );
    $factor = $wordcount / 8;
    mashdebug()->info("wordcount: " . $wordcount);
    return apply_filters('mashpv_fake_factor', $factor);
}

/* Pagevisits fake number
 * 
 * @return int
 * @since 1.0.0
 * 
 */

function mashpv_getFakecount($postid) {
    global $mashsb_options;
    $fakecountoption = 0;
    if (isset($mashsb_options['mashpv_fake_count'])) {
        
        $fakecountoption = $mashsb_options['mashpv_fake_count'];
        $fakecount = round($fakecountoption * mashpv_get_fake_factor($postid), 0);
        mashdebug()->info("fakecount: " . $fakecount);
       return $fakecount;
       
    }
    return '';
}


/* Round the number of pagevisits
 * 2.500 results in 2.5k
 * 
 * @since 1.0.0
 * @return string
 */

function mashpv_round($number){
    global $mashsb_options;
    if (isset($mashsb_options['mashpv_round'])) {
         //$numbers = round(numbers);
             if ($number > 1000000) {
                    $rounded = round(($number / 1000000)*10)/10 . 'M';
                    return $rounded;
                }
                if ($number > 1000) {
                    $rounded = round(($number / 1000)*10)/10 . 'k';
                    return $rounded;  
                 }  
       }
     return $number;
}

/**
 * Add Custom Styles
 *
 * @since 1.0
 * 
 * @return string
 * DEPRECATED not used here
 */

function mashpv_styles_method() {
    global $mashsb_options;
    $mashpv_custom_css = '';
    /* VARS */
    isset($mashsb_options['mashpv_float']) ? $float = $mashsb_options['mashpv_float'] : $float = '';
    isset ($mashsb_options['mashpv_ajax']) ? $mashpv_ajax = true : $mashpv_ajax = false;
    
    if ($mashpv_ajax !== true) {
        $mashpv_custom_css .= '
            .mashpv .mashsb-sharetext{
             opacity: 1;
            }';
    }

    // ----------- Hook into existed 'mashpv.css' at /assets/css/mashpv.min.css -----------
        wp_add_inline_style( 'mashpv', $mashpv_custom_css );
}
//add_action( 'wp_enqueue_scripts', 'mashpv_styles_method' );



?>
