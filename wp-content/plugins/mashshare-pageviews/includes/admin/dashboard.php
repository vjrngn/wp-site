<?php
/**
 * Admin Dashboard
 *
 * @package     MASHPV
 * @subpackage  Admin/Dashboard
 * @copyright   Copyright (c) 2016, Rene Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       3.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Add new columns in posts dashboard
 * 
 * @return string
 */
function mashpv_create_views_columns($col) {
	$col['mashpv_views'] = 'Views';
	return $col;
}
add_filter('manage_posts_columns', 'mashpv_create_views_columns');

/**
 * Get views count in post columns
 * 
 * @param array $col
 * @param int $post_id
 * @retrun int
 */
function mashpv_get_views($col, $post_id) {
	if ($col == 'mashpv_views') {
		$shares = get_post_meta($post_id,'mashpv_pageviews_count',true);
		echo (int)$shares;
	}
}
add_action('manage_posts_custom_column', 'mashpv_get_views', 10, 2);
/**
 * Make share count columns sortable
 * 
 * @param array $col
 * @return string
 */
// Make the column Sortable
function mashpv_views_column_sortable( $col ) {
	$col['mashpv_views'] = 'Views';
	return $col;
}
add_filter('manage_edit-post_sortable_columns', 'mashpv_views_column_sortable');


/**
 * Change columns get_posts() query
 * 
 * @param type $query
 * @return void
 */
function mashpv_sort_views_by( $query ) {
    if( ! is_admin() ){
        return false;
    }
 
    $orderby = $query->get( 'orderby');
 
    if( 'Views' == $orderby ) {
        $query->set('meta_key','mashpv_pageviews_count');
        $query->set('orderby','meta_value_num');
    }
}
add_action( 'pre_get_posts', 'mashpv_sort_views_by' );


