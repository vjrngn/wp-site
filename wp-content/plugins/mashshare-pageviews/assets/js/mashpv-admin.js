(function ($) {
    'use strict';

    $(function () {

        if ($("#mashsb_settings\\[mashpv_realtime\\]").attr('checked')) {
            $("#mashsb_settings\\[mashpv_maxload\\]").closest('.row').css("display", "table-row");
            $("#mashsb_settings\\[mashpv_cache_expire\\]").closest('.row').css("display", "table-row");
            $("#mashsb_settings\\[mashpv_freq\\]").closest('.row').css("display", "table-row");
        } else {
            $("#mashsb_settings\\[mashpv_maxload\\]").closest('.row').css("display", "none");
            $("#mashsb_settings\\[mashpv_cache_expire\\]").closest('.row').css("display", "none");
            $("#mashsb_settings\\[mashpv_freq\\]").closest('.row').css("display", "none");
        }

        $("#mashsb_settings\\[mashpv_realtime\\]").click(function () {
            if ($(this).attr('checked')) {
                $("#mashsb_settings\\[mashpv_maxload\\]").closest('.row').css("display", "table-row");
                $("#mashsb_settings\\[mashpv_cache_expire\\]").closest('.row').css("display", "table-row");
                $("#mashsb_settings\\[mashpv_freq\\]").closest('.row').css("display", "table-row");
            } else {
                $("#mashsb_settings\\[mashpv_maxload\\]").closest('.row').css("display", "none");
                $("#mashsb_settings\\[mashpv_cache_expire\\]").closest('.row').css("display", "none");
                $("#mashsb_settings\\[mashpv_freq\\]").closest('.row').css("display", "none");
            }

        })
    });

}(jQuery));