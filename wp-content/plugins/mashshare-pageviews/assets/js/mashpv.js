jQuery(function($) {

'use strict';

var mashpv_setviews_ajax = function() {
        if (mashpv === undefined || mashpv.postID === '0') {   
            console.log ('do not count views');
            return false;
        }
            var id = mashpv.postID ? mashpv.postID : 1;
            $.ajax({
                //type: 'GET',
                method: 'get',
                data: {
                    id: id,
                    action: 'mashpv_set_views'
                },
                dataType: 'html',
                url: mashpv.ajaxurl,
                success: function(data) {
                    console.log('mashpvsetview: ' + data);
                }
            });

    };
    
var mashpv_getviews_ajax = function() {
        // Only retrieve pageviews via ajax on singular pages
        if (mashpv === undefined || mashpv.singular === '0') { 
            console.log('mashpv::no ajax');
            return false;
        }
            var id = mashpv.postID ? mashpv.postID : 1;
        
            var numItems = $('.mashpv .count').length;
            //console.log('numItems: ' + numItems);
        
            if (numItems > 1){
                var oldValue = new Array();
                $('.mashpv .count').each(function(){
                    oldValue.push($(this).text());

            });
                    oldValue = oldValue[1];
        } else {
                var oldValue = $('.mashpv .count').text();
        }
            

            $.ajax({
                //type: 'GET',
            method: 'get',
            data: {
                id: id,
                action: 'mashpv_get_views'
            },
            dataType: 'html',
            url: mashpv.ajaxurl,
                success: function(data) {
                
                console.log('mashpv::ajax get views:' + data) 
                //Create the current value
                var thisValue = mashpv_round(data);
                        //console.log ('old value:' + oldValue);
                        //console.log ('this value:' + thisValue);
                //If the old value is different to the new, change start animation
                if (String(oldValue) !== String(thisValue) && String(thisValue) !== '') {
                           $('.mashpv .count').stop(true, false).animate({'opacity': 0}, 500, function() {
                           $(this).text(mashpv_round(data)).stop(true, false).animate({'opacity': 1}, 500);
                    });
                }
                //Change the old value
                oldValue = thisValue;
            }
        });

    };
    
/* Load one time and increments the pagevisit ++1 after 2 sec delay.*/
function mashpv_init(){
     if (mashpv === 'undefined' || mashpv.postID === '0') {    
         //console.log('nothing');
         return false;
     }
        if ($('.mashpv .count').length) {
                //console.log('Count up ++1');
                //String to store the old value
                //mashpv_animateShare();
                setTimeout(mashpv_setviews_ajax, 2000);
            }

    };  


/* Different modes - Request the Pageviews .*/
function mashpv_getcount(){
     if (typeof mashpv !== undefined) {
         
            // Mode 1: Runs only when ajax is enabled
            // 
            if ($('.mashpv .count').length && mashpv.enableajax === "1") {
                //setTimeout(mashpv_getviews_ajax, 1000);
                //console.log('setTimeout: ' + mashpv.enableajax);
                mashpv_getviews_ajax();
            }
            // Mode 2: Runs only in Realtime mode
            if ($('.mashpv .count').length && mashpv.realtime === "1") {
                console.log('setInterval');
                mashpv_getviews_ajax();
                setInterval(mashpv_getviews_ajax, mashpv.ajaxfreq * 1000);
            }
     }
};  


function mashpv_round(numbers){
     if (typeof mashpv !== "undefined" && mashpv.round === "1") {
         console.log("valid");
         numbers = Math.round(numbers);
         var visits = ''
         //console.log("gerundet " + numbers);
             if (numbers > 1000000) {
                    visits = Math.round((numbers / 1000000)*10)/10 + 'M';
                    return visits;
                }
                if (numbers > 1000) {
                    visits = Math.round((numbers / 1000)*10)/10 + 'k';
                    return visits;
                    
                 }  
     }
     return numbers;
}
  
    /* Animate Shares with the same feed in effect like the Pageview counter */
    function mashpv_animateShare() {
        $('.mashsb-count .counts').stop(true, false).animate({'opacity': 0.1}, 100, function() {
            $(this).stop(true, false).animate({'opacity': 1}, 500);
            $('.mashsb-count .mashsb-sharetext').stop(true, false).animate({'opacity': 1}, 500);
        });

        $('.mashpv .count').stop(true, false).animate({'opacity': 1}, 100, function() {
            $(this).text().stop(true, false).animate({'opacity': 1}, 500);
            $('.mashpv .mashsb-sharetext').stop(true, false).animate({'opacity': 1}, 500);
        });
    }

mashpv_init();
mashpv_getcount(); 

    
    /* Tool tips */
    $('.mashpv').addClass('mashtip');
    $('.mashtip').each(function() {
        $(this).append('<span class="mashtip-content">' + $(this).attr('data-mashtip') + '</span>');
    });

    // browser.msie deprecated and removed in jQuery 1.9
    //if ($.browser.msie && $.browser.version.substr(0, 1) < 7) 
    if(navigator.appVersion.indexOf("MSIE 7.")!=-1)
    {
        $('.mashtip').mouseover(function() {
            $(this).children('.mashtip-content').css('visibility', 'visible');
        }).mouseout(function() {
            $(this).children('.mashtip-content').css('visibility', 'hidden');
        })
    }
});



