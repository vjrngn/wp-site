=== Mashshare - Pageviews Add-On===
Author: Rene Hermenau
Tags: Pageviews
Requires at least: 3.3
Tested up to: 4.7.4
Stable tag: 1.1.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Credit: This software is based on WP Open Graph by Nick Yurov https://wordpress.org/plugins/wp-open-graph

The Pageviews Add-On shows you the total visits of any post or page beside the Mashshare sharecount

== Description ==

The Pageviews Add-On shows you the total visits of any post or page beside the Mashshare sharecount


== Installation ==

1. Upload plugin folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Edit open graph data in any post
4. Enjoy!

== Frequently asked questions ==



== Screenshots ==


== Changelog ==

= 1.1.2 =
* Fix: Pageviews are wrong and are all the same on category pages.
* Fix: Prevent multiple entries of mashpv_pageviews_count  in the custom fields db table

= 1.1.1 =
* New: Add pageviews counter to admin page / posts listings
* New: Tested up to WP 4.7.2
* Fix: Show correct share count on non singular pages

= 1.1.0 =
* Fix Pageview count not shown in MASHSB 3.1.7

= 1.0.9 =
* Use new filter mashsb_share_count for pageview location

= 1.0.8 =
* New: additional class to change the size of the counter

= 1.0.7 =
* Fix: jQuery Conflict
* New: Do not count page views for logged in admin users
* New: Hide non used admin settings and make them visible when they become active

= 1.0.6 =
* Tweak: Layout of admin settings are broken after update to MashShare 3.x

= 1.0.5 =
* New: use native wp_ajax calls instead custom ajax function (more reliable) and better compatibility for sites with none default location of admin-ajax.php
* Tweak: Change some words in admin settings
* Tweak clean up code

= 1.0.4 =
* Fix: Some old styles in mashpv.css

= 1.0.3 =
Fix: Remove browser.msie - not longer supported in jQuery 1.9
Tweak: Change some wording in admin

= 1.0.2 =
Fix: custom Tooltip text not shown
Fix: HTML 5 compliance data-mashtip attribute in div

= 1.0.1 =
Fix: Typo 

= 1.0.0 Initial version. =

== Upgrade notice ==