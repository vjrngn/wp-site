<?php
/**
 * Plugin Name: Mashshare - Google Analytics Integration
 * Plugin URI: https://www.mashshare.net
 * Description: This Mashshare Add-On counts all clicks on the Mashshare Share Buttons and you can see the clicks in your Google Analytics Account
 * Author: René Hermenau
 * Author URI: https://www.mashshare.net
 * Version: 1.0.5
 * Text Domain: mashga
 * Domain Path: languages
 *
 *
 * @package MASHGA
 * @category Add-On
 * @author René Hermenau
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'MashshareGoogleAnalytics' ) ) :
    
    // Plugin version
    if (!defined('MASHGA_VERSION')) {
        define('MASHGA_VERSION', '1.0.5');
    }

    /**
 * Main mashga Class
 *
 * @since 1.0.0
 */
class MashshareGoogleAnalytics {
	/** Singleton *************************************************************/

	/**
	 * @var MashshareGoogleAnalytics The one and only MashshareGoogleAnalytics
	 * @since 2.0.0
	 */
	private static $instance;
        
        /**
	 * MASHGA HTML Element Helper Object
	 *
	 * @var object
	 * @since 2.0.0
	 */
	//public $html;
	
	
	/**
	 * Main Instance
	 *
	 * Insures that only one instance of this Add-On exists in memory at any one
	 * time. Also prevents needing to define globals all over the place.
	 *
	 * @since 1.0.0
	 * @static
	 * @staticvar array $instance
	 * @uses MashshareGoogleAnalytics::setup_constants() Setup the constants needed
	 * @uses MashshareGoogleAnalytics::includes() Include the required files
	 * @uses MashshareGoogleAnalytics::load_textdomain() load the language files
	 * @see MASHGA()
	 * @return The one true Add-On
	 */
	public static function instance() {
		if ( ! isset( self::$instance ) && ! ( self::$instance instanceof MashshareGoogleAnalytics ) ) {
			self::$instance = new MashshareGoogleAnalytics;
			self::$instance->setup_constants();
			self::$instance->includes();
			self::$instance->load_textdomain();
                        self::$instance->hooks();
		}
		return self::$instance;
        }

	/**
	 * Throw error on object clone
	 *
	 * The whole idea of the singleton design pattern is that there is a single
	 * object therefore, we don't want the object to be cloned.
	 *
	 * @since 1.0.0
	 * @access protected
	 * @return void
	 */
	public function __clone() {
		// Cloning instances of the class is forbidden
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'MASHGA' ), '1.0.0' );
	}

	/**
	 * Disable unserializing of the class
	 *
	 * @since 1.0.0
	 * @access protected
	 * @return void
	 */
	public function __wakeup() {
		// Unserializing instances of the class is forbidden
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'MASHGA' ), '1.0.0' );
	}
        
        /**
	 * Constructor Function
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	public function __construct() {
		//self::$instance = $this;
	}

	/**
	 * Setup plugin constants
	 *
	 * @access private
	 * @since 1.0.0
	 * @return void
	 */
	private function setup_constants() {
		global $wpdb, $mashga_options; 


		// Plugin Folder Path
		if ( ! defined( 'MASHGA_PLUGIN_DIR' ) ) {
			define( 'MASHGA_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
		}

		// Plugin Folder URL
		if ( ! defined( 'MASHGA_PLUGIN_URL' ) ) {
			define( 'MASHGA_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
		}

		// Plugin Root File
		if ( ! defined( 'MASHGA_PLUGIN_FILE' ) ) {
			define( 'MASHGA_PLUGIN_FILE', __FILE__ );
		}
                
                /*  Don´t need it here
                *   Plugin database
                *   Plugin Root File
		/*  if ( ! defined( 'MASHGA_TABLE' ) ) {
			define( 'MASHGA_TABLE', $wpdb->prefix.'mashsharer' );
		}*/
	}

	/**
	 * Include required files
	 *
	 * @access private
	 * @since 1.0.0
	 * @return void
	 */
	private function includes() {
            require_once MASHGA_PLUGIN_DIR . 'includes/scripts.php';

		if ( is_admin() || ( defined( 'WP_CLI' ) && WP_CLI ) ) {
                        require_once MASHGA_PLUGIN_DIR . 'includes/admin/welcome.php';
                        require_once MASHGA_PLUGIN_DIR . 'includes/admin/plugins.php';
                        require_once MASHGA_PLUGIN_DIR . 'includes/admin/settings.php';
		}
	}
        
        /**
         * Run action and filter hooks
         *
         * @access      private
         * @since       1.0.0
         * @return      void
         *
         */
        private function hooks() {

             /* Instantiate class MASHGA_licence 
             * Create 
             * @since 1.0.0
             * @return apply_filter mashsb_settings_licenses and create licence key input field in core mashsbs
             */
            if (class_exists('MASHSB_License')) {
                $mashsb_sl_license = new MASHSB_License(__FILE__, 'Google Analytics Integration', MASHGA_VERSION, 'Rene Hermenau', 'edd_sl_license_key'); 
            }
        }

	/**
	 * Loads the plugin language files
	 *
	 * @access public
	 * @since 1.4
	 * @return void
	 */
	public function load_textdomain() {
		load_plugin_textdomain( 'mashga', false, dirname( plugin_basename( MASHGA_PLUGIN_FILE ) ) . '/languages/' );
	}
        
        /**
	 * Activation function fires when the plugin is activated.
	 *
	 * This function is fired when the activation hook is called by WordPress,
	 *
	 * @since 1.0
	 * @access public
	 *
	 * @return void
	 */
	public static function activation() {

            // Add Upgraded From Option
            $current_version = get_option('mashga_version');
            if ($current_version) {
                update_option('mashga_version_upgraded_from', $current_version);
            }
            // Bail if activating from network, or bulk
             if ( is_network_admin() || isset( $_GET['activate-multi'] ) ) {
              return;
              } 
            // Add the current version
            update_option('mashga_version', MASHGA_VERSION);
            // Add the transient to redirect
            set_transient('_mashga_activation_redirect', true, 30);
        }   
}




/**
 * The main function responsible for returning the one true Add-On
 * Instance to functions everywhere.
 *
 * Use this function like you would a global variable, except without needing
 * to declare the global.
 *
 * Example: <?php $MASHGA = MASHGA(); ?>
 *
 * @since 1.0.0
 * @return object The one true MashshareGoogleAnalytics Instance
 *
 * @todo        Inclusion of the activation code below isn't mandatory, but
 *              can prevent any number of errors, including fatal errors, in
 *              situations where this extension is activated but MASHSB is not
 *              present.
 */

function MASHGA() {
	if( ! class_exists( 'Mashshare' ) ) {
        if( ! class_exists( 'MASHGA_Extension_Activation' ) ) {
            require_once 'includes/class.extension-activation.php';
        }
        $activation = new MASHGA_Extension_Activation( plugin_dir_path( __FILE__ ), basename( __FILE__ ) );
        $activation = $activation->run();
        return MashshareGoogleAnalytics::instance();
    } else {
        return MashshareGoogleAnalytics::instance();
    }
}

/**
 * The activation hook is called outside of the singleton because WordPress doesn't
 * register the call from within the class hence, needs to be called outside and the
 * function also needs to be static.
 */
register_activation_hook( __FILE__, array( 'MashshareGoogleAnalytics', 'activation' ) );

// Get MASHGA Running after other plugins loaded
add_action( 'plugins_loaded', 'MASHGA' );
//MASHGA();

endif; // End if class_exists check