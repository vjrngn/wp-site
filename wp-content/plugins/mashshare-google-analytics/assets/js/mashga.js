jQuery(document).ready(function ($) {
    if (typeof ga !== 'undefined' && typeof mashsb != 'undefined') {

        var shareurl = mashsb.share_url;
        $('.mashicon-facebook').click(function () {
            ga('send', 'event', 'Social Shares', 'Facebook', shareurl);
        });
        $('.mashicon-twitter').click(function () {
            ga('send', 'event', 'Social Shares', 'Twitter', shareurl);
        });
        $('.mashicon-subscribe').click(function () {
            ga('send', 'event', 'Social Shares', 'Subscribe', shareurl);
        });
        $('.mashicon-google').click(function () {
            ga('send', 'event', 'Social Shares', 'Google G+', shareurl);
        });
        $('.mashicon-whatsapp').click(function () {
            ga('send', 'event', 'Social Shares', 'WhatsApp', shareurl);
        });
        $('.mashicon-buffer').click(function () {
            ga('send', 'event', 'Social Shares', 'Buffer', shareurl);
        });
        $('.mashicon-pinterest').click(function () {
            ga('send', 'event', 'Social Shares', 'Pinterest', shareurl);
        });
        $('.mashicon-linkedin').click(function () {
            ga('send', 'event', 'Social Shares', 'LinkedIn', shareurl);
        });
        $('.mashicon-digg').click(function () {
            ga('send', 'event', 'Social Shares', 'Digg', shareurl);
        });
        $('.mashicon-vk').click(function () {
            ga('send', 'event', 'Social Shares', 'VK', shareurl);
        });
        $('.mashicon-print').click(function () {
            ga('send', 'event', 'Social Shares', 'Print', shareurl);
        });
        $('.mashicon-reddit').click(function () {
            ga('send', 'event', 'Social Shares', 'Reddit', shareurl);
        });
        $('.mashicon-delicious').click(function () {
            ga('send', 'event', 'Social Shares', 'Delicious', shareurl);
        });
        $('.mashicon-weibo').click(function () {
            ga('send', 'event', 'Social Shares', 'Weibo', shareurl);
        });
        $('.mashicon-pocket').click(function () {
            ga('send', 'event', 'Social Shares', 'Pocket', shareurl);
        });
        $('.mashicon-xing').click(function () {
            ga('send', 'event', 'Social Shares', 'Xing', shareurl);
        });
        $('.mashicon-odnoklassniki').click(function () {
            ga('send', 'event', 'Social Shares', 'Odnoklassniki', shareurl);
        });
        $('.mashicon-managewp').click(function () {
            ga('send', 'event', 'Social Shares', 'ManageWP', shareurl);
        });
        $('.mashicon-tumblr').click(function () {
            ga('send', 'event', 'Social Shares', 'Tumblr', shareurl);
        });
        $('.mashicon-meneame').click(function () {
            ga('send', 'event', 'Social Shares', 'Memeame', shareurl);
        });
        $('.mashicon-stumbleupon').click(function () {
            ga('send', 'event', 'Social Shares', 'Stumbleupon', shareurl);
        });
        $('.mashicon-mail').click(function () {
            ga('send', 'event', 'Social Shares', 'Mail', shareurl);
        });
        $('.mashicon-mailru').click(function () {
            ga('send', 'event', 'Social Shares', 'Mail RU', shareurl);
        });
        $('.mashicon-line').click(function () {
            ga('send', 'event', 'Social Shares', 'Line', shareurl);
        });
        $('.mashicon-yummly').click(function () {
            ga('send', 'event', 'Social Shares', 'Yummly', shareurl);
        });        
        $('.mashicon-frype').click(function () {
            ga('send', 'event', 'Social Shares', 'Frype', shareurl);
        });
        $('.mashicon-skype').click(function () {
            ga('send', 'event', 'Social Shares', 'Skype', shareurl);
        });
        $('.mashicon-telegram').click(function () {
            ga('send', 'event', 'Social Shares', 'Telegram', shareurl);
        });
        $('.mashicon-flipboard').click(function () {
            ga('send', 'event', 'Social Shares', 'Flipboard', shareurl);
        });
        $('.mashicon-hackernews').click(function () {
            ga('send', 'event', 'Social Shares', 'Hacker News', shareurl);
        });
        $('.onoffswitch').click(function () {
            ga('send', 'event', 'Social Shares', 'Mashshare Plus Button', shareurl);
        });
    } else if (typeof _gaq !== 'undefined' && typeof mashsb != 'undefined') {
        var shareurl = mashsb.share_url;
        $('.mashicon-facebook').click(function () {
            _gaq.push(['_trackEvent'], 'event', 'Social Shares', 'Facebook', shareurl);
        });
        $('.mashicon-twitter').click(function () {
            _gaq.push(['_trackEvent'], 'event', 'Social Shares', 'Twitter', shareurl);
        });
        $('.mashicon-subscribe').click(function () {
            _gaq.push(['_trackEvent'], 'event', 'Social Shares', 'Subscribe', shareurl);
        });
        $('.mashicon-google').click(function () {
            _gaq.push(['_trackEvent'], 'event', 'Social Shares', 'Google G+', shareurl);
        });
        $('.mashicon-whatsapp').click(function () {
            _gaq.push(['_trackEvent'], 'event', 'Social Shares', 'WhatsApp', shareurl);
        });
        $('.mashicon-buffer').click(function () {
            _gaq.push(['_trackEvent'], 'event', 'Social Shares', 'Buffer', shareurl);
        });
        $('.mashicon-pinterest').click(function () {
            _gaq.push(['_trackEvent'], 'event', 'Social Shares', 'Pinterest', shareurl);
        });
        $('.mashicon-linkedin').click(function () {
            _gaq.push(['_trackEvent'], 'event', 'Social Shares', 'LinkedIn', shareurl);
        });
        $('.mashicon-digg').click(function () {
            _gaq.push(['_trackEvent'], 'event', 'Social Shares', 'Digg', shareurl);
        });
        $('.mashicon-vk').click(function () {
            _gaq.push(['_trackEvent'], 'event', 'Social Shares', 'VK', shareurl);
        });
        $('.mashicon-print').click(function () {
            _gaq.push(['_trackEvent'], 'event', 'Social Shares', 'Print', shareurl);
        });
        $('.mashicon-reddit').click(function () {
            _gaq.push(['_trackEvent'], 'event', 'Social Shares', 'Reddit', shareurl);
        });
        $('.mashicon-delicious').click(function () {
            _gaq.push(['_trackEvent'], 'event', 'Social Shares', 'Delicious', shareurl);
        });
        $('.mashicon-weibo').click(function () {
            _gaq.push(['_trackEvent'], 'event', 'Social Shares', 'Weibo', shareurl);
        });
        $('.mashicon-pocket').click(function () {
            _gaq.push(['_trackEvent'], 'event', 'Social Shares', 'Pocket', shareurl);
        });
        $('.mashicon-xing').click(function () {
            _gaq.push(['_trackEvent'], 'event', 'Social Shares', 'Xing', shareurl);
        });
        $('.mashicon-odnoklassniki').click(function () {
            _gaq.push(['_trackEvent'], 'event', 'Social Shares', 'Odnoklassniki', shareurl);
        });
        $('.mashicon-managewp').click(function () {
            _gaq.push(['_trackEvent'], 'event', 'Social Shares', 'Managewp', shareurl);
        });
        $('.mashicon-tumblr').click(function () {
            _gaq.push(['_trackEvent'], 'event', 'Social Shares', 'Tumblr', shareurl);
        });
        $('.mashicon-meneame').click(function () {
            _gaq.push(['_trackEvent'], 'event', 'Social Shares', 'Meneame', shareurl);
        });
        $('.mashicon-stumbleupon').click(function () {
            _gaq.push('send', 'event', 'Social Shares', 'Stumbleupon', shareurl);
        });
        $('.mashicon-mail').click(function () {
            _gaq.push('send', 'event', 'Social Shares', 'Mail', shareurl);
        });
        $('.mashicon-mailru').click(function () {
            _gaq.push('send', 'event', 'Social Shares', 'Mail RU', shareurl);
        });
        $('.mashicon-line').click(function () {
            _gaq.push('send', 'event', 'Social Shares', 'Line', shareurl);
        });
        $('.mashicon-yummly').click(function () {
            _gaq.push('send', 'event', 'Social Shares', 'Yummly', shareurl);
        });
        $('.mashicon-frype').click(function () {
            _gaq.push('send', 'event', 'Social Shares', 'Frype', shareurl);
        });
        $('.mashicon-skype').click(function () {
            _gaq.push('send', 'event', 'Social Shares', 'Skype', shareurl);
        });
        $('.mashicon-telegram').click(function () {
            _gaq.push('send', 'event', 'Social Shares', 'Telegram', shareurl);
        });
        $('.mashicon-flipboard').click(function () {
            _gaq.push('send', 'event', 'Social Shares', 'Flipboard', shareurl);
        });
        $('.mashicon-hackernews').click(function () {
            _gaq.push('send', 'event', 'Social Shares', 'Hackernews', shareurl);
        });
        $('.onoffswitch').click(function () {
            _gaq.push('send', 'event', 'Social Shares', 'Mashshare Plus Button', shareurl);
        });
    } else if (typeof __gaTracker !== 'undefined' && typeof mashsb != 'undefined') {
        var shareurl = mashsb.share_url;
        $('.mashicon-facebook').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Facebook', shareurl);
        });
        $('.mashicon-twitter').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Twitter', shareurl);
        });
        $('.mashicon-subscribe').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Subscribe', shareurl);
        });
        $('.mashicon-google').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Google G+', shareurl);
        });
        $('.mashicon-whatsapp').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'WhatsApp', shareurl);
        });
        $('.mashicon-buffer').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Buffer', shareurl);
        });
        $('.mashicon-pinterest').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Pinterest', shareurl);
        });
        $('.mashicon-linkedin').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'LikedIn', shareurl);
        });
        $('.mashicon-digg').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Digg', shareurl);
        });
        $('.mashicon-vk').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'VK', shareurl);
        });
        $('.mashicon-print').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Print', shareurl);
        });
        $('.mashicon-reddit').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Reddit', shareurl);
        });
        $('.mashicon-delicious').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Delicious', shareurl);
        });
        $('.mashicon-weibo').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Weibo', shareurl);
        });
        $('.mashicon-pocket').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Pocket', shareurl);
        });
        $('.mashicon-xing').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Xing', shareurl);
        });
        $('.mashicon-odnoklassniki').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Odnoklassniki', shareurl);
        });
        $('.mashicon-managewp').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'ManageWP', shareurl);
        });
        $('.mashicon-tumblr').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Tumblr', shareurl);
        });
        $('.mashicon-meneame').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Memeame', shareurl);
        });
        $('.mashicon-stumbleupon').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Stumbleupon', shareurl);
        });
        $('.mashicon-mail').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Mail', shareurl);
        });
        $('.mashicon-mailru').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Mail RU', shareurl);
        });
        $('.mashicon-line').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Line', shareurl);
        });
        $('.mashicon-yummly').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Yummly', shareurl);
        });
        $('.mashicon-frype').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Frype', shareurl);
        });
        $('.mashicon-skype').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Skype', shareurl);
        });
        $('.mashicon-telegram').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Telegram', shareurl);
        });
        $('.mashicon-flipboard').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Flipboard', shareurl);
        });
        $('.mashicon-hackernews').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Hacker News', shareurl);
        });
        $('.onoffswitch').click(function () {
            __gaTracker('send', 'event', 'Social Shares', 'Mashshare Plus Button', shareurl);
        });
    }
});