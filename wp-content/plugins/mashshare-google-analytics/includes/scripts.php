<?php
/**
 * Scripts
 *
 * @package     MASHGA
 * @subpackage  Functions
 * @copyright   Copyright (c) 2014, René Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*Check if Plugin is enabled
 * 
 * @since 1.0.0
 * @return bool
 */
function mashgaGetActiveStatus(){
    global $mashsb_options;
    $status = isset( $mashsb_options['analytics_status'] ) ? $status = 1 : $status = 0;
    
    if ($status !== 1){
        return true;
    }
}

/**
 * Load Scripts
 *
 * Enqueues the required scripts.
 *
 * @since 1.0.0
 * @global $mashga_options
 * @global $post
 * @return void
 */
function mashga_load_scripts($hook) {
    
        if ( ! apply_filters( 'mashsga_load_scripts', mashgaGetActiveStatus(), $hook ) ) {
            return;
	}
    
	global $mashsb_options;
        
        $js_title = '';
            
	$js_dir = MASHGA_PLUGIN_URL . 'assets/js/';

        $js_title = 'mashga';

	// Use minified libraries if SCRIPT_DEBUG is turned off
	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';

	wp_enqueue_script( 'mashga', $js_dir . $js_title . $suffix . '.js', array( 'jquery' ), MASHGA_VERSION );             
}
add_action( 'wp_enqueue_scripts', 'mashga_load_scripts' );

/**
 * Register Styles
 *
 * Checks the styles option and hooks the required filter.
 *
 * @since 2.0.0
 * @global $mashga_options
 * @return void
 */
function mashga_register_styles() {
	global $mashga_options;

	if ( isset( $mashga_options['disable_styles'] ) ) {
		return;
	}

	// Use minified libraries if SCRIPT_DEBUG is turned off
	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
	$file          = 'mashga' . $suffix . '.css';

	$url = trailingslashit( plugins_url(). '/mashshare/templates/'    ) . $file;
	wp_enqueue_style( 'mashga-styles', $url, array(), MASHGA_VERSION );
}
//add_action( 'wp_enqueue_scripts', 'mashga_register_styles' );

/**
 * Load Admin Scripts
 *
 * Enqueues the required admin scripts.
 *
 * @since 2.0.0
 * @global $post
 * @param string $hook Page hook
 * @return void
 */

function mashga_load_admin_scripts( $hook ) {
	if ( ! apply_filters( 'mashga_load_admin_scripts', mashga_is_admin_page(), $hook ) ) {
		return;
	}
	global $wp_version;

	$js_dir  = MASHGA_PLUGIN_URL . 'assets/js/';
	$css_dir = MASHGA_PLUGIN_URL . 'assets/css/';

	// Use minified libraries if SCRIPT_DEBUG is turned off
	$suffix  = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
        //echo $css_dir . 'mashga-admin' . $suffix . '.css', MASHGA_VERSION;
	// These have to be global
	wp_enqueue_script( 'mashga-admin-scripts', $js_dir . 'mashga-admin' . $suffix . '.js', array( 'jquery' ), MASHGA_VERSION, false );
        wp_enqueue_script('jquery-ui-sortable');
	wp_enqueue_style( 'mashga-admin', $css_dir . 'mashga-admin' . $suffix . '.css', MASHGA_VERSION );
}
//add_action( 'admin_enqueue_scripts', 'mashga_load_admin_scripts', 100 );