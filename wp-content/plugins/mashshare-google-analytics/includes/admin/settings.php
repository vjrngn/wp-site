<?php

/**
 * Registers the new Mashshare Google Analytics options in Extensions
 * *
 * @access      private
 * @since       1.0
 * @param 	$settings array the existing plugin settings
 * @return      array
*/

function mashga_extension_settings( $settings ) {

	$ext_settings = array(
		array(
			'id' => 'analytics_header',
			'name' => '<strong>' . __( 'Google Analytics Settings', 'mashga' ) . '</strong>',
			'desc' => '',
			'type' => 'header',
			'size' => 'regular'
		),
		array(
			'id' => 'analytics_help',
			'name' => __( 'See Shares in Google Analytics - Instruction', 'mashga' ),
			'desc' => sprintf(__( '<a href="%s" target="blank">Follow this link</a> and learn how to see all your shares in your Google Anlaytics Dashboard', 'mashga' ), 'http://docs.mashshare.net/article/104-check-share-counts-with-google-analytics'),
			'type' => 'renderhr'
		),
		array(
			'id' => 'tracking_params',
			'name' => __( 'Create utm_source urls', 'mashga' ),
			'desc' => sprintf(__( 'This option adds utm_source parameter to the shared urls (except twitter) so you can track on google analytics how much traffic has come through the shared links. E.g. A url shared on facebook like <i style="font-weight:bold;">site.com/article-xy</i> will be converted to <i style="font-weight:bold;">site.com/article-xy?utm_source=sharebuttons&utm_medium=facebook</i>', 'mashga' ), 'http://docs.mashshare.net/article/104-check-share-counts-with-google-analytics'),
			'type' => 'checkbox'
		)
	);
	return array_merge( $settings, $ext_settings );

}
add_filter('mashsb_settings_extension', 'mashga_extension_settings');