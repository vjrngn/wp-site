=== Mashshare - Google Analytics Integration ===
Author URL: https://www.mashshare.net
Plugin URL: https://www.mashshare.net
Contributors: renehermi
Donate link: https://www.mashshare.net/buy-me-a-coffee/
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: Mashable, Share button, share buttons, Facebook Share button, Twitter Share Button, Social Share, Social buttons, Share, Google+, Twitter, Facebook, Digg, Email, Stumble Upon, Linkedin,+1, add to any, AddThis, addtoany, admin, bookmark, bookmarking, bookmarks, buffer, button, del.icio.us, Digg, e-mail, email, Facebook, facebook like, google, google plus, google plus one, icon, icons, image, images, Like, linkedin, links, lockerz, page, pages, pin, pin it, pinit, pinterest, plugin, plus 1, plus one, Post, posts, Reddit, save, seo, Share, Shareaholic, sharedaddy, sharethis, sharing, shortcode, sidebar, sociable, social, social bookmarking, social bookmarks, statistics, stats, stumbleupon, svg, technorati, tumblr, tweet, twitter, vector, widget, wpmu
Requires at least: 3.1+
Tested up to: 4.7.3
Stable tag: 1.0.5

Adds Google Analytics integration and event tracking into Mashshare for Google, Whatsapp, Pinterest, Digg, 
Linkedin, Reddit, Stumbleupon, Vk, Print, Delicious, Buffer, Weibo, Pocket, Xing, Tumblr


== Description == 

> Mashshare Share Buttons shows the total share counts of Facebook and Twitter at a glance 
It puts some beautiful and clean designed Share Buttons on top and end of your posts to get the best most possible social share feedback from your user.
It´s inspired by the Share buttons Mashable is using on his website.

<h3> Mashshare demo </h3>

[Share Buttons](http://www.mashshare.net "Share-Buttons - Mashable inspired Share Buttons")


This plugin is in active development and will be updated on a regular basis - Please do not rate negative before i tried my best to solve your issue. Thank you!

= Main features Features =

* Performance improvement for your website as no external scripts and count data is loaded
* Privacy protection for your user - No permanent connection to Facebook, Twitter and Google needed for sharing
* High-Performance caching functionality. You decide how often counts are updated.
* All counts will be collected in your database and loaded first from cache. No further database requests than.
* Up to 10.000 free daily requests
* Up to 40.000 free additional daily requests with an api key (Get it free at sharedcount.com)
* Social buttons works with every Theme
* Works on pages and posts
* Automatic embedding or manual via Shortcode into posts and pages
* Simple installation and setup
* Uninstaller: Removes all plugin tables and settings in the WP database
* Service and support by the author
* Periodic updates and improvements. (Feel free to tell me your demand)
* More Share Buttons are coming soon. 

**Shortcodes**

* Use `[mashshare]` anywhere in pages or post's text to show the buttons and total count where you like to at a custom position.
Buttons are shown exactly where you put the shortcode in.
* For manual insertion of the Share Buttons in your template files use the following php code where you want to show your Mash share buttons:`mashsharer();`
Configure the Share buttons sharing function in the settings page of the plugin.
* Change the color and font size of Mashshare directly in the css file `yourwebsite.com/wp-content/mashsharer/assets/mashsharer.css`
* With one of the next updates i will give you the possibility to change color and font-size on the settings page. So you dont have to fiddle around in css files any longer.

= How does it work? =

Mashshare Google Analytics tracks every click on one of the Mashshare Share buttons. Go into your Google Analytics Account and 
 
= How to install and setup? =
Install it via the admin dashboard and to 'Plugins', click 'Add New' and 'Upload Plugin'. Install the plugin with 'Install Now'.
After installation goto the settings page Settings->Mashshare->Extensions and do your desired changes.


== Official Site ==
* https://www.mashshare.net

== Installation ==
1. Download the Add-On "Mashshare Google Analytics" , unzip and place it in your wp-content/plugins/ folder. You can alternatively upload and install it via the WordPress plugin backend.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Select Plugins->Mashshare Google Analytics. Click 'Activate'

== Screenshots ==

1. screenshot-1.png
2. screenshot-2.png
3. screenshot-3.png
3. screenshot-4.png

== Changelog ==

= 1.0.5 =
* New: Track how much traffic the share buttons generate with utm_source tracking

= 1.0.4 =
* Fix: Change docs url

= 1.0.3 =
* New: No more manual settings. Install it, fire and forget
* New: add more networks hackernews, flipboard, telegram, skype, frype
* Fix: Change url to the docs

= 1.0.2 =
* New: Add more supported networks
* Fix: undefined variables

= 1.0.1 =
* Fix: rename extension settings array
* New: Compatible to Yoast Analytic Add-On which uses __gaTracker instead ga

= 1.0.0 = 
* First release