<?php
/**
 * Plugin Name:       MWP Diet Calories Calculator
 * Plugin URI:        http://mwp-development.com/wordpress-diet-calories-calculator/
 * Description:       MWP Diet Calories Calculator(Macro Modification) - powerful wordpress plugin which  can help you to calculate BMR, BMI, TDEE, Rest and Workout calories, waist to height, lean body mass, etc.
 * Version:           1.0.6
 * Author:            MWP Development
 * Author URI:        http://mwp-development.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       mwp-diet
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'MWP_DIET_PATH', plugin_dir_path( __FILE__ ) );
define( 'MWP_DIET_URL',     plugin_dir_url( __FILE__ ) );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-mwp-diet-activator.php
 */
function activate_mwp_diet() {
	require_once MWP_DIET_PATH . 'admin/class-mwp-diet-admin.php';
	require_once MWP_DIET_PATH . 'includes/class-mwp-diet-activator.php';
	MWP_Diet_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-mwp-diet-deactivator.php
 */
function deactivate_mwp_diet() {
	require_once MWP_DIET_PATH . 'includes/class-mwp-diet-deactivator.php';
	MWP_Diet_Deactivator::deactivate();
}

/**
 * The code that runs during plugin unistallation.
 */
function uninstall_mwp_diet() {
	require_once MWP_DIET_PATH . 'admin/class-mwp-diet-admin.php';
	$mwp_diet_options_array = MWP_Diet_Admin::get_options_array();
	
	foreach($mwp_diet_options_array as $option=>$value){
		delete_option($option);
	}
}

register_activation_hook( __FILE__, 'activate_mwp_diet' );
register_deactivation_hook( __FILE__, 'deactivate_mwp_diet' );
register_uninstall_hook( __FILE__, 'uninstall_mwp_diet' );


/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require MWP_DIET_PATH . 'includes/class-mwp-diet.php';

/**
 * Begins execution of the plugin.
 *
 * @since    1.0.0
 */
function run_mwp_diet() {

	$plugin = new MWP_Diet();
	$plugin->run();

}
run_mwp_diet();
