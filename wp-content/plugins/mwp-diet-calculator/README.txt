=== MWP Diet Calories Calculator ===
Contributors: MWP Development
Donate link: http://mwp-development.pp.ua/mwp-diet-calorie-calculator/
Tags: diet, calories, calculator, bootatrap3, responsive, bmi, bmr, tdee, weight lose, waight gain
Requires at least: 3.8
Tested up to: 4.5.3
Stable tag: 4.5.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

MWP Diet Calories Calculator - powerful wordpress plugin which  can help you to calculate BMR, BMI, TDEE,
Rest and Workout calories, waist to height, lean body mass, etc.

== Description ==
MWP Diet Calories Calculator - powerful wordpress plugin which  can help you to calculate BMR, BMI, TDEE,
Rest and Workout calories, waist to height, lean body mass, etc.

== Installation ==
1. Upload `mwp-diet-calculator` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Place shortcode [mwp_diet_calculator] in your templates or in posts, pages

== Frequently Asked Questions ==

= If bootstrap already included in your theme =

Disable plugin's bootstrap files. Go to MWP Diet -> General-> Disable Bootstrap

== Changelog ==

= 1.0.0 =
- Initial Release(08/08/2016)

= 1.0.1 =
- (25/08/2016)
- Added option to set metric/imperial system as default
- Updated shortcode enqueue scripts system

= 1.0.2 =
- (10/11/2016)
- Small fixing
- Word 'Metric' spelling

= 1.0.3 =
- (22/11/2016)
- Fixed jQuery autoclear input bugs

= 1.0.4 =
- (05/01/2017)
- Added tinymce shortcode button
- API extended
- CSS changes added

= 1.0.5 =
- (05/03/2017)
- New: Added Spanish (Spain) translation
- Fixed cm field size
- Descriptions fixed

= 1.0.6 =
- (28/04/2017)
- New: Added field Your Goal(Updated instead Rest / Workout TDEE)


