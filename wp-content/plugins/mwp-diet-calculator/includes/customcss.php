<?php
	$google_fonts       				= esc_html( get_option('mwp_diet_google_fonts', ''));
    $labels_color               		= esc_html( get_option('mwp_diet_labels_color', '') );
    $form_heding_bg            			= esc_html( get_option('mwp_diet_form_heading_bg_color', '') );
    $form_heding_color                  = esc_html( get_option('mwp_diet_form_heading_color', '') );
    $form_body_color         			= esc_html( get_option('mwp_diet_form_body_color', '') );
    $form_body_bg         				= esc_html( get_option('mwp_diet_form_body_bg_color', '') );
	$input_focus_color 					= esc_html( get_option('mwp_diet_input_focus', ''));
	$input_error_color 					= esc_html( get_option('mwp_diet_input_error', ''));
    $submit_btn_bg                    	= esc_html( get_option('mwp_diet_submit_btn_bg_color', '') );
    $submit_btn_bg_hover                = esc_html( get_option('mwp_diet_submit_btn_bg_hcolor', '') );
    $submit_btn_color                  	= esc_html( get_option('mwp_diet_submit_btn_tcolor', '') );
    $submit_btn_hcolor           		= esc_html( get_option('mwp_diet_submit_btn_thcolor', '') );
    $result_block_color              	= esc_html( get_option('mwp_diet_result_block_color', '') );
	$result_block_active_color          = esc_html( get_option('mwp_diet_result_active_color', '') );
    $result_block_attention_color       = esc_html( get_option('mwp_diet_result_attention_color', ''));
	$radio_btn_general_color 			= esc_html( get_option('mwp_diet_radio_btn_general_color', ''));
	$radio_btn_general_secondary_color 	= esc_html( get_option('mwp_diet_radio_btn_general_secondary_color', ''));
	$radio_btn_color 					= esc_html( get_option('mwp_diet_radio_btn_color', ''));
	$radio_btn_secondary_color 			= esc_html( get_option('mwp_diet_radio_btn_secondary_color', ''));
////////// Custom Colors  //////////// 

if ($google_fonts != '') {
print'.mwp-diet-form, .mwp-diet-form label, .mwp-diet-form input, .mwp-diet-form textarea, .mwp-diet-form h1, .mwp-diet-form h2, .mwp-diet-form h3, .mwp-diet-form h4, .mwp-diet-form h5, .mwp-diet-form h6,.mwp-diet-form .input-group-addon,.mwp-diet-form .mwp_result_inner_wrapper h3{font-family: ' . $google_fonts . '!important;}';        

} // end $google_fonts

if ($labels_color != '') {
print'.mwp-diet-form label {color: ' . $labels_color . '!important;}';        

} // end $labels_color

if ($form_heding_bg != '') {
print'.mwp-diet-form .panel-default > .panel-heading {background-color: ' . $form_heding_bg . '!important;}';        

} // end $form_heding_bg

if ($form_heding_color != '') {
print'.mwp-diet-form .panel-default > .panel-heading h3 {color: ' . $form_heding_color . '!important;}';        

} // end $form_heding_color

if ($form_body_color != '') {
print'.mwp-diet-form .panel {color: ' . $form_body_color . '!important;}';        

} // end $form_body_color

if ($form_body_bg != '') {
print'.mwp-diet-form .panel {background-color: ' . $form_body_bg . '!important;}';        

} // end $form_body_bg

if ($input_focus_color != '') { 
print'.mwp-diet-form .form-control:focus{border-color:' . $input_focus_color . '!important; }';       
print'.mwp-diet-form .form-control:focus + .input-group-addon {background:' . $input_focus_color . '!important; border-color:' . $input_focus_color . '!important; }';   
} // end $input_focus_color

if ($input_error_color != '') {     
print'.mwp-diet-form .form-control.mwp-diet-error{border-color:' . $input_error_color . '!important;}';  
print'.mwp-diet-form .form-control.mwp-diet-error + .input-group-addon, .mwp-diet-form .form-control.mwp-diet-error:focus + .input-group-addon {background:' . $input_error_color . '!important; border-color:' . $input_error_color . '!important;}';  
} // end $input_error_color

if ($submit_btn_bg != '') {
print'.mwp-diet-form .ladda-button {background-color: ' . $submit_btn_bg . '!important;}';        

} // end $submit_btn_bg

if ($submit_btn_bg_hover != '') {
print'.mwp-diet-form .ladda-button:hover {background-color: ' . $submit_btn_bg_hover . '!important;}';        

} // end $submit_btn_bg_hover

if ($submit_btn_color != '') {
print'.mwp-diet-form .ladda-button {color: ' . $submit_btn_color . '!important;}';        

} // end $submit_btn_color

if ($submit_btn_hcolor != '') {
print'.mwp-diet-form .ladda-button:hover {color: ' . $submit_btn_hcolor . '!important;}';        

} // end $submit_btn_hcolor

if ($result_block_color != '') { 
print'.mwp_result_inner_wrapper {color:' . $result_block_color . '!important; border-color:' . $result_block_color . '!important;}';        
print'.mwp_result_inner_wrapper h3{color:' . $result_block_color . '!important;}';        

} // end $result_block_color

if ($result_block_active_color != '') {
print'.mwp_result_inner_wrapper.result-success {color:' . $result_block_active_color . '!important; border-color:' . $result_block_active_color . '!important;}';        
print'.mwp_result_inner_wrapper.result-success h3{color:' . $result_block_active_color . '!important;}';    
} // end $result_block_active_color

if ($result_block_attention_color != '') {
print'.mwp_result_inner_wrapper.attention-result {color:' . $result_block_attention_color . '!important; border-color:' . $result_block_attention_color . '!important; }';
print'.mwp_result_inner_wrapper.attention-result h3{color:' . $result_block_attention_color . '!important;}';             

} // end $result_block_attention_color

if ($radio_btn_general_color != '') {
print'.mwp-diet-form label.mwp-radio.mwp-toggle input[type="radio"]:checked + .data-trigger {background: ' . $radio_btn_general_color . '!important; border-color: ' . $radio_btn_general_color . '!important;}';
} // end $radio_btn_general_color

if ($radio_btn_general_secondary_color != '') {
print'.mwp-diet-form label.mwp-radio.mwp-toggle input[type="radio"]:checked + .data-trigger::after {background: ' . $radio_btn_general_secondary_color . '!important;}';
print'.mwp-diet-form label.mwp-radio.mwp-toggle input[type="radio"]:checked + .data-trigger::before {color: ' . $radio_btn_general_secondary_color . '!important;}';
} // end $radio_btn_general_secondary_color

if ($radio_btn_color != '') {
print'.mwp-diet-form label.mwp-radio.mwp-base input[type="radio"]:checked + .data-trigger {border-color: ' . $radio_btn_color . '!important; background: ' . $radio_btn_color . '!important;}';    
} // end $radio_btn_color

if ($radio_btn_secondary_color != '') {
print'.mwp-diet-form label.mwp-radio.mwp-base.mwp-toggle input[type="radio"]:checked + .data-trigger::after {background: ' . $radio_btn_secondary_color . '!important;}';    
print'.mwp-diet-form label.mwp-radio.mwp-base.mwp-toggle input[type="radio"]:checked + .data-trigger::before {color: ' . $radio_btn_secondary_color . '!important;}';    
} // end $radio_btn_secondary_color
?>


