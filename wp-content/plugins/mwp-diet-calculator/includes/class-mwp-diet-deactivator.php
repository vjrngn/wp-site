<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://mwp-development.com/wordpress-diet-calories-calculator/
 * @uthor 	   MWP Development
 * @since      1.0.0
 *
 * @package    MWP_Diet
 * @subpackage MWP_Diet/includes
 */
class MWP_Diet_Deactivator {

	/**
	 * Short Description.
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {}

}
