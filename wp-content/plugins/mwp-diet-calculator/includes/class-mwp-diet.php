<?php
/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://mwp-development.com/wordpress-diet-calories-calculator/
 * @uthor 	   MWP Development
 * @since      1.0.0
 *
 * @package    MWP_Diet
 * @subpackage MWP_Diet/includes
 */

class MWP_Diet {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      MWP_Diet_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.5
	 */
	public function __construct() {

		$this->plugin_name = 'mwp-diet';
		$this->version = '1.0.6';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - MWP_Diet_Loader. Orchestrates the hooks of the plugin.
	 * - MWP_Diet_i18n. Defines internationalization functionality.
	 * - MWP_Diet_Admin. Defines all hooks for the admin area.
	 * - MWP_Diet_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {
		/**
		 * Tinymce button.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-mwp-diet-tinymce.php';

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-mwp-diet-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-mwp-diet-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-mwp-diet-admin.php';
		
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-mwp-diet-calculator.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-mwp-dc-nutritions.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-mwp-diet-public.php';

		$this->loader = new MWP_Diet_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the MWP_Diet_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new MWP_Diet_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new MWP_Diet_Admin( $this->get_plugin_name(), $this->get_version() );
		$tinymce = new MWP_Tinymce();

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'wp_head', $plugin_admin, 'mwp_diet_custom_css' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'add_mwp_diet_menu_page' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'mwp_diet_settings_init' );
		$this->loader->add_action( 'init', $tinymce, 'add_mce_button' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new MWP_Diet_Public( $this->get_plugin_name(), $this->get_version() );
		
		$this->loader->add_filter( 'mwp_dc_result_data', $plugin_public, 'add_nutrition_data_to_results' , $priority = 10, $accepted_args = 2 );
		$this->loader->add_filter( 'mwp_dc_results_meta', $plugin_public, 'rewrite_meta_array' , $priority = 10, $accepted_args = 1 );
		$this->loader->add_action('mwp_dc_script_options',  $plugin_public, 'mwp_dc_script_options_override');	
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		$this->loader->add_shortcode( "mwp_diet_calculator", $plugin_public, "mwp_diet_calculator_shortcode", $priority = 10, $accepted_args = 2 );
		$this->loader->add_action( 'wp_ajax_nopriv_wmp_diet_form_processing', $plugin_public, 'processing_diet_data'  );  
		$this->loader->add_action( 'wp_ajax_wmp_diet_form_processing', $plugin_public, 'processing_diet_data'  ); 

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    MWP_Diet_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
