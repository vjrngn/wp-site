<?php
/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://mwp-development.com/wordpress-diet-calories-calculator/
 * @uthor 	   MWP Development
 * @since      1.0.6
 *
 * @package    MWP_DC_Nutrition
 * @subpackage MWP_DC_Nutrition/includes
 */
 
class MWP_DC_Nutrition extends MWP_Diet_Calculator {
	
	public function __construct( $input_data ) {
		parent::__construct( $input_data );

	}
	
	/**
	 * Get results meta array
	 * @since    1.0.5
	 */
	public static function get_rest_nutrition_gd_meta() {
		$result_meta 			        = array();
		$result_meta['nfat']		    = array( 'title' => __('Fat per day','mwp-dccs'), 'text' 	=> esc_html( get_option('mwp_dc_nutritions_fat_popover_text', '') ) );
		$result_meta['ncarbs']		    = array( 'title' => __('Carbohydrates per day','mwp-dccs'), 'text' 		=> esc_html( get_option('mwp_dc_nutritions_carbs_popover_text', '') ) );
		$result_meta['nprotein']		= array( 'title' => __('Protein per day','mwp-dccs'), 'text' 		=> esc_html( get_option('mwp_dc_nutritions_protein_popover_text', '') ) );
		return $result_meta;
	}
	
	
	/**
	 * Retreive default data array
	 *
	 * @since    1.0.5
	 */
	 
	public function get_nutrition_defaults(){
		$result = array(
			'mwp_nfat' 				=> 0,
			'mwp_ncarbs' 			=> 0,
			'mwp_nprotein' 			=> 0,
			
			'mwp_nutrition_mass_label' 	=> __('g','mwp-dccs'),
		);
		return $result;
	}	
	
	public function get_nutrition_results(){
		$defaults = $this->get_nutrition_defaults();
		
		$result = $this->get_macro_nutrients_result_array();
		
		$result = array_merge($defaults, $result);
		return $result;
	}
	
	/**
	 * Get rest recommended calories
	 *
	 * @access  public
	 * @since  1.0.5
	 */	
	public function get_macro_nutrients_array($mfat, $mcarbs, $mprotein){
		$nutrients_array = array();
		
		if( empty($mfat) || empty($mcarbs) || empty($mprotein) )
			return $nutrients_array;
		
		$nutrients_array['mwp_nfat']     = round( (parent:: get_rest_cals() * $mfat)/ 9 );
		$nutrients_array['mwp_ncarbs']   = round( (parent:: get_rest_cals() * $mcarbs)/ 4 );
		$nutrients_array['mwp_nprotein'] = round( (parent:: get_rest_cals() * $mprotein)/ 4 );
		
		return $nutrients_array;
	}
	
	public function get_macro_nutrients_result_array() {
		switch ($this->data['mwp_macro_preference']) {
			case 1: 
					return $this->get_macro_nutrients_array(0.25, 0.4, 0.35);
					break;
				
			case 2: 
					return $this->get_macro_nutrients_array(0.1, 0.6, 0.3);
					break;
				
			case 3: 
					return $this->get_macro_nutrients_array(0.4, 0.2, 0.4);
					break;
				
			case 4: 
					return $this->get_macro_nutrients_array(0.2, 0.2, 0.6);
					break;
				
			default: 
					return $this->get_macro_nutrients_array(0, 0, 0);
					break;
			
		}
	}
	
}