<?php
/**
 * Core calculator functionality class.
 *
 * @link       http://mwp-development.com/wordpress-diet-calories-calculator/
 * @uthor 	   MWP Development
 * @since      1.0.0
 *
 * @package    MWP_Diet_Calculator
 * @subpackage MWP_Diet_Calculator/includes
 */
class MWP_Diet_Calculator {
	
	/**
	 * Data variable
	 *
	 * @since    1.0.0
	 */
	public $data;
	
	/**
	 * Rest day TDEE
	 *
	 * @since    1.0.6
	 */
	public $rest_tdee_pct;
	
	/**
	 * Workout day TDEE
	 *
	 * @since    1.0.6
	 */
	public $workout_tdee_pct;
	
	/**
	 * Construct
	 *
	 * @since    1.0.6
	 */
	public function __construct( $input_data = array() ) {
		$this->data = array_merge(self::get_default_data(), $input_data);
		$this->set_rest_workout_tdee();
	}
	
	/**
	 * Get default data
	 *
	 * @since    1.0.0
	 */
	public static function get_default_data()
	{
		$formData = array(
			'mwp_gender' 			=> 'male',
			'mwp_metric' 			=> 'imperial',
			'mwp_height_ft' 		=> 0,
			'mwp_height_in' 		=> 0,
			'mwp_weight_lbs' 		=> 0,
			'mwp_goal_weight_lbs' 	=> 0,
			'mwp_age_year' 			=> 0,
			'mwp_activity' 			=> 1.2,
			'mwp_workouts_per_week' => 0,
			'mwp_workout_tdee' 		=> 10,
			'mwp_rest_tdee' 		=> -10,
			'mwp_bodyfat' 			=> 0,
			'mwp_waist_in' 			=> 0,
			'mwp_bmr' 				=> 'mst',
			'mwp_bmr_sm' 			=> 0,
			'mwp_bmr_custom' 		=> 0,
			'mwp_tdee' 				=> 'calc',
			'mwp_tdee_sm' 			=> 0,
			'mwp_tdee_custom' 		=> 0,
			'mwp_goal_type' 		=> 4,
		);

		return $formData;
	}
	
	/**
	 * Retreive result data array
	 *
	 * @since    1.0.4
	 */
	public function get_results(){
		$result = array(
			'mwp_bmr' 			=> 0,
			'mwp_tdee' 			=> 0,
			'mwp_lbm' 			=> 0,
			'mwp_fbm' 			=> 0,
			'mwp_bmi' 			=> 0,
			'mwp_mfm' 			=> 0,
			'mwp_mrdc' 		    => 0,
			'mwp_wth'           => 0,
			'mwp_wc' 	        => 0,
			'mwp_rc' 	        => 0,
			'mwp_wtg'           => 0,
			'mwp_fw' 	        => 0,
			'mwp_bmi_class' 	=> 0,
			'mwp_mass_label' 	=> __('kg','mwp-diet'),
		);
		
		if( $this->data['mwp_height_ft'] > 0 && $this->data['mwp_weight_lbs'] > 0 && $this->data['mwp_age_year'] > 0 ){
			$result['mwp_bmr'] 				= $this->BMR();
			$result['mwp_tdee'] 			= $this->TDEE();
			$result['mwp_mrdc'] 			= $this->Mincals();
			$result['mwp_wc'] 				= $this->get_workouts_cals();
			$result['mwp_rc'] 				= $this->get_rest_cals();
			$result['mwp_wtg'] 				= $this->calcWeeksToGoal();
			$result['mwp_fw'] 				= $this->data['mwp_goal_weight_lbs']?$this->data['mwp_goal_weight_lbs']:0;
		}
		
		if( $this->data['mwp_height_ft'] > 0 && $this->data['mwp_weight_lbs'] > 0 ){
			$result['mwp_bmi'] 			 = $this->BMI();	
			$result['mwp_bmi_class'] 	 = self::obesity_class($this->BMI());
		}
		
		if( $this->data['mwp_height_ft'] > 0 && $this->data['mwp_waist_in'] > 0 ){
			$result['mwp_wth'] = $this->WTH();
		}
		
		if( $this->data['mwp_weight_lbs'] > 0 && $this->data['mwp_bodyfat'] > 0 ){
			$result['mwp_lbm'] = $this->LBM();
			$result['mwp_fbm'] = $this->FBM();			
			$result['mwp_mfm'] = $this->MFM();
		}
		
		$result['mwp_mass_label'] = $this->get_mass_label();

		return $result;
    }
	
	/**
	 * Calculating BMR
	 *
	 * @since    1.0.0
	 */
	public function BMR(){
		switch ($this->data['mwp_bmr']) {
			case "mst": {
					$mwp_bmr = round($this->calcBmrMSJ());
					break;
				}
			case "hb": {
					$mwp_bmr = round($this->calcBmrHB());
					break;
				}
			case "km": {
					$mwp_bmr = round($this->calcBmrKM());
					break;
				}
			case "cunn": {
					$mwp_bmr = round($this->calcBmrCunn());
					break;
				}	
			case "avrg": {
					$mwp_bmr = round(($this->calcBmrHB() + $this->calcBmrKM() + $this->calcBmrMSJ() + $this->calcBmrCunn()) / 4);
					break;
				}
			case "sm": {
					$mwp_bmr = $this->data['mwp_bmr_sm'] ? round($this->pounds() * $this->data['mwp_bmr_sm']) : 0;
					break;
				}
			case "custom": {
					$mwp_bmr = $this->data['mwp_bmr_custom'] ? $this->data['mwp_bmr_custom'] : 0;
					break;
				}
			default: {
					$mwp_bmr = 0;
					break;
				}
		}
		return $mwp_bmr;
    }

	/**
	 * Calculating TDEE
	 *
	 * @since    1.0.0
	 */
    public function TDEE(){
		switch ($this->data['mwp_tdee']) {
			case "calc": {
					$mwp_tdee = round($this->BMR() * $this->data['mwp_activity']);
					break;
				}
			case "sm": {
					$mwp_tdee = $this->data['mwp_tdee_sm'] ? round($this->data['mwp_weight_lbs'] * $this->data['mwp_tdee_sm']) : 0;
					break;
				}
			case "custom": {
					$mwp_tdee = $this->data['mwp_tdee_custom'] ? $this->data['mwp_tdee_custom'] : 0;
					break;
				}
			default: {
					$mwp_tdee = 0;
					break;
				}
		}
		return $mwp_tdee;
    }
	
	/**
	 * Set obesity class
	 *
	 * @since    1.0.4
	 */
	 public static function obesity_class($bmi = 0){
		if ($bmi < 16) {
			 $obesity = __('Severely Underweight','mwp-diet');
		} else if ($bmi < 18.5) {
			 $obesity = __('Underweight','mwp-diet');
		} else if ($bmi < 25.1) {
			 $obesity = __('Normal Weight','mwp-diet');
		} else if ($bmi < 30.1) {
			 $obesity = __('Overweight','mwp-diet');
		} else if ($bmi < 35.1) {
			 $obesity = __('Class I Obesity','mwp-diet');
		} else if ($bmi < 40) {
			 $obesity = __('Class II Obesity','mwp-diet');
		} else if ($bmi < 50) {
			 $obesity = __('Class III Obesity','mwp-diet');
		} else if ($bmi < 60) {
			 $obesity = __('Class IV Obesity','mwp-diet');
		}else {
			 $obesity = __('Class V Obesity','mwp-diet');
		}
		return $obesity;
    }

	/**
	 * Calculating LBM
	 *
	 * @since    1.0.0
	 */
    public function LBM(){
		return $this->data['mwp_metric'] !== 'imperial' ? round($this->lbmkg(),2) : round($this->data['mwp_weight_lbs'] - (($this->data['mwp_weight_lbs'] / 100) * $this->data['mwp_bodyfat']),2);
    }

	/**
	 * Calculating FBM
	 *
	 * @since    1.0.0
	 */
    public function FBM() {
		return $this->data['mwp_metric'] !== 'imperial' ? round(($this->kg() / 100) * $this->data['mwp_bodyfat'],2) : round(($this->data['mwp_weight_lbs'] / 100) * $this->data['mwp_bodyfat'],2);
    }
	
	/**
	 * Calculating BMI
	 *
	 * @since    1.0.0
	 */
	public function BMI(){
		$mwp_bmi = $this->pounds() / pow($this->inches(), 2) * 703;
		return round($mwp_bmi,1);
    }

	/**
	 * Calculating MFM
	 *
	 * @since    1.0.0
	 */
    public function MFM() {
		$mwp_mfm = round($this->fatpounds() * 31);
		return $mwp_mfm;
    }
	
	/**
	 * Calculating Minimum calories
	 *
	 * @since    1.0.0
	 */
	public function Mincals(){
		$mwp_mincals = round($this->TDEE() - $this->MFM());
		if($mwp_mincals < 0){
			return 0;
		}
		return $mwp_mincals;
		
    }

	/**
	 * Calculating Waist to Height
	 *
	 * @since    1.0.0
	 */
    public function WTH() {
		$mwp_waistToHeight = round(($this->waistinches() / $this->inches() * 100), 2 ) ;
		return $mwp_waistToHeight;
    }
	
	/**
	 * Calculating body fat
	 *
	 * @since    1.0.0
	 */
	public function FAT() {
		$mwp_fat = ($this->data['mwp_bodyfat'] / 100) * $this->data['mwp_weight_lbs'];
		return $mwp_fat;
    }
	
	/**
	 * Calculating workout calories
	 *
	 * @since    1.0.0
	 */
	public function get_workouts_cals() {
		$mwp_workout_cals = round($this->TDEE() * ((100 + $this->data['mwp_workout_tdee']) / 100));
		return $mwp_workout_cals;
    }
	
	/**
	 * Calculating rest calories
	 *
	 * @since    1.0.0
	 */
	public function get_rest_cals() {
		$mwp_rest_cals = round($this->TDEE() * ((100 + $this->data['mwp_rest_tdee']) / 100));
		return $mwp_rest_cals;
    }
	
	/**
	 * Calculating weeks to goal
	 *
	 * @since    1.0.0
	 */
	public function calcWeeksToGoal() {
		$tempLose = 0.00;
		$weeks = 0;
		$mwp_cycleTEE = round(7 * $this->TDEE());
		$mwp_cycleCals = (7 - $this->data['mwp_workouts_per_week']) * $this->get_rest_cals() + ($this->data['mwp_workouts_per_week'] * $this->get_workouts_cals());
		$mwp_cycleDiff = $mwp_cycleCals - $mwp_cycleTEE;
		$mwp_cycleChange = $this->data['mwp_metric'] !== 'imperial' ? round(($mwp_cycleDiff / 3500 / 2.2),2) : round(($mwp_cycleDiff / 3500),2);
		$varCycleChange = $mwp_cycleChange;
		if($this->data['mwp_goal_weight_lbs'] > 0 && $this->data['mwp_weight_lbs'] > 0 && abs($mwp_cycleChange) > 0){
			$mwp_goal_value = $this->data['mwp_goal_weight_lbs'] - $this->data['mwp_weight_lbs'];
			while (abs($tempLose) < abs($mwp_goal_value)) {
				$weeks++;
				$tempLose = $tempLose + $varCycleChange;
			}
		}
		return $weeks;
    }
	
	/**
	 * Calculating Mifflin-St Jeor
	 *
	 * @since    1.0.0
	 */
	public function calcBmrMSJ() {
		if ($this->data['mwp_gender'] === 'male') {
			return 9.99 * $this->kg() + 6.25 * $this->cm() - 4.92 * $this->data['mwp_age_year'] + 5;
		}
		return 9.99 * $this->kg() + 6.25 * $this->cm() - 4.92 * $this->data['mwp_age_year'] - 161;
    }
	
	/**
	 * Calculating Harris-Benedict
	 *
	 * @since    1.0.0
	 */
	public function calcBmrHB(){
		if ($this->data['mwp_gender'] === 'male') {
			return 66 + 6.23 * $this->pounds() + 12.7 * $this->inches() - 6.8 * $this->data['mwp_age_year'];
		}
		return 655 + 4.35 * $this->pounds() + 4.7 * $this->inches() - 4.7 * $this->data['mwp_age_year'];
    }

	/**
	 * Calculating Katch-Macardle
	 *
	 * @since    1.0.0
	 */
    public function calcBmrKM() {
		return 370 + 21.6 * $this->lbmkg();
    }
	
	/**
	 * Calculating Cunningham
	 *
	 * @since    1.0.0
	 */
	public function calcBmrCunn() {
		return 500 + 22 * $this->lbmkg();
    }
	
	/**
	 * Calculating pounds
	 *
	 * @since    1.0.0
	 */
	public function pounds() {
		return $this->data['mwp_metric'] !== 'imperial' ? round($this->data['mwp_weight_lbs'] * 2.2) : $this->data['mwp_weight_lbs'];
	}

	/**
	 * Calculating fatpounds
	 *
	 * @since    1.0.0
	 */
	public function fatpounds() {
		return $this->data['mwp_metric']  !== 'imperial' ? ($this->FAT() * 2.2) : $this->FAT();
	}

	/**
	 * Calculating weight kg
	 *
	 * @since    1.0.0
	 */
	public function kg() {
		return $this->data['mwp_metric']  !== 'imperial' ? $this->data['mwp_weight_lbs'] : ($this->data['mwp_weight_lbs'] / 2.2);
	}

	/**
	 * Calculating height cm
	 *
	 * @since    1.0.0
	 */
	public function cm() {
		return $this->data['mwp_metric']  !== 'imperial' ? $this->data['mwp_height_ft'] : (($this->data['mwp_height_ft'] * 12 + $this->data['mwp_height_in'] * 1) * 2.54);
	}

	/**
	 * Calculating height inches
	 *
	 * @since    1.0.0
	 */
	public function inches() {
		return $this->data['mwp_metric']  !== 'imperial' ? ($this->data['mwp_height_ft'] / 2.54) : ($this->data['mwp_height_ft'] * 12 + $this->data['mwp_height_in'] * 1);
	}

	/**
	 * Calculating waist inches
	 *
	 * @since    1.0.0
	 */
	public function waistinches() {
		return $this->data['mwp_metric']  !== 'imperial' ? ($this->data['mwp_waist_in'] / 2.54) : $this->data['mwp_waist_in'];
	}

	/**
	 * Calculating lbm kg
	 *
	 * @since    1.0.0
	 */
	public function lbmkg() {
		return $this->kg() - (($this->kg() / 100) * $this->data['mwp_bodyfat']);
	}
	
	/**
	 * Get Mass label
	 *
	 * @since    1.0.0
	 */
    public function get_mass_label(){
		return $this->data['mwp_metric'] !== 'imperial' ? __('kg','mwp-diet') : __('lbs','mwp-diet');
    }
	
	/**
	 * Set day split
	 *
	 * @access  public
	 * @since  1.0.6
	 */
	public function set_tdee_split($rest = 0, $workout = 0) {
		$this->rest_tdee_pct 		= $rest;
		$this->workout_tdee_pct 	= $workout;
	}	
	
	/**
	 * Set rest and workout TDEE
	 *
	 * @access  public
	 * @since  1.0.6
	 */
	public function set_rest_workout_tdee() {
		if($this->data['mwp_goal_type'] != 7){
			$this->day_split();
			$this->data['mwp_rest_tdee'] = $this->rest_tdee_pct;
			$this->data['mwp_workout_tdee'] = $this->workout_tdee_pct;
		}
	}
	
	/**
	 * Day goal split
	 *
	 * @access  public
	 * @since  1.0.6
	 */
	public function day_split() {
		switch ($this->data['mwp_goal_type']) {
			case 4: 
					$this->set_tdee_split(0, 0);
					break;
				
			case 2: 
					$this->set_tdee_split(-20, -20);
					break;
				
			case 1: 
					$this->set_tdee_split(-25, -25);
					break;
				
			case 3: 
					$this->set_tdee_split(-15, -15);
					break;
				
			case 5: 
					$this->set_tdee_split(10, 10);
					break;	
					
			case 6: 
					$this->set_tdee_split(15, 15);
					break;
				
			default: 
					$this->set_tdee_split(0, 0);
					break;
			
		}
		return;
	}
	
}
