<?php

/**
 * Fired during plugin activation
 *
 * @link       http://mwp-development.com/wordpress-diet-calories-calculator/
 * @uthor 	   MWP Development
 * @since      1.0.0
 *
 * @package    MWP_Diet
 * @subpackage MWP_Diet/includes
 */
class MWP_Diet_Activator {

	/**
	 * Plugin activator
	 *
	 * @since    1.0.6
	 */
	public static function activate() {

		$mwp_diet_options_array = MWP_Diet_Admin::get_options_array();
		foreach($mwp_diet_options_array as $option=>$value){
			add_option($option,$value);
		}
		
		update_option( 'mwp-diet-plugin-version', '1.0.6' );
		 
	}

}
