<?php
 /**
 * Shortcode TinyMCE plugin class
 *
 * @link       http://mwp-development.com/wordpress-diet-calories-calculator/
 * @uthor 	   MWP Development
 * @since      1.0.4
 *
 * @package    MWP_Diet
 * @subpackage MWP_Diet/includes
 */
 
class MWP_Tinymce {
	
	public function __construct() {}
	
	/**
	 * Adding shortcode button to visual editor
	 *
	 * @since    1.0.4  
	 */
	public function add_mce_button() {
        if ( ! current_user_can( 'edit_posts' ) && ! current_user_can( 'edit_pages' ) ) {
            return;
        }
        if ( get_user_option( 'rich_editing' ) !== 'true' ) {
            return;
        }
        add_filter( 'mce_external_plugins', array( $this, 'add_buttons') );
        add_filter( 'mce_buttons', array( $this, 'register_buttons') );
    }
	
	/**
	 * Add button.
	 *
	 * @since    1.0.4  
	 */
	public function add_buttons( $plugin_array ) {
        $plugin_array['mwp_diet_calculator'] = MWP_DIET_URL .'admin/js/mwp-diet-tinymce.js';
        return $plugin_array;
    }
	
	/**
	 * Register button.
	 *
	 * @since    1.0.4  
	 */
	public function register_buttons( $buttons ) {
        array_push( $buttons, 'mwp_diet_calculator' );
        return $buttons;
    }
}
