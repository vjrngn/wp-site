(function( $ ) {
	'use strict';
	
	$.fn.mwpDietCalculator = function(options) {	
	    var $container  		= $('#mwp-diet-calculator-form-' + options.form_id);
		var ajaxurl     		=  options.ajaxurl; 
		var defaults = {
				default_system: '',
				disable_popover: '',
				bootstrap_select: '',
				all_sections: '',
				result_sections: {
					bmr		: '', 
					tdee	: '', 
					lbm		: '', 
					fbm		: '', 
					bmi		: '', 
					wth		: '', 
					mfm		: '', 
					mrdc	: '', 
					wc		: '',
					rc		: '',
					wtg		: '', 
					fw		: '',	
					obesity	: ''
				},
				required_field_msg: 'The field is required!',
				empty_field_msg: 'Please fill in this field!',
				before_result_processing: function (data, $container) {},
				after_result_processing: function (data, $container) {},
			 };
		var settings = $.extend( true, {}, defaults, options );
		
		var default_system 		=  settings.default_system; 						
		var disable_popover  	=  settings.disable_popover;
		var bootstrap_select    =  settings.bootstrap_select; 
		
		var mwp_input_height 			= $('input[name="mwp_height_ft"]', $container),
			mwp_input_weight 			= $('input[name="mwp_weight_lbs"]', $container),
			mwp_input_age 				= $('input[name="mwp_age_year"]', $container),
			mwp_input_bfat 				= $('input[name="mwp_bodyfat"]', $container),
			mwp_input_waist				= $('input[name="mwp_waist_in"]', $container),
			mwp_default_bmr 			= $('input[name="mwp_bmr"]:checked', $container),
			mwp_default_tdee			= $('input[name="mwp_tdee"]:checked', $container);
			
		if( settings.all_sections || settings.result_sections.bmr || settings.result_sections.tdee || settings.result_sections.mrdc || settings.result_sections.wc || settings.result_sections.rc || settings.result_sections.fw || settings.result_sections.wtg ){	
			mwp_input_height.prop('required',true); 
			mwp_input_weight.prop('required',true);  
			mwp_input_age.prop('required',true); 
		}
		if(settings.result_sections.lbm || settings.result_sections.fbm || settings.result_sections.mfm){
			mwp_input_weight.prop('required',true);  
			mwp_input_bfat.prop('required',true);	
		}
		if(settings.result_sections.wth){
			mwp_input_height.prop('required',true); 
			mwp_input_waist.prop('required',true);
		}
		if(settings.result_sections.bmi || settings.result_sections.obesity){
			mwp_input_height.prop('required',true); 
			mwp_input_weight.prop('required',true);
		}
			
		// bootstrap popover
		if(!disable_popover){
			$('[data-toggle="popover"]',$container).popover();
		}
		
		// bootstrap select
		if(bootstrap_select !== 'enabled'){
			if(jQuery.fn.selectpicker !== undefined) {
				$('#mwp_activity, #mwp_goal_type, #mwp_macro_preference',$container).selectpicker();
				if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
					$('#mwp_activity, #mwp_goal_type, #mwp_macro_preference').selectpicker('mobile');
				}
			}
		}
		
		clearForm();
		
		$("#mwp-clear-diet-form", $container).click(function(){
			clearForm();
		});
		
		autoclear_inputs();
		
		if(default_system !== 'enabled'){
			$("#mwp_gender_male, #mwp_metric_imp, #mwp_tdee_radio_calc", $container).prop('checked', true);
		} else{
			$("#mwp_gender_male, #mwp_metric_std, #mwp_tdee_radio_calc", $container).prop('checked', true);
		}
		
		metric_system_handle_labels();
				
		//submit form
		$container.submit(function(e){
			e.preventDefault();
			var mwp_weight 				= $('input[name="mwp_weight_lbs"]', $container),
				mwp_age 				= $('input[name="mwp_age_year"]', $container),
				mwp_height 				= $('input[name="mwp_height_ft"]', $container),
				mwp_bmr_method 			= $('input:radio[name="mwp_bmr"]:checked', $container).val(),
				mwp_tdee_method 		= $('input:radio[name="mwp_tdee"]:checked', $container).val(),
				mwp_bmr_radio_sm 		= $('input[name="mwp_bmr_sm"]', $container),
				mwp_bmr_radio_custom 	= $('input[name="mwp_bmr_custom"]', $container),
				mwp_tdee_radio_sm 		= $('input[name="mwp_tdee_sm"]', $container),
				mwp_tdee_radio_custom 	= $('input[name="mwp_tdee_custom"]', $container);		
			
			$('input', $container).removeClass('mwp-diet-error');
			$('.mwp-error-message', $container).html('');
			
		if( check_required_fields() ){				
				if(mwp_bmr_method == 'sm' && mwp_bmr_radio_sm.val() <= 0){
					mwp_bmr_radio_sm.addClass('mwp-diet-error');
					$('#mwp_bmr_radio_sm-error', $container).html(settings.empty_field_msg);
					return false;
				}else if(mwp_bmr_method == 'custom' && mwp_bmr_radio_custom.val() <= 0){
					mwp_bmr_radio_custom.addClass('mwp-diet-error');
					$('#mwp_bmr_radio_custom-error', $container).html(settings.empty_field_msg);
					return false;
				}else if(mwp_tdee_method == 'sm' && mwp_tdee_radio_sm.val() <= 0){
					mwp_tdee_radio_sm.addClass('mwp-diet-error');
					$('#mwp_tdee_radio_sm-error', $container).html(settings.empty_field_msg);
					return false;
				}else if(mwp_tdee_method == 'custom' && mwp_tdee_radio_custom.val() <= 0){
					mwp_tdee_radio_custom.addClass('mwp-diet-error');
					$('#mwp_tdee_radio_custom-error', $container).html(settings.empty_field_msg);
					return false;
				}else{
					mwp_get_diet_results($(this));
				}
			}else{
				higlight_required_fields();
				$('body, html').animate({scrollTop:$container.offset().top}, 'slow');
			}				
			return false;
		});
		
		if ($('input[name="mwp_bodyfat"]', $container).val() > 0) {
			$("#mwp_bmr_radio_km", $container).prop('checked', true);
		} else {
			mwp_default_bmr.prop('checked', true);
		}
		
		$('input:radio[name="mwp_metric"]', $container).change(function () {
			metric_system_handler();
			metric_system_handle_labels();
		});
		
		$('input[name="mwp_workouts_per_week"]', $container).change(function () {
			select_picker_handler();
		});
		
		$('input[name="mwp_bodyfat"]', $container).change(function () {
			handle_bodyfat();
		});
		
		$('select[name="mwp_activity"]', $container).change(function () {
			change_workout_day();
		});		
		
		$('select[name="mwp_goal_type"]', $container).change(function () {
			handle_custom_tdee();
		});
		
		$('input[name*="mwp_weight_lbs"], input[name*="mwp_goal_weight_lbs"]', $container).change(function () {
			handle_rest_workout_tdee();
		});
		
		$('input[name*="mwp_"], select[name*="mwp_"]', $container).change(function () {
			handle_bmr_method();
			handle_tdee_method();
			handle_bmr_disabled();
		});
		
			function mwp_get_diet_results (thisObj){
				var  nonce       =  $('#mwp-diet-nonce', $container).val();
				var l = Ladda.create( document.querySelector('#mwp-submit-diet-form',$container));
				// Start loading
				l.start();

				$.ajax({
					type: 'POST', 
					url: ajaxurl,
					data: {
						'action'                 : 'wmp_diet_form_processing',
						'mwp_diet_nonce'         : nonce,
						'mwp_diet_data'			 : thisObj.serialize()
					},
					success:function(result) {
						l.stop(); 
						if(result.status == 'success'){
							settings.before_result_processing(result.data, $container);
							mwp_diet_result_processing(result.data);
							settings.after_result_processing(result.data, $container);
							$('html, body').animate({
								scrollTop: $('.mwp-diet-results-holder', $container).offset().top
							}, 1500);
							
						} else{

						}
					},
					error: function(errorThrown){
					}
				 });
			}
			
			function mwp_diet_result_processing(data) {	
								
				var mwp_bodyfat 	= parseInt($('#mwp_bodyfat', $container).val());
				var mwp_goal 		= $('input[name="mwp_goal_weight_lbs"]', $container);
				var mwp_bmr 		= $('#mwp_bmr_result', $container),
					mwp_tdee 		= $('#mwp_tdee_result', $container),
					mwp_lbm 		= $('#mwp_lbm_result', $container),
					mwp_fbm 		= $('#mwp_fbm_result', $container),
					mwp_bmi 		= $('#mwp_bmi_result', $container),
					mwp_bmi_class 	= $('#mwp_obesity_result', $container),
					mwp_mfm 		= $('#mwp_mfm_result', $container),
					mwp_mrdc 		= $('#mwp_mrdc_result', $container),
					mwp_whr 		= $('#mwp_wth_result', $container),
					mwp_wtg 		= $('#mwp_wtg_result', $container),
					mwp_rest_cals 	= $('#mwp_rc_result', $container),
					mwp_workout_cals = $('#mwp_wc_result', $container),
					mwp_final_weight = $('#mwp_fw_result', $container),
					mwp_cals_label 	= ' cals';
					
					$('.mwp_result_inner_wrapper', $container).removeClass('result-success');
					
					if (data.mwp_bmr) {
						mwp_bmr.parent().addClass('result-success');
						mwp_bmr.html(data.mwp_bmr + mwp_cals_label);
					}else{
						mwp_bmr.html('--');
					}
					
					if (data.mwp_tdee) {
						mwp_tdee.parent().addClass('result-success');
						mwp_tdee.html(data.mwp_tdee + mwp_cals_label);
					}else{
						mwp_tdee.html('--');
					}
					if (data.mwp_lbm && mwp_bodyfat > 0) {
						mwp_lbm.parent().addClass('result-success');
						mwp_lbm.html(data.mwp_lbm + ' ' + data.mwp_mass_label);
					}else{
						mwp_lbm.html('--');
					}
					
					if (data.mwp_fbm && mwp_bodyfat > 0) {
						mwp_fbm.parent().addClass('result-success');
						mwp_fbm.html(data.mwp_fbm + ' ' + data.mwp_mass_label);
					}else{
						mwp_fbm.html('--');
					}
					
					if (data.mwp_bmi) {
						mwp_bmi.parent().addClass('result-success');
						mwp_bmi.html(data.mwp_bmi);
					}else{
						mwp_bmi.html('--');
					}
					
					if (data.mwp_bmi_class) {
						mwp_bmi_class.parent().addClass('result-success');
						mwp_bmi_class.html(data.mwp_bmi_class);
					}else{
						mwp_bmi_class.html('--');
					}
					
					if (data.mwp_mfm !== 0 && mwp_bodyfat > 0) {
						mwp_mfm.parent().addClass('result-success');
						mwp_mfm.html(data.mwp_mfm + mwp_cals_label);
					} else{
						mwp_mfm.html('--');
					}
					if (data.mwp_mrdc !== 0 && mwp_bodyfat > 0) {
						mwp_mrdc.parent().addClass('result-success');
						mwp_mrdc.html(data.mwp_mrdc + mwp_cals_label);
					}else{
						mwp_mrdc.html('--');
					}
					
					if (data.mwp_wth !== 0) {
						mwp_whr.parent().addClass('result-success');
						mwp_whr.html(data.mwp_wth + ' %');
					}else{
						mwp_whr.html('--');
					}
					
					if (data.mwp_wc) {
						mwp_workout_cals.parent().addClass('result-success');
						mwp_workout_cals.html(data.mwp_wc + mwp_cals_label);
					}else{
						mwp_workout_cals.html('--');
					}
					
					if (data.mwp_rc) {
						mwp_rest_cals.parent().addClass('result-success');
						mwp_rest_cals.html(data.mwp_rc + mwp_cals_label);
					}else{
						mwp_rest_cals.html('--');
					}
					
					if (data.mwp_wtg) {
						mwp_wtg.parent().addClass('result-success');
						mwp_wtg.html(data.mwp_wtg);
					}else{
						mwp_wtg.html('--');
					}
					
					if (data.mwp_fw) {
						mwp_final_weight.parent().addClass('result-success');
						mwp_final_weight.html(mwp_goal.val()+ ' ' + data.mwp_mass_label);
					}else{
						mwp_final_weight.html('--');
					}
					if (mwp_bodyfat > 0 && (settings.all_sections || settings.result_sections.mrdc || settings.result_sections.wc || settings.result_sections.rc || settings.result_sections.fw || settings.result_sections.wtg)) {
						if(Math.abs(data.mwp_mincals) > Math.abs(data.mwp_rest_cals)){
							mwp_mrdc.parent().addClass('result-error');
							mwp_rest_cals.parent().addClass('result-error');
							alert('Your Minimum Recommended Daily Calories are lower then Rest Calories');
						}else{
							mwp_mrdc.parent().removeClass('result-error');
							mwp_rest_cals.parent().removeClass('result-error');
							
						}
					}
						
				
			}
			
			function metric_system_handle_labels(){
				var mwp_metric_system = $('input:radio[name="mwp_metric"]:checked', $container).val();
				if (mwp_metric_system === 'imperial') { //imperial
					$('.mwp_height_in', $container).show();
					$('.mwp_height_ft', $container).removeClass( 'mwp-pr-0' );
					$('#mwp_height_ft', $container).attr("placeholder", "ft");
					$('#mwp_waist_in', $container).attr("placeholder", "in");
					$('#mwp_weight_lbs,#mwp_goal_weight_lbs', $container).attr("placeholder", "lbs");
					
					$('#addon_mwp_height_ft', $container).text("ft");
					$('#addon_mwp_waist_in', $container).text("in");
					$('#addon_mwp_weight_lbs', $container).text("lbs");
					$('#addon_mwp_goal_weight_lbs', $container).text("lbs");

				} else { //metric					
					$('.mwp_height_in', $container).hide();
					$('.mwp_height_ft', $container).addClass( 'mwp-pr-0' );
					$('#mwp_height_ft, #mwp_waist_in', $container).attr("placeholder", "cm");
					$('#mwp_weight_lbs,#mwp_goal_weight_lbs', $container).attr("placeholder", "kg");
					
					$('#addon_mwp_waist_in', $container).text("cm");
					$('#addon_mwp_height_ft', $container).text("cm");
					$('#addon_mwp_waist_in', $container).text("cm");
					$('#addon_mwp_weight_lbs', $container).text("kg");
					$('#addon_mwp_goal_weight_lbs', $container).text("kg");
				}
			}
			
			function metric_system_handler(){
				var mwp_metric_system = $('input:radio[name="mwp_metric"]:checked', $container).val(),
					mwp_height_in = $('input[name="mwp_height_in"]', $container),
					mwp_height_ft = $('input[name="mwp_height_ft"]', $container),
					mwp_waist_in = $('input[name="mwp_waist_in"]', $container),
					mwp_weight_lbs = $('input[name="mwp_weight_lbs"]', $container),
					mwp_goal_weight_lbs = $('input[name="mwp_goal_weight_lbs"]', $container),
					h = 0,
					hin = 0,
					hmod = 0,
					w = 0,
					gw = 0,
					ws = 0;
					
				if (mwp_metric_system === 'imperial') { //imperial					
					hmod = mwp_height_ft.val() / 2.54;
					h = Math.floor(hmod / 12);
					hin = Math.round(hmod - (h * 12));
					h !== 0 ? mwp_height_ft.val(h) : mwp_height_ft.val(0);
					hin !== 0 ? mwp_height_in.val(hin) : mwp_height_in.val(0);

					w = Math.round(mwp_weight_lbs.val() * 2.2 * 100) / 100;
					w !== 0 ? mwp_weight_lbs.val(w) : mwp_weight_lbs.val(0);
					
					gw = Math.round(mwp_goal_weight_lbs.val() * 2.2 * 100) / 100;
					gw !== 0 ? mwp_goal_weight_lbs.val(gw) : mwp_goal_weight_lbs.val(0);

					ws = Math.round(mwp_waist_in.val() / 2.54 * 100) / 100;
					ws !== 0 ? mwp_waist_in.val(ws) : mwp_waist_in.val(0);

				} else { //metric
					hin = parseInt(mwp_height_in.val()) ? parseInt(mwp_height_in.val()) : 0;
					h = Math.round((mwp_height_ft.val() * 12 + hin) * 2.54 * 10) / 10;
					h !== 0 ? mwp_height_ft.val(h) : mwp_height_ft.val(0);

					w = Math.round(mwp_weight_lbs.val() / 2.2 * 100) / 100;
					w !== 0 ? mwp_weight_lbs.val(w) : mwp_weight_lbs.val(0);

					gw = Math.round(mwp_goal_weight_lbs.val() / 2.2 * 100) / 100;
					gw !== 0 ? mwp_goal_weight_lbs.val(gw) : mwp_goal_weight_lbs.val(0);

					ws = Math.round(mwp_waist_in.val() * 2.54 * 100) / 100;
					ws !== 0 ? mwp_waist_in.val(ws) : mwp_waist_in.val(0);
				}
			}
			
			function change_workout_day() {
				var activity = $('select[name="mwp_activity"]', $container).val();
				switch (activity) {
					case '1.2': {
							$('#mwp_workouts_per_week', $container).val(0);
							break;
						}
					case '1.375': {
							$('#mwp_workouts_per_week', $container).val(2);
							break;
						}
					case '1.55': {
							$('#mwp_workouts_per_week', $container).val(4);
							break;
						}
					case '1.725': {
							$('#mwp_workouts_per_week', $container).val(6);
							break;
					}
					case '1.9': {
							$('#mwp_workouts_per_week', $container).val(7);
							break;
					}	
					default: {
							$('#mwp_workouts_per_week', $container).val(0);
							break;
					}
				}
				return activity;
			}
			
			function handle_bmr_method(){
				var mwp_bmr_method = $('input:radio[name="mwp_bmr"]:checked', $container).val(),
					mwp_bmr_sm  = $('.mwp_bmr_sm', $container),
					mwp_bmr_custom  = $('.mwp_bmr_custom', $container),
					mwp_bmr_sm_error  = $('#mwp_bmr_radio_sm-error', $container),
					mwp_bmr_custom_error  = $('#mwp_bmr_radio_custom-error', $container);;
				
				if (mwp_bmr_method === 'sm') {
					mwp_bmr_sm.show();
					mwp_bmr_sm_error.show();
					mwp_bmr_custom.hide();
					mwp_bmr_custom_error.hide();

				} else if (mwp_bmr_method === 'custom') {
					mwp_bmr_sm.hide();
					mwp_bmr_sm_error.hide();
					mwp_bmr_custom.show();
					mwp_bmr_custom_error.show();

				} else {
					mwp_bmr_sm.hide();
					mwp_bmr_sm_error.hide();
					mwp_bmr_custom.hide();
					mwp_bmr_custom_error.hide();

				}
			}
			
			function handle_tdee_method(){	
				var mwp_tdee_method = $('input:radio[name="mwp_tdee"]:checked', $container).val(),
					mwp_tdee_sm = $('.mwp_tdee_sm', $container),
					mwp_tdee_custom = $('.mwp_tdee_custom', $container),
					mwp_tdee_sm_error = $('#mwp_tdee_radio_sm-error', $container),
					mwp_tdee_custom_error = $('#mwp_tdee_radio_custom-error', $container);
					
				if (mwp_tdee_method === 'sm') {
					mwp_tdee_sm.show();
					mwp_tdee_sm_error.show();
					mwp_tdee_custom.hide();
					mwp_tdee_custom_error.hide();

				} else if (mwp_tdee_method === 'custom') {
					mwp_tdee_sm.hide();
					mwp_tdee_sm_error.hide();
					mwp_tdee_custom.show();
					mwp_tdee_custom_error.show();

				} else {
					mwp_tdee_sm.hide();
					mwp_tdee_sm_error.hide();
					mwp_tdee_custom.hide();
					mwp_tdee_custom_error.hide();

				}
			}
			
			function handle_rest_workout_tdee(){	
				var weight    = parseInt($('input[name="mwp_weight_lbs"]', $container).val()), 
				goal_weight   = parseInt($('input[name="mwp_goal_weight_lbs"]', $container).val()),
				mwp_goal_type = $('select[name="mwp_goal_type"]', $container);
				
				if( mwp_goal_type.val() != 7 ){	
					if(weight > goal_weight && weight > 0 && goal_weight > 0){
						mwp_goal_type.val(2);
						if(bootstrap_select !== 'enabled'){
							mwp_goal_type.selectpicker('render');
						}
					}else if(weight < goal_weight && weight > 0 && goal_weight > 0){
						mwp_goal_type.val(5);
					}else{
						mwp_goal_type.val(4);
					}
					if(bootstrap_select !== 'enabled'){
						mwp_goal_type.selectpicker('render');
					}
				}
			}
			
			function handle_custom_tdee(){	
				var mwp_goal_type_val   = parseInt($('select[name="mwp_goal_type"]', $container).val()), 
				custom_tdee_container   = $('.custom_rest_workout_tdee', $container);
					
				if(mwp_goal_type_val == 7){
					custom_tdee_container.show();
				}else{
					custom_tdee_container.hide();
				}
				
			}
			
			function handle_bodyfat(){
				if ($('input[name="mwp_bodyfat"]', $container).val() > 0) {
					$('#mwp_bmr_radio_km', $container).prop('checked', true);
				} else {
					mwp_default_bmr.prop('checked', true);
				}
			}
			
			function handle_bmr_disabled(){
				if ($('input[name="mwp_bodyfat"]', $container).val() > 0) {
					$('#mwp_bmr_radio_km, #mwp_bmr_radio_avrg,#mwp_bmr_radio_cunn', $container).attr('disabled', false);
				} else {
					$('#mwp_bmr_radio_km, #mwp_bmr_radio_avrg,#mwp_bmr_radio_cunn', $container).attr('disabled', true);
				}
			}
			
			function select_picker_handler(){
				var mwp_wpw = $('input[name="mwp_workouts_per_week"]', $container).val();
				if(mwp_wpw == 0){
					set_select_picker_value(1.2);
				} else if(mwp_wpw > 0 && mwp_wpw < 4){
					set_select_picker_value(1.375);
				}else if(mwp_wpw > 3 && mwp_wpw < 6){
					set_select_picker_value(1.55);
				}else if(mwp_wpw > 5 && mwp_wpw < 7){
					set_select_picker_value(1.725);
				}else if(mwp_wpw == 7){
					set_select_picker_value(1.9);
				}else{
					set_select_picker_value(1.2);
				}
				
			}
			
			function set_select_picker_value(value){
				if(value){
					if(bootstrap_select !== 'enabled'){
						$('#mwp_activity', $container).selectpicker('val', value);
					}else{
						$('#mwp_activity', $container).val(value);
					}
				}
			}
			
			function check_required_fields(){
				var no_errors = true;
				$($container.selector + ' input[required]').each(function(index, elem){
					if( !($(elem).val() > 0) ){
						no_errors = false;
					}
				});	
				return no_errors;
			}
			
			function higlight_required_fields(){
				$($container.selector + ' input[required]').each(function(index, elem){
					if($(elem).val() <= 0){
						$(elem).addClass('mwp-diet-error');
						$('#'+elem.name+'-error', $container).html(settings.required_field_msg);
					}
				});	
			}
			
			function autoclear_inputs() {
				var defaultText;
				$($container.selector + ' input[type=number]').focus(function() {
					defaultText = $(this).val();
					$(this).val('');
				});
				$($container.selector + ' input[type=number]').blur(function() {
					if($(this).val() == ''){
						$(this).val(defaultText); 
					}
				 });
			}	
			
			function clearForm() {
				$(':input', $container).each(function() {
				  var type = this.type;
				  var tag = this.tagName.toLowerCase();
				 if ((type == 'text' || type == 'number') && type !== 'hidden'){
					this.value = this.defaultValue;
				 }else if (type == 'checkbox' || type == 'radio'){
					this.checked = false;
				  }else if (tag == 'select'){
					this.selectedIndex = 0;
				  }
				});
				$('input', $container).removeClass('mwp-diet-error');
				$('.mwp-error-message', $container).html('');
				$('#mwp_gender_male', $container).prop('checked', true);
				
				if(default_system !== 'enabled'){
					$("#mwp_metric_imp", $container).prop('checked', true);
				} else{
					$("#mwp_metric_std", $container).prop('checked', true);
				}
				
				mwp_default_bmr.prop('checked', true);
				mwp_default_tdee.prop('checked', true);
				handle_bmr_method();
				handle_tdee_method();
				handle_bmr_disabled();
				metric_system_handler();
				metric_system_handle_labels();
				handle_rest_workout_tdee();
				handle_custom_tdee();
				if(bootstrap_select !== 'enabled'){
					if(jQuery.fn.selectpicker !== undefined) {
						$('#mwp_activity',$container).selectpicker('refresh');
						$('#mwp_goal_type',$container).selectpicker('refresh');
						$('#mwp_macro_preference',$container).selectpicker('refresh');
					}
				}
			};
		
	}
	

})( jQuery );