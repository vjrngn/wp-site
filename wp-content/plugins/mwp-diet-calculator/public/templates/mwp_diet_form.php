<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
	$form_heading_text      		= esc_html( get_option('mwp_diet_form_heading_text', ''));
    $submit_btn_text            	= esc_html( get_option('mwp_diet_submit_btn_text', '') ); 
	$metric_system_default 			= esc_html( get_option('mwp_diet_metric_default', '') );
	$section_all 					= esc_html( get_option('mwp_diet_section_all', '') );

	if( $section_all ){ // show all sections
		$result_meta  				= $results_meta;
	}else{
		$result_meta 				= array_intersect_key($results_meta,$result_sections);
	}

?>
<form action="" class="form" id="mwp-diet-calculator-form-<?php echo $counter;?>" class="clearfix" method="POST">
	<?php do_action('mwp_dc_form_inner_top');?>
	<div id="mwp-diet-form-body" class="mwp-diet-form form-horizontal">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-title">
					<h2><?php echo $form_heading_text;?></h2>
				</div>
			</div><!-- /.panel-heading -->	
			<div class="panel-body clearfix">
				<div class="row">
					<div class="col-md-6 col-sm-6">
					<?php if($section['wth'] || $section['bmr'] || $section['tdee'] || $section['nfat'] || $section['ncarbs'] || $section['nprotein']  || $section['rc'] || $section['wc'] || $section['mrdc'] || $section['wtg'] || $section['fw'] || $section['bmi'] || $section['obesity'] || $section_all  ): ?>
						<div class="form-group mwp-first-group">
							<label for="mwp_height_ft" class="col-sm-4 control-label"><?php _e('Height','mwp-diet');?></label>
							<div class="col-sm-4 mwp_height_ft">
								<div class="input-group">
								  <input type="number" min="0" step="0.01" class="form-control" tabindex="0" name="mwp_height_ft" id="mwp_height_ft" placeholder="<?php _e('ft','mwp-diet');?>" value="<?php echo $formData['mwp_height_ft']; ?>">
								  <span id="addon_mwp_height_ft" class="input-group-addon"><?php _e('ft','mwp-diet');?></span>
								</div>
								<div id="mwp_height_ft-error" class="mwp-error-message"></div>
							</div>
							<div class="col-sm-4 mwp_height_in">
								<div class="input-group">
								   <input type="number" min="0" step="0.01" class="form-control" tabindex="1" name="mwp_height_in" id="mwp_height_in" placeholder="<?php _e('in','mwp-diet');?>" value="<?php echo $formData['mwp_height_in']; ?>">
								  <span id="addon_mwp_height_in" class="input-group-addon"><?php _e('in','mwp-diet');?></span>
								</div>
							</div>
						</div><!-- /.form-group -->	
						<?php endif; ?>
						<?php if($section['lbm'] || $section['fbm'] || $section['bmr'] || $section['tdee'] || $section['nfat'] || $section['ncarbs'] || $section['nprotein'] || $section['rc'] || $section['wc'] || $section['mrdc'] || $section['wtg'] || $section['fw']  || $section['bmi'] || $section['obesity'] || $section['mfm'] || $section_all ): ?>	
						<div class="form-group">
							<label for="mwp_weight_lbs" class="col-sm-4 control-label"><?php _e('Weight','mwp-diet');?></label>
							<div class="col-sm-8 mwp_weight_lbs">
								<div class="input-group">
									<input type="number" min="0" step="0.01" class="form-control" tabindex="2" name="mwp_weight_lbs" id="mwp_weight_lbs" placeholder="<?php _e('lbs','mwp-diet');?>" value="<?php echo $formData['mwp_weight_lbs']; ?>">
									<span id="addon_mwp_weight_lbs" class="input-group-addon"><?php _e('lbs','mwp-diet');?></span>
								</div>
								<div id="mwp_weight_lbs-error" class="mwp-error-message"></div>
							</div>
						</div><!-- /.form-group -->	
						<?php endif; ?>
						<?php if($section['wtg'] || $section['fw'] || $section_all ): ?>	
						<div class="form-group">
							<label for="mwp_goal_weight_lbs" class="col-sm-4 control-label"><?php _e('Goal Weight','mwp-diet');?></label>
							<div class="col-sm-8 mwp_goal_weight_lbs">
								<div class="input-group">
									<input type="number" min="0" step="0.01" class="form-control" tabindex="3" name="mwp_goal_weight_lbs" id="mwp_goal_weight_lbs" placeholder="<?php _e('lbs','mwp-diet');?>" value="<?php echo $formData['mwp_weight_lbs']; ?>">
									<span id="addon_mwp_goal_weight_lbs" class="input-group-addon"><?php _e('lbs','mwp-diet');?></span>
								</div>
							</div>
						</div><!-- /.form-group -->	
						<?php endif; ?>
						<?php if($section['bmr'] || $section['tdee'] || $section['nfat'] || $section['ncarbs'] || $section['nprotein'] || $section['rc'] || $section['wc'] || $section['mrdc'] || $section['wtg'] || $section['fw'] || $section_all ): ?>	
						<div class="form-group">
								<label for="mwp_age_year" class="col-sm-4 control-label"><?php _e('Age','mwp-diet');?></label>
								<div class="col-sm-8">
									<div class="input-group">
										<input type="number" min="0" step="1" class="form-control" tabindex="4" name="mwp_age_year" id="mwp_age_year" placeholder="<?php _e('years','mwp-diet');?>" value="<?php echo $formData['mwp_age_year']; ?>">
										<span id="addon_mwp_age_year" class="input-group-addon"><?php _e('yrs','mwp-diet');?></span>
									</div>
									<div id="mwp_age_year-error" class="mwp-error-message"></div>
								</div>
						</div><!-- /.form-group -->	
						<?php endif; ?>
						<?php if($section['tdee'] || $section['nfat'] || $section['ncarbs'] || $section['nprotein'] || $section['mrdc'] || $section['rc'] || $section['wc'] || $section['wtg'] || $section['fw'] || $section_all ): ?>	
						<div class="form-group">
							<label for="mwp_activity" class="col-sm-4 control-label"><?php _e('Activity','mwp-diet');?></label>
							<div class="col-sm-8">
								<select name="mwp_activity" id="mwp_activity" tabindex="5" class="form-control">
								  <option value="1.2" <?php echo ($formData['mwp_activity']==1.2)?'selected':''; ?>><?php _e('Sedentary','mwp-diet');?></option>
								  <option value="1.375" <?php echo ($formData['mwp_activity']==1.375)?'selected':''; ?>><?php _e('Lightly active / sports 1 – 3 days/week','mwp-diet');?></option>
								  <option value="1.55" <?php echo ($formData['mwp_activity']==1.55)?'selected':''; ?>><?php _e('Moderately active / sports 3 – 5 days/week','mwp-diet');?></option>
								  <option value="1.725" <?php echo ($formData['mwp_activity']==1.725)?'selected':''; ?>><?php _e('Very active / sports 6 – 7 days/week','mwp-diet');?></option>
								  <option value="1.9" <?php echo ($formData['mwp_activity']==1.9)?'selected':''; ?>> <?php _e('Extra active / training 2 times/day','mwp-diet');?></option>
								</select>
							</div>
						</div><!-- /.form-group -->
						<?php endif; ?>
						<?php if($section['wtg'] || $section['fw'] || $section_all ): ?>							
						<div class="form-group">
							<label for="mwp_workouts_per_week" class="col-sm-4 control-label"><?php _e('Workouts per wk.','mwp-diet');?></label>
							<div class="col-sm-8">
								<div class="input-group">
									 <input type="number" class="form-control" tabindex="6" name="mwp_workouts_per_week" id="mwp_workouts_per_week" min="0" max="7" step="1" placeholder="<?php _e('days','mwp-diet');?>" value="<?php echo $formData['mwp_workouts_per_week']; ?>">
									<span id="addon_mwp_workouts_per_week" class="input-group-addon"><?php _e('days','mwp-diet');?></span>
								</div>
							</div>
						</div><!-- /.form-group -->
						<?php endif; ?>
						<?php if($section['wc'] || $section['wtg'] || $section['rc'] || $section['fw'] || $section_all ): ?>							
						<div class="form-group">
							<label for="mwp_goal_type" class="col-sm-4 control-label"><?php _e('Your Goal','mwp-diet');?></label>
							<div class="col-sm-8">
								<select name="mwp_goal_type" id="mwp_goal_type" tabindex="5" class="form-control">
								  <option value="1" <?php echo ($formData['mwp_goal_type']==1)?'selected':''; ?>><?php _e('Aggressive weight loss (-25% deficit)','mwp-diet');?></option>
								  <option value="2" <?php echo ($formData['mwp_goal_type']==2)?'selected':''; ?>><?php _e('Fast weight loss (-20% deficit)','mwp-diet');?></option>
								  <option value="3" <?php echo ($formData['mwp_goal_type']==3)?'selected':''; ?>><?php _e('Recommended weight loss (-15% deficit)','mwp-diet');?></option>
								  <option value="4" <?php echo ($formData['mwp_goal_type']==4)?'selected':''; ?>><?php _e('Maintain','mwp-diet');?></option>
								  <option value="5" <?php echo ($formData['mwp_goal_type']==5)?'selected':''; ?>><?php _e('Lean gain (10% surplus)','mwp-diet');?></option>
								  <option value="6" <?php echo ($formData['mwp_goal_type']==6)?'selected':''; ?>> <?php _e('Bulk (15% surplus)','mwp-diet');?></option>
								  <option value="7" <?php echo ($formData['mwp_goal_type']==7)?'selected':''; ?>> <?php _e('Custom','mwp-diet');?></option>
								</select>
							</div>
						</div><!-- /.form-group -->	
						<div class="form-group custom_rest_workout_tdee">
							<label for="mwp_rest_tdee" class="col-sm-4 control-label"><?php _e('Rest / Workout TDEE','mwp-diet');?></label>
							<div class="col-sm-4 mwp_rest_tdee">
								<div class="input-group">
								  <input type="number" class="form-control" name="mwp_rest_tdee" tabindex="8" id="mwp_rest_tdee" min="-100" max="100" step="1" placeholder="<?php _e('%','mwp-diet');?>" value="<?php echo $formData['mwp_rest_tdee']; ?>">
									<span id="addon_mwp_rest_tdee" class="input-group-addon"><?php _e('%','mwp-diet');?></span>
								</div>
							</div>
							<div class="col-sm-4 mwp_workout_tdee">
								<div class="input-group">
									<input type="number" class="form-control" tabindex="7" name="mwp_workout_tdee" id="mwp_workout_tdee" min="-100" max="100" step="1" placeholder="<?php _e('%','mwp-diet');?>" value="<?php echo $formData['mwp_workout_tdee']; ?>">
									<span id="addon_mwp_workout_tdee" class="input-group-addon"><?php _e('%','mwp-diet');?></span>
								</div>
							</div>
						</div><!-- /.form-group -->	
						<?php endif; ?>	
						<?php if( $section['nfat'] || $section['ncarbs'] || $section['nprotein'] || $section_all  ): ?>
						<div class="form-group">
							<label for="mwp_macro_preference" class="col-sm-4 control-label"><?php _e('Macro Preferences','mwp-diet');?></label>
							<div class="col-sm-8">
								<select name="mwp_macro_preference" id="mwp_macro_preference" tabindex="8" class="form-control">
								  <option value="1" selected ><?php _e('Balanced','mwp-diet');?></option>
								  <option value="2" ><?php _e('Carb Lover','mwp-diet');?></option>
								  <option value="3" ><?php _e('High Fat','mwp-diet');?></option>
								  <option value="4" ><?php _e('High Protein','mwp-diet');?></option>
								</select>
							</div>
						</div><!-- /.form-group -->	
						<?php endif; ?>	
						<?php if($section['bmr'] || $section['tdee'] || $section['nfat'] || $section['ncarbs'] || $section['nprotein'] || $section['lbm'] || $section['fbm'] || 	$section['mfm'] || $section['mrdc'] || $section['wtg'] || $section['fw'] || $section['rc'] || $section['wc'] || $section_all ): ?>						
						<div class="form-group">
							<label for="mwp_bodyfat" class="col-sm-4 control-label"><?php _e('Bodyfat','mwp-diet');?></label>
							<div class="col-sm-8">
								<div class="input-group">
									<input type="number" min="0" step="0.01" class="form-control" tabindex="9" name="mwp_bodyfat" id="mwp_bodyfat" placeholder="<?php _e('%','mwp-diet');?>" value="<?php echo $formData['mwp_bodyfat']; ?>">
									<span id="addon_mwp_bodyfat" class="input-group-addon"><?php _e('%','mwp-diet');?></span>
								</div>
								<div id="mwp_bodyfat-error" class="mwp-error-message"></div>
							</div>
						</div><!-- /.form-group -->	
						<?php endif; ?>	
						<?php if($section['wth'] || $section_all ): ?>
						<div class="form-group">
							<label for="mwp_weistin" class="col-sm-4 control-label"><?php _e('Waist','mwp-diet');?></label>
							<div class="col-sm-8 mwp_waist_in">
								<div class="input-group">
									<input type="number" min="0" step="0.01" class="form-control" tabindex="10" name="mwp_waist_in" id="mwp_waist_in" placeholder="<?php _e('in','mwp-diet');?>" value="<?php echo $formData['mwp_waist_in']; ?>">
									<span id="addon_mwp_waist_in" class="input-group-addon"><?php _e('in','mwp-diet');?></span>
								</div>
								<div id="mwp_waist_in-error" class="mwp-error-message"></div>
							</div>				
						</div><!-- /.form-group -->	
						<?php endif; ?>			
					</div><!-- /.col-md-6 -->
					<div class="col-md-6 col-sm-6">
						<div class="general-section mwp-select-method-section">
							<?php if($section['bmr'] || $section['tdee'] || $section['nfat'] || $section['ncarbs'] || $section['nprotein'] || $section['rc'] || $section['wc'] || $section['wtg'] || $section['fw'] || $section['mrdc'] || $section_all ): ?>	
							<div class="row">
								<div class="col-sm-10">
									<div class="col-sm-6 mwp-no-padding">
									  <div class="radio-input">
									   <label class="mwp-radio mwp-toggle">
											<input type="radio" name="mwp_gender" id="mwp_gender_male" tabindex="11" class="mwp-switcher" value="male" <?php echo ($formData['mwp_gender']=='male')?'checked':''; ?>>
											<span class="data-trigger" data-on="ON" data-off="OFF"></span>
										</label>
											<label for="mwp_gender_male">
												<?php _e('Male','mwp-diet');?>
											</label>
										</div>
									</div>
									<div class="col-sm-6 mwp-no-padding">
										<div class="radio-input">
											<label class="mwp-radio mwp-toggle">
												<input type="radio" name="mwp_gender" id="mwp_gender_female" tabindex="12" class="mwp-switcher"  value="female" <?php echo ($formData['mwp_gender']=='female')?'checked':''; ?>>
												<span class="data-trigger" data-on="ON" data-off="OFF"></span>
											</label>
											<label for="mwp_gender_female">
												<?php _e('Female','mwp-diet');?>
											</label>
										</div>
									</div>
								</div>
							</div><!-- /.row -->
							<?php endif; ?>								
							<div class="row">
								<div class="col-sm-10">
									<?php if($metric_system_default !== 'enabled' ): ?>
										<div class="col-sm-6 mwp-no-padding">
										  <div class="radio-input">
											  <label class="mwp-radio mwp-toggle">
													<input type="radio" name="mwp_metric" id="mwp_metric_imp" tabindex="13" value="imperial" <?php echo ($formData['mwp_metric']=='imperial')?'checked':''; ?>>
													<span class="data-trigger" data-on="ON" data-off="OFF"></span>
												</label>
												<label for="mwp_metric_imp">
													<?php _e('Imperial','mwp-diet');?>
												</label>
											</div>
										</div><!-- /.col-sm-6 -->
										<div class="col-sm-6 mwp-no-padding">
											<div class="radio-input">
												<label class="mwp-radio mwp-toggle">
													<input type="radio" name="mwp_metric" id="mwp_metric_std" tabindex="14" value="std" <?php echo ($formData['mwp_metric']=='std')?'checked':''; ?>>
													<span class="data-trigger" data-on="ON" data-off="OFF"></span>
												</label>
												<label for="mwp_metric_std">
													<?php _e('Metric','mwp-diet');?>
												</label>
											</div>
										</div><!-- /.col-sm-6 -->
									<?php else: ?>
										<div class="col-sm-6 mwp-no-padding">
											<div class="radio-input">
												<label class="mwp-radio mwp-toggle">
													<input type="radio" name="mwp_metric" id="mwp_metric_std" tabindex="14" value="std" <?php echo ($formData['mwp_metric']=='std')?'checked':''; ?>>
													<span class="data-trigger" data-on="ON" data-off="OFF"></span>
												</label>
												<label for="mwp_metric_std">
													<?php _e('Metric','mwp-diet');?>
												</label>
											</div>
										</div><!-- /.col-sm-6 -->
										<div class="col-sm-6 mwp-no-padding">
										  <div class="radio-input">
											  <label class="mwp-radio mwp-toggle">
													<input type="radio" name="mwp_metric" id="mwp_metric_imp" tabindex="13" value="imperial" <?php echo ($formData['mwp_metric']=='imperial')?'checked':''; ?>>
													<span class="data-trigger" data-on="ON" data-off="OFF"></span>
												</label>
												<label for="mwp_metric_imp">
													<?php _e('Imperial','mwp-diet');?>
												</label>
											</div>
										</div><!-- /.col-sm-6 -->
									<?php endif; ?>
								</div>
							</div><!-- /.row -->	
						</div>
						<?php if($section['bmr'] || $section['tdee'] || $section['nfat'] || $section['ncarbs'] || $section['nprotein'] || $section['rc'] || $section['mrdc'] || $section['wtg'] || $section['fw'] || $section['wc'] || $section_all  ): ?>	
						<div class="bmr-section mwp-select-method-section">
							<h3><?php _e('Basal Metabolic Rate','mwp-diet');?></h3>					
								<div class="radio-input">
									<label class="mwp-radio mwp-toggle mwp-base">
										<input type="radio" name="mwp_bmr" class="form-control" tabindex="15" id="mwp_bmr_radio_mst" value="mst" <?php echo ($formData['mwp_bmr']=='mst')?'checked':''; ?>>
										<span class="data-trigger" data-on="ON" data-off="OFF"></span>
									</label>
									<label for="mwp_bmr_radio_mst">
										<?php _e('Mifflin-St Jeor formula','mwp-diet');?>
									</label>
								</div>	
			
								<div class="radio-input">
									<label class="mwp-radio mwp-toggle mwp-base">
										<input type="radio" name="mwp_bmr" class="form-control" tabindex="16" id="mwp_bmr_radio_hb" value="hb" <?php echo ($formData['mwp_bmr']=='hb')?'checked':''; ?>>
										<span class="data-trigger" data-on="ON" data-off="OFF"></span>
									</label>
									<label for="mwp_bmr_radio_hb">
										<?php _e('Harris-Benedict formula','mwp-diet');?>
									</label>
								</div><!-- /.radio -->					
								<div class="radio-input">
									<label class="mwp-radio mwp-toggle mwp-base">
										<input type="radio" name="mwp_bmr" class="form-control" tabindex="17" id="mwp_bmr_radio_km" value="km" disabled="disabled" <?php echo ($formData['mwp_bmr']=='km')?'checked':''; ?>>
										<span class="data-trigger" data-on="ON" data-off="OFF"></span>
									</label>
									<label for="mwp_bmr_radio_km">
										<?php _e('Katch-Macardle formula','mwp-diet');?>
									</label>
								</div><!-- /.radio -->		
								<div class="radio-input">
									<label class="mwp-radio mwp-toggle mwp-base">
										<input type="radio" name="mwp_bmr" class="form-control" tabindex="18" id="mwp_bmr_radio_cunn" value="cunn" disabled="disabled" <?php echo ($formData['mwp_bmr']=='cunn')?'checked':''; ?>>
										<span class="data-trigger" data-on="ON" data-off="OFF"></span>
									</label>
									<label for="mwp_bmr_radio_cunn">
										<?php _e('Cunningham formula','mwp-diet');?>
									</label>
								</div><!-- /.radio -->							
								<div class="radio-input">
									<label class="mwp-radio mwp-toggle mwp-base">
										<input type="radio" name="mwp_bmr" class="form-control" tabindex="19" id="mwp_bmr_radio_avrg" value="avrg" disabled="disabled" <?php echo ($formData['mwp_bmr']=='avrg')?'checked':''; ?>>
										<span class="data-trigger" data-on="ON" data-off="OFF"></span>
									</label>
									<label for="mwp_bmr_radio_avrg">
										<?php _e('Average','mwp-diet');?>
									</label>
								</div><!-- /.radio -->					
								<div class="radio-input">
									<label class="mwp-radio mwp-toggle mwp-base">
										<input type="radio" name="mwp_bmr" class="form-control" tabindex="20" id="mwp_bmr_radio_sm" value="sm" <?php echo ($formData['mwp_bmr']=='sm')?'checked':''; ?>>
										<span class="data-trigger" data-on="ON" data-off="OFF"></span>
										
									</label>
									<label for="mwp_bmr_radio_sm">
										<?php _e('Simple Multiplier','mwp-diet');?>
									</label>
									<input type="number" class="form-control mwp_bmr_sm" tabindex="21" name="mwp_bmr_sm" id="mwp_bmr_sm" min="0" value="<?php echo $formData['mwp_bmr_sm']; ?>">
									<div id="mwp_bmr_radio_sm-error" class="mwp-error-message"></div>
								</div><!-- /.radio -->						
								<div class="radio-input">
									<label class="mwp-radio mwp-toggle mwp-base">
										<input type="radio" name="mwp_bmr" id="mwp_bmr_radio_custom" tabindex="22" value="custom" <?php echo ($formData['mwp_bmr']=='custom')?'checked':''; ?>>
										<span class="data-trigger" data-on="ON" data-off="OFF"></span>
										
									</label>
									<label for="mwp_bmr_radio_custom">
										<?php _e('Custom','mwp-diet');?>
									</label>
									 <input type="number" class="form-control mwp_bmr_custom" tabindex="23"  name="mwp_bmr_custom" id="mwp_bmr_custom" min="0" value="<?php echo $formData['mwp_bmr_custom']; ?>">
									<div id="mwp_bmr_radio_custom-error" class="mwp-error-message"></div>
								</div><!-- /.radio -->			
						</div><!-- /.bmr-section -->
						<?php endif; ?>	
						<?php if( $section['tdee'] || $section['nfat'] || $section['ncarbs'] || $section['nprotein'] || $section['mrdc'] || $section['rc'] || $section['wc'] || $section['wtg'] || $section['fw'] || $section_all ): ?>	
						<div class="tdee-section mwp-select-method-section">
							<h3><?php _e('Total Daily Energy Expenditure','mwp-diet');?></h3>								
								<div class="radio-input">
									<label class="mwp-radio mwp-toggle mwp-base">
										<input type="radio" name="mwp_tdee" class="form-control" tabindex="24" id="mwp_tdee_radio_calc" value="calc" <?php echo ($formData['mwp_tdee']=='calc')?'checked':''; ?>>
										<span class="data-trigger" data-on="ON" data-off="OFF"></span>
									</label>
									<label for="mwp_tdee_radio_calc">
										<?php _e('Calculate','mwp-diet');?>
									</label>
								</div><!-- /.radio -->					
								<div class="radio-input">
									<label class="mwp-radio mwp-toggle mwp-base">
										<input type="radio" name="mwp_tdee" class="form-control" tabindex="25" id="mwp_tdee_radio_sm" value="sm" <?php echo ($formData['mwp_tdee']=='sm')?'checked':''; ?>>
										<span class="data-trigger" data-on="ON" data-off="OFF"></span>
									</label>
									<label for="mwp_tdee_radio_sm">
										<?php _e('Simple Multiplier','mwp-diet');?>
									</label>
									<input type="number" class="form-control mwp_tdee_sm" tabindex="26" name="mwp_tdee_sm" id="mwp_tdee_sm" min="0" value="<?php echo $formData['mwp_tdee_sm']; ?>">
									<div id="mwp_tdee_radio_sm-error" class="mwp-error-message"></div>
								</div><!-- /.radio -->					
								<div class="radio-input">
									<label class="mwp-radio mwp-toggle mwp-base">
										<input type="radio" name="mwp_tdee" class="form-control" tabindex="27" id="mwp_tdee_radio_custom" value="custom" <?php echo ($formData['mwp_tdee']=='custom')?'checked':''; ?>>
										<span class="data-trigger" data-on="ON" data-off="OFF"></span>
									</label>
									<label for="mwp_tdee_radio_custom">
										<?php  _e('Custom','mwp-diet');?>
									</label>
									<input type="number" class="form-control mwp_tdee_custom" tabindex="28" name="mwp_tdee_custom" id="mwp_tdee_custom" min="0" value="<?php echo $formData['mwp_tdee_custom']; ?>">
									<div id="mwp_tdee_radio_custom-error" class="mwp-error-message"></div>
								</div><!-- /.radio -->			
						</div><!-- /.tdee-section -->
						<?php endif; ?>	
					</div><!-- /.col-md-6 -->
				</div><!-- /row -->
				<div class="row">
					<div class="col-md-12">
						<div class="mwp-submit-wrapper">
							<div class="col-md-6 col-sm-6">
								<button type="button" class="btn btn-default" id="mwp-clear-diet-form"><?php  _e('Clear','mwp-diet');?></button>
							</div>
							<div class="col-md-6 col-sm-6">
								<?php wp_nonce_field( 'mwp_register_ajax_nonce', 'mwp-diet-nonce' );?>
								<button type="submit" class="ladda-button pull-right" data-style="expand-left" data-color="green" id="mwp-submit-diet-form"> <span class="ladda-label"><?php echo $submit_btn_text;?></span></button>
							</div><!-- /.col-md-6 -->	
						</div><!-- /.form-group -->	
					</div><!-- /.col-md-12 -->	
				</div>
			</div> <!-- /.panel-body -->
							
		</div>
		<div class="mwp-diet-results-holder clearfix">
			<?php echo MWP_Diet_Public::bootstrap_content_arrange($result_meta);?>
		</div><!-- /.mwp-diet-results-holder --> 
	<?php do_action('mwp_dc_form_inner_bottom');?>
	</div><!-- /.mwp-diet-form -->
	<p style="font-size:10px;"><?php  _e('Before using the data obtained using this calculator , please consult with doctor','mwp-diet');?></p>
</form>

<script type="text/javascript">
(function( $ ) {
	$(document).ready(function(){  
			$( "body" ).mwpDietCalculator({
				ajaxurl        		: <?php echo json_encode( admin_url('admin-ajax.php') ) ?>,
				form_id        		: <?php echo json_encode( $mwp_options['counter'] ) ?>,
				default_system      : <?php echo json_encode( $mwp_options['metric_system_default'] ) ?>,
				disable_popover     : <?php echo json_encode( $mwp_options['disable_popover'] ) ?>,
				bootstrap_select    : <?php echo json_encode( $mwp_options['bootstrap_select'] ) ?>,
				<?php if( $section_all ): ?>
					all_sections 		: true,
				<?php else: ?>
					result_sections 	: <?php echo json_encode($result_sections); ?>,
				<?php endif; ?>
				<?php if($mwp_options['required_field_text'] ): ?>
				required_field_msg  : <?php echo json_encode( $mwp_options['required_field_text']) ?>,
				<?php endif; ?>
				<?php if( $mwp_options['required_field_text_2'] ): ?>
				empty_field_msg     : <?php echo json_encode( $mwp_options['required_field_text_2'] ) ?>
				<?php endif; ?>
				<?php do_action( 'mwp_dc_script_options' ); ?>
			});
	});
})(jQuery);
</script>