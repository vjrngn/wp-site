<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://mwp-development.com/wordpress-diet-calories-calculator/
 * @uthor 	   MWP Development
 * @since      1.0.0
 *
 * @package    MWP_Diet
 * @subpackage MWP_Diet/public
 */
class MWP_Diet_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		global $post;
		if( isset($post->post_content) AND has_shortcode( $post->post_content, 'mwp_diet_calculator') )
		{
			$protocol = is_ssl() ? 'https' : 'http';
			$google_font = get_option('mwp_diet_google_fonts', '');
			if($google_font){
				$google_font =  str_replace(' ', '+', $google_font);
				wp_enqueue_style( $this->plugin_name.'-custom-font',"$protocol://fonts.googleapis.com/css?family=$google_font:400,500,600,700,300&amp;subset=latin,latin-ext");  
			}else{
				wp_enqueue_style( $this->plugin_name.'-open-sans', "$protocol://fonts.googleapis.com/css?family=Open+Sans:400,500,600,700,300&amp;subset=latin,latin-ext" );
			}
			if(get_option('mwp_diet_disable_bootstrap') !== 'enabled'){
				wp_enqueue_style( $this->plugin_name.'-bootstrap-css', plugin_dir_url( __FILE__ ) . 'css/bootstrap.min.css', array(), $this->version, 'all' );
			}
			if(get_option('mwp_diet_disable_bootstrap_select') !== 'enabled'){
				wp_enqueue_style( $this->plugin_name.'-bootstrap-select-css', plugin_dir_url( __FILE__ ) . 'css/bootstrap-select.min.css', array(), $this->version, 'all' );
			}
			wp_enqueue_style( $this->plugin_name.'-ladda-css', plugin_dir_url( __FILE__ ) . 'css/ladda.min.css', array(), $this->version, 'all' );
			wp_enqueue_style( $this->plugin_name.'-public-css', plugin_dir_url( __FILE__ ) . 'css/mwp-diet-public.min.css', array(), $this->version, 'all' );

		}
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		global $post;
		if( isset($post->post_content) AND has_shortcode( $post->post_content, 'mwp_diet_calculator') )
		{
			if(get_option('mwp_diet_disable_bootstrap') !== 'enabled'){
				wp_enqueue_script( $this->plugin_name.'-bootstrap-js', plugin_dir_url( __FILE__ ) . 'js/bootstrap.min.js', array( 'jquery' ), $this->version, true );
			}
			if(get_option('mwp_diet_disable_bootstrap_select') !== 'enabled'){
				wp_enqueue_script( $this->plugin_name.'-bootstrap-select-js', plugin_dir_url( __FILE__ ) . 'js/bootstrap-select.min.js', array( 'jquery' ), $this->version, true );
			}
			wp_enqueue_script( $this->plugin_name.'-spin-js', plugin_dir_url( __FILE__ ) . 'js/spin.min.js', array( 'jquery' ), $this->version, true );
			wp_enqueue_script( $this->plugin_name.'-ladda-js', plugin_dir_url( __FILE__ ) . 'js/ladda.min.js', array( 'jquery' ), $this->version, true );
			wp_enqueue_script( $this->plugin_name.'-public-js', plugin_dir_url( __FILE__ ) . 'js/mwp-diet-public.min.js', array( 'jquery' ), $this->version, true );
		}

	}
	
	/**
	 * MWP Diet Calculator Shortcode.
	 *
	 * @since    1.0.4
	 */
	public function mwp_diet_calculator_shortcode($atts, $content = null) {
		STATIC $i=0;
		$i++;
		$mwp_options = array();
		$formData 									= MWP_Diet_Calculator::get_default_data();
		$section 									= MWP_Diet_Admin::get_sections_array();
		$result_sections 							= MWP_Diet_Admin::get_filtered_sections_array();
		$results_meta 								= MWP_Diet_Admin::get_results_meta_array();
		$mwp_options['counter'] 					= $i;	
		$mwp_options['disable_popover'] 			= get_option('mwp_diet_popover_off') ? get_option('mwp_diet_popover_off') : '';
		$mwp_options['bootstrap_select'] 			= get_option('mwp_diet_disable_bootstrap_select') ? get_option('mwp_diet_disable_bootstrap_select') : '';
		$mwp_options['required_field_text']       	= esc_html( get_option('mwp_diet_required_field_text', '')); 
		$mwp_options['required_field_text_2'] 		= esc_html( get_option('mwp_diet_required_field_text_2', '')); 
		$mwp_options['metric_system_default']		= esc_html( get_option('mwp_diet_metric_default', ''));
		
		$content = $this->mwp_render_form_template( 'mwp_diet_form',array( 
																		'formData'           => $formData, 
																		'counter'           => $i, 
																		'mwp_options'       => $mwp_options, 
																		'section'           => $section, 
																		'result_sections'   => $result_sections, 
																		'results_meta'		=> $results_meta  
																		));
		return apply_filters('mwp_dc_shortcode_content', $content);
    }
	
	
	/**
	 * Render template
	 *
	 * @since    1.0.0
	 */ 
	public function mwp_render_form_template($template='mwp_diet_form', $variables = array()) {
		extract($variables);
		// Start output buffering.
		ob_start();
		ob_implicit_flush(0);

		try {
			include MWP_DIET_PATH . 'public/templates/' . $template . '.php';
		} catch (Exception $e) {
			ob_end_clean();
			throw $e;
		}

		return ob_get_clean();
	} 
	
	/**
	 * Processing AJAX request data
	 *
	 * @since    1.0.4
	 */
	public function processing_diet_data()
	{
		$parameters = array();
		$results = array();
		$result = array(
			'status' => 'error',
			'data' => '',
			'message' => '',
		);
		
		$parameters = isset($_POST['mwp_diet_data']) ? $_POST['mwp_diet_data']:array();
		parse_str($parameters, $parameters);
		self::sanitize($parameters);	
		$nonce = isset($parameters['mwp-diet-nonce']) ? $parameters['mwp-diet-nonce']:'';
		
		if (wp_verify_nonce( $nonce, 'mwp_register_ajax_nonce' ) && !empty($parameters)) {
				$calculations = new MWP_Diet_Calculator($parameters);
				do_action( 'mwp_dc_before_result_processed', $parameters );
				
				$results = apply_filters('mwp_dc_result_data',$calculations->get_results(), $parameters);

				
				do_action( 'mwp_dc_after_result_processed', $results );
				
				$result = array(
					'status' => 'success',
					'data' => $results,
					'message' => '',
				);		
        }

		wp_send_json($result);
	}
	
	public function add_nutrition_data_to_results( $result, $parameters ) {
		
		$nutrition         = new MWP_DC_Nutrition($parameters);
		$nutrition_results = $nutrition->get_nutrition_results();
		$nutrition_results = array_merge( $result, $nutrition_results );
		
		return $nutrition_results;
	}
	
	public function rewrite_meta_array( $result_meta ) {
		if( isset($result_meta['bmr']) && isset($result_meta['tdee']) ){
			unset($result_meta['bmr']);
			unset($result_meta['tdee']);
			$tdee = array( 'tdee' =>
				array( 'title' => __('TDEE','mwp-diet'), 
				'text' => esc_html( get_option('mwp_diet_tdee_popover_text', '') ) )
				);
			$nutrition_meta_array        = MWP_DC_Nutrition::get_rest_nutrition_gd_meta();
			$nutrition_meta_array 		 = $tdee + $nutrition_meta_array;
			$nutrition_meta_array['bmr'] = array( 'title' => __('BMR','mwp-diet'), 
													'text' => esc_html( get_option('mwp_diet_bmr_popover_text', '') ) );
													
			$result_meta                 = array_merge($nutrition_meta_array,$result_meta);
		}
		
		return $result_meta;
	}
	
	/**
	 * Sanitize fields input values
	 *
	 * @since    1.0.4
	 */
	public static function sanitize( $input ) {
		$new_input = array();

		foreach ( $input as $key => $val ) {	
			$new_input[ $key ] = ( isset( $input[ $key ] ) ) ?
				sanitize_text_field( $val ) :
				'';
		}
		return $new_input;
	}
	
	/**
	 * Arrange bootstrap columns
	 *
	 * @since    1.0.4
	 */
	public static function bootstrap_content_arrange( $data ) {
		if(!is_array($data))
			return;
		
		$html = '';
		$items 	= count($data);       // qnt of items
		$rows 	= ceil($items/4);     // rows to fill
		$lr 	= $items%4;           // last row items
		$lrc 	= $lr;                // counter to last row
		$i 		= 0;
		$cell 	= 0;
						
		$html .=  '<div class="row">';
		foreach ($data as $key => $value) { 
			if(!isset($value['value'])){
				$value['value'] = '--';
			}	
			if ($rows > 1) { // if not last row...
				if ($cell < 4) { 
					$class = "col-md-3";
					$html .=  self::get_result_item($class, $key, $value['title'], $value['text'], $value['value']);
					$cell++;
					if($cell == 4){
						$cell 	= 0;
						$rows--; 
						$html .=  '</div><div class="row">'; // end a row
					}      
				}
			} elseif ($rows == 1 && $lr > 0) { // if last row and still has items
				if ($lrc > 0) { // iterate over qnt of remaining items
					if($lr == 2){ 
						$class = "col-md-6";
						$html .=  self::get_result_item($class, $key, $value['title'], $value['text'], $value['value']);
					}elseif($lr == 3){
						$class = "col-md-4";
						$html .=  self::get_result_item($class, $key, $value['title'], $value['text'], $value['value']);
					}elseif($lr == 4){
						$class = "col-md-3";
						$html .=  self::get_result_item($class, $key, $value['title'], $value['text'], $value['value']);
					}else{ 
						$class = "col-md-12";
						$html .=  self::get_result_item($class, $key, $value['title'], $value['text'], $value['value']);
					}	
				} 
			}else{
				if ($cell < 4) { 
					$class = "col-md-3";
					$html .=  self::get_result_item($class, $key, $value['title'], $value['text'], $value['value']);
					$cell++;
					if($cell == 4){
						$cell 	= 0;
						$rows--; 
						$html .=  '</div><div class="row">'; // end a row
					}      
				}
			}
			$i++;
		}
		$html .=  "</div>";  // end a row
		
		return $html;
	}
	
	/**
	 * Result item template
	 *
	 * @since    1.0.3
	 */
	public static function get_result_item( $class = 'col-md-12', $field = 'bmi', $title = 'BMI', $text = 'Popover text', $result_value = '--' ) {
		$html = '<div class="'.$class.'">
		 <div class="mwp_result_inner_wrapper" title="'.$title.'" data-toggle="popover" data-trigger="hover" data-content="'.$text.'" data-placement="top">
				<span id="mwp_'.$field.'_result" class="mwp_results">'.$result_value.'</span>
				<h3>'.$title.'</h3>
			</div>
		</div>';
		return apply_filters('mwp_dc_result_item_template',$html, $class, $field, $title, $text, $result_value);
	}
	
	/**
	 * Result script override
	 *
	 * @since    1.0.6
	 */
	public function mwp_dc_script_options_override() {
		echo ', after_result_processing: function (data, $container) {	
					var mwp_nfat 	 = $("#mwp_nfat_result", $container),
					mwp_ncarbs 		 = $("#mwp_ncarbs_result", $container),
					mwp_nprotein 	 = $("#mwp_nprotein_result", $container);
					
					if (data.mwp_nfat) {
						mwp_nfat.parent().addClass("result-success");
						mwp_nfat.html(data.mwp_nfat + " " +  data.mwp_nutrition_mass_label);
					}else{
						mwp_nfat.html("--");
					}
					if (data.mwp_ncarbs) {
						mwp_ncarbs.parent().addClass("result-success");
						mwp_ncarbs.html(data.mwp_ncarbs + " " +  data.mwp_nutrition_mass_label);
					}else{
						mwp_ncarbs.html("--");
					}
					
					if (data.mwp_nprotein) {
						mwp_nprotein.parent().addClass("result-success");
						mwp_nprotein.html(data.mwp_nprotein + " " + data.mwp_nutrition_mass_label);
					}else{
						mwp_nprotein.html("--");
					}
										
			},';
	}
}
