(function( $ ) {
	'use strict';
	
	$(document).ready(function() {
		//colorpicker
		if ($('.mwp-color-field').length){
			$('.mwp-color-field').wpColorPicker();
		}
		
		//tabs
		if ($('.mwp-admin-tabs').length){
	
			$('.mwp-admin-tabs').each(function(){
				
				var adminTabs = $(this);
				var adminTabsSection = $(this).parent();
				adminTabsSection.find('.tab-content').hide();
				var tabHash 	= window.location.hash;
				
				if (tabHash){
					var activeTab = tabHash;
					activeTab = activeTab.split('#');
					activeTab = activeTab[1];
					adminTabs.find('li').removeClass('active');
					adminTabs.find('a[href="'+tabHash+'"]').parent().addClass('active');
					adminTabsSection.find('#mwp-'+activeTab).show();
				} else {

					var activeTab = adminTabs.find('.active > a').attr('href');
					activeTab = activeTab.split('#');
					activeTab = activeTab[1];
					adminTabsSection.find('#mwp-'+activeTab).show();
				}
				
				adminTabs.find('li > a').on('click', function(e) {
					adminTabsSection.find('.tab-content').hide();
					adminTabs.find('li').removeClass('active');
					
					$(this).parent().addClass('active');
					var activeTab = $(this).attr('href');
					activeTab = activeTab.split('#');
					activeTab = activeTab[1];
					
					adminTabsSection.find('#mwp-'+activeTab).show();
					
				});
			
			});
		
		}
		
		//sections
		var mwp_diet_section_all = $('input[name="mwp_diet_section_all"]');
		mwp_diet_section_all.change(function () {
			if ( this.checked){
				$('#mwp-diet-admin-select-sections input').prop('checked', false);
			}
		});
		$('#mwp-sections input:checkbox').change(function () {
			if ($("#mwp-diet-admin-select-sections input:checked").length == 0){
				mwp_diet_section_all.prop('checked', true);
			}
			else{
			   mwp_diet_section_all.prop('checked', false);
			}
		});
	});
	
})( jQuery );
