( function() {
	tinymce.create('tinymce.plugins.MWPDC', {
		
		mwp_assets_url: function( url ) {
			var sUrl, i, l, assetsUrl = '';
			sUrl = url.split( '/' );
			for ( i = 0, l = sUrl.length - 1; i < l; i++ ) {
				assetsUrl += sUrl[ i ] + '/';
			}

			return assetsUrl;
		},
		
		/**
		 * Get assets directory path
		 */
		mwp_img_url: function( url ) {
			return this.mwp_assets_url( url ) + 'img/mwp-diet-tinymce.png';
		},
		
		/**
		 * Initialize the TinyMCE plugin
		 */
		init : function( editor, url ) {
			editor.addButton( 'mwp_diet_calculator', {
				title: 'MWP Diet Calculator',
				image: this.mwp_img_url( url ),
				onclick: function() {
					editor.insertContent('[mwp_diet_calculator]');
				}
			} );
		},
 
        /**
         * Returns information about the plugin as a name/value array.
         */
        getInfo : function() {
            return {
                longname : 'MWP Diet Calculator',
                author : 'MWP Development',
                authorurl : 'http://mwp-development.com',
                infourl : 'http://mwp-development.com',
                version : "1.0"
            };
        }
	} );

	// Register the plugin
	tinymce.PluginManager.add( 'mwp_diet_calculator', tinymce.plugins.MWPDC );
} )();