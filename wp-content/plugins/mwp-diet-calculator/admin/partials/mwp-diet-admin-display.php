<?php
/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://mwp-development.com/wordpress-diet-calories-calculator/
 * @uthor 	   MWP Development
 * @since      1.0.3
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin/partials
 */
?>
<div class="mwp-settings-wrapper">
	<div id="mwp-admin-options-container">
		<ul class="mwp-admin-tabs">
			<li class="active"><a href="#general"><?php _e('General','mwp-diet'); ?></a></li>
			<li><a href="#styling"><?php _e('Styling','mwp-diet'); ?></a></li>
			<li><a href="#texts"><?php _e('Texts','mwp-diet'); ?></a></li>
			<li><a href="#sections"><?php _e('Sections','mwp-diet'); ?></a></li>
			<?php do_action('mwp_dc_admin_tab');?>
		</ul>
		<div class="form-wrapper">
			<form action="options.php" method="post">
				<?php settings_fields('mwp_diet_group'); ?>	
				<div id="mwp-general" class="tab-content">	
					<div class="section">
						<div class="section-head">
							<h2><?php _e('Calculation System', 'mwp-diet'); ?></h2>
						</div><!-- / section-head -->
		
						<div class="section-body">
							<?php $option_name = 'mwp_diet_metric_default';
								$action_option_label = __('Set metric calculation system as default', 'mwp-diet');
								$action_options = get_option($option_name) ? get_option($option_name) : ''; 
							?>
							<div class="checkbox">
								<label class="mwp-checkbox mwp-toggle">
									<input type="checkbox" id="<?php echo $option_name; ?>" name="<?php echo $option_name; ?>" value="enabled" <?php checked( 'enabled', $action_options, true ) ; ?> />
									<span class="data-trigger" data-on="ON" data-off="OFF"></span>
								</label>
								<label for="<?php echo $option_name; ?>">
									<?php echo esc_attr($action_option_label); ?>
								</label>
							</div><!-- / checkbox -->
						</div><!-- / section-body -->
					</div><!-- / section -->
					<div class="section">
						<div class="section-head">
							<h2><?php _e('Disable Popover', 'mwp-diet'); ?></h2>
						</div><!-- / section-head -->
		
						<div class="section-body">
							<?php $option_name = 'mwp_diet_popover_off';
								$action_option_label = __('Disable', 'mwp-diet');
								$action_options = get_option($option_name) ? get_option($option_name) : ''; 
							?>
							<div class="checkbox">
								<label class="mwp-checkbox mwp-toggle">
									<input type="checkbox" id="<?php echo $option_name; ?>" name="<?php echo $option_name; ?>" value="enabled" <?php checked( 'enabled', $action_options, true ) ; ?> />
									<span class="data-trigger"  data-on="ON" data-off="OFF"></span>
								</label>
								<label for="<?php echo $option_name; ?>">
									<?php echo esc_attr($action_option_label); ?>
								</label>
							</div><!-- / checkbox -->
						</div><!-- / section-body -->
					</div><!-- / section -->
					<div class="section">
						<div class="section-head">
							<h2><?php _e('Disable Bootstrap', 'mwp-diet');  ?></h2>
						</div><!-- / section-head -->
		
						<div class="section-body">
							<?php $option_name = 'mwp_diet_disable_bootstrap';
								$action_option_label = __('Disable', 'mwp-diet');
								$action_options = get_option($option_name) ? get_option($option_name) : ''; 
							?>
							<div class="checkbox">
								<label class="mwp-checkbox mwp-toggle">
									<input type="checkbox" id="<?php echo $option_name; ?>" name="<?php echo $option_name; ?>" value="enabled" <?php checked( 'enabled', $action_options, true ) ; ?> />
									<span class="data-trigger"  data-on="ON" data-off="OFF"></span>
								</label>
								<label for="<?php echo $option_name; ?>">
									<?php echo esc_attr($action_option_label); ?>
								</label>
							</div><!-- / checkbox -->
						</div><!-- / section-body -->
					</div><!-- / section -->
					<div class="section">
						<div class="section-head">
							<h2><?php  _e('Disable Bootstrap Select', 'mwp-diet');?></h2>
						</div><!-- / section-head -->
		
						<div class="section-body">
							<?php $option_name = 'mwp_diet_disable_bootstrap_select';
								$action_option_label = __('Disable', 'mwp-diet');
								$action_options = get_option($option_name) ? get_option($option_name) : ''; 
							?>
							<div class="checkbox">
								<label class="mwp-checkbox mwp-toggle">
									<input type="checkbox" id="<?php echo $option_name; ?>" name="<?php echo $option_name; ?>" value="enabled" <?php checked( 'enabled', $action_options, true ) ; ?> />
									<span class="data-trigger"  data-on="ON" data-off="OFF"></span>
								</label>
								<label for="<?php echo $option_name; ?>">
									<?php echo esc_attr($action_option_label); ?>
								</label>
							</div><!-- / checkbox -->
						</div><!-- / section-body -->
					</div><!-- / section -->

				
				</div><!-- / tab-content -->
				<div id="mwp-styling" class="tab-content">
					<div class="section">
						<div class="section-head">
							<h2><?php _e('Use Custom Color Scheme', 'mwp-diet'); ?></h2>
						</div><!-- / section-head -->
		
						<div class="section-body">
							<?php $option_name = 'mwp_diet_custom_color_scheme';
								$action_option_label = __('Default scheme', 'mwp-diet');
								$action_options = get_option($option_name) ? get_option($option_name) : ''; 
							?>
							<div class="checkbox">
								<label class="mwp-checkbox mwp-toggle">
									<input type="checkbox" id="<?php echo $option_name; ?>" name="<?php echo $option_name; ?>" value="enabled" <?php checked( 'enabled', $action_options, true ) ; ?> />
									<span class="data-trigger"  data-on="ON" data-off="OFF"></span>
								</label>
								<label for="<?php echo $option_name; ?>">
									<?php echo esc_attr($action_option_label); ?>
								</label>
							</div><!-- / checkbox -->
						</div><!-- / section-body -->
					</div><!-- / section -->
					<div class="section">
						<div class="section-head">
							<h2><?php _e('Google Fonts', 'mwp-diet'); ?></h2>
						</div><!-- / section-head -->
						<div class="section-body">
							<?php $option_name = 'mwp_diet_google_fonts';
		
							$options_array = $google_fonts_array;
							
		
							$selected_value = get_option($option_name); ?>
							<div class="select-box">
								<select name="<?php echo $option_name; ?>">
									<?php if(!empty($options_array)) :
										foreach($options_array as $hrs => $label) : ?>
											<option value="<?php echo $hrs; ?>"<?php echo ($selected_value == $hrs ? ' selected="selected"' : ''); ?>><?php echo $label; ?></option>
										<?php endforeach;
									endif; ?>
								</select>
							</div><!-- /select-box -->
							
						</div><!-- /section-body -->
					</div><!-- / section -->
					<div class="section">
						<div class="section-head">
							<h2><?php _e('FORM COLOR SETTINGS', 'mwp-diet'); ?></h2>
						</div><!-- / section-head -->
						<div class="section-body">
							<?php
							$color_options = array(
								array(
									'name' => 'mwp_diet_labels_color',
									'title' => __('Form Labels Color', 'mwp-diet'),
									'value' => get_option('mwp_diet_labels_color'),
									'default' => '#696560'
								),
								array(
									'name' => 'mwp_diet_form_heading_bg_color',
									'title' => __('Form Heading BG Color', 'mwp-diet'),
									'value' => get_option('mwp_diet_form_heading_bg_color'),
									'default' => '#59c4bc'
								),
								array(
									'name' => 'mwp_diet_form_heading_color',
									'title' => __('Form Heading Color', 'mwp-diet'),
									'value' => get_option('mwp_diet_form_heading_color'),
									'default' => '#ffffff'
								),
								array(
									'name' => 'mwp_diet_form_body_color',
									'title' => __('Form Body Color', 'mwp-diet'),
									'value' => get_option('mwp_diet_form_body_color'),
									'default' => '#696560'
								),
								array(
									'name' => 'mwp_diet_form_body_bg_color',
									'title' => __('Form Body BG Color', 'mwp-diet'),
									'value' => get_option('mwp_diet_form_body_bg_color'),
									'default' => '#ffffff'
								),
							);
							
							foreach($color_options as $color_option):
							
								echo '<label class="mwp-color-label" for="'.$color_option['name'].'">'.$color_option['title'].'</label>';
								echo '<input data-default-color="'.$color_option['default'].'" type="text" name="'.$color_option['name'].'" value="'.$color_option['value'].'" id="'.$color_option['name'].'" class="mwp-color-field" />';
								
							endforeach;
							?>
		
						</div><!-- / section-body -->
					</div><!-- / section -->
					<div class="section">
						<div class="section-head">
							<h2><?php _e('FORM INPUTS COLORS', 'mwp-diet'); ?></h2>
						</div><!-- / section-head -->
						<div class="section-body">
							<?php
							$color_options = array(
								array(
									'name' => 'mwp_diet_input_focus',
									'title' => __('Input Focus Color', 'mwp-diet'),
									'value' => get_option('mwp_diet_input_focus'),
									'default' => '#59c4bc'
								),
								array(
									'name' => 'mwp_diet_input_error',
									'title' => __('Input Error Color', 'mwp-diet'),
									'value' => get_option('mwp_diet_input_error'),
									'default' => '#ea6582'
								),
							);
							
							foreach($color_options as $color_option):
							
								echo '<label class="mwp-color-label" for="'.$color_option['name'].'">'.$color_option['title'].'</label>';
								echo '<input data-default-color="'.$color_option['default'].'" type="text" name="'.$color_option['name'].'" value="'.$color_option['value'].'" id="'.$color_option['name'].'" class="mwp-color-field" />';
								
							endforeach;
							?>
		
						</div><!-- / section-body -->
					</div><!-- / section -->
					<div class="section">
						<div class="section-head">
							<h2><?php _e('SUBMIT BUTTON COLORS', 'mwp-diet'); ?></h2>
						</div><!-- / section-head -->
						<div class="section-body">
							<?php
							$color_options = array(
								array(
									'name' => 'mwp_diet_submit_btn_bg_color',
									'title' => __('Submit Button BG Color', 'mwp-diet'),
									'value' => get_option('mwp_diet_submit_btn_bg_color'),
									'default' => '#ff9966'
								),
								array(
									'name' => 'mwp_diet_submit_btn_bg_hcolor',
									'title' => __('Submit Button Hover BG Color', 'mwp-diet'),
									'value' => get_option('mwp_diet_submit_btn_bg_hcolor'),
									'default' => '#fea075'
								),
								array(
									'name' => 'mwp_diet_submit_btn_tcolor',
									'title' => __('Submit Button Text Color', 'mwp-diet'),
									'value' => get_option('mwp_diet_submit_btn_tcolor'),
									'default' => '#ffffff'
								),
								array(
									'name' => 'mwp_diet_submit_btn_thcolor',
									'title' => __('Submit Button Hover Text Color', 'mwp-diet'),
									'value' => get_option('mwp_diet_submit_btn_thcolor'),
									'default' => '#ffffff'
								),
							);
							
							foreach($color_options as $color_option):
							
								echo '<label class="mwp-color-label" for="'.$color_option['name'].'">'.$color_option['title'].'</label>';
								echo '<input data-default-color="'.$color_option['default'].'" type="text" name="'.$color_option['name'].'" value="'.$color_option['value'].'" id="'.$color_option['name'].'" class="mwp-color-field" />';
								
							endforeach;
							?>
		
						</div><!-- / section-body -->
					</div><!-- / section -->
					
					<div class="section">
						<div class="section-head">
							<h2><?php _e('RESULTS BLOCK', 'mwp-diet'); ?></h2>
						</div><!-- / section-head -->
						<div class="section-body">
							<?php
							$color_options = array(
								array(
									'name' => 'mwp_diet_result_block_color',
									'title' => __('Results Block Color', 'mwp-diet'),
									'value' => get_option('mwp_diet_result_block_color'),
									'default' => '#e8eeee'
								),
								array(
									'name' => 'mwp_diet_result_active_color',
									'title' => __('Results Block Active Color', 'mwp-diet'),
									'value' => get_option('mwp_diet_result_active_color'),
									'default' => '#59c4bc'
								),
								array(
									'name' => 'mwp_diet_result_attention_color',
									'title' => __('Results Block Error Color', 'mwp-diet'),
									'value' => get_option('mwp_diet_result_attention_color'),
									'default' => '#ea6582'
								),
							);
							
							foreach($color_options as $color_option):
							
								echo '<label class="mwp-color-label" for="'.$color_option['name'].'">'.$color_option['title'].'</label>';
								echo '<input data-default-color="'.$color_option['default'].'" type="text" name="'.$color_option['name'].'" value="'.$color_option['value'].'" id="'.$color_option['name'].'" class="mwp-color-field" />';
								
							endforeach;
							?>
		
						</div><!-- / section-body -->
					</div><!-- / section -->
					<div class="section">
						<div class="section-head">
							<?php $section_title = __('RADIO BUTTONS', 'mwp-diet'); ?>
							<h2><?php echo esc_attr($section_title); ?></h2>
						</div><!-- / section-head -->
						<div class="section-body">
							<?php
							$color_options = array(
								array(
									'name' => 'mwp_diet_radio_btn_general_color',
									'title' => __('Radio Buttons General Color', 'mwp-diet'),
									'value' => get_option('mwp_diet_radio_btn_general_color'),
									'default' => '#ff9966'
								),								
								array(
									'name' => 'mwp_diet_radio_btn_general_secondary_color',
									'title' => __('Radio Buttons General - Secondary Color', 'mwp-diet'),
									'value' => get_option('mwp_diet_radio_btn_general_secondary_color'),
									'default' => '#ffffff'
								),
								array(
									'name' => 'mwp_diet_radio_btn_color',
									'title' => __('Radio Buttons Color', 'mwp-diet'),
									'value' => get_option('mwp_diet_radio_btn_color'),
									'default' => '#59c4bc'
								),
								array(
									'name' => 'mwp_diet_radio_btn_secondary_color',
									'title' => __('Radio Buttons - Secondary Color', 'mwp-diet'),
									'value' => get_option('mwp_diet_radio_btn_secondary_color'),
									'default' => '#ffffff'
								),
							);
							
							foreach($color_options as $color_option):
							
								echo '<label class="mwp-color-label" for="'.$color_option['name'].'">'.$color_option['title'].'</label>';
								echo '<input data-default-color="'.$color_option['default'].'" type="text" name="'.$color_option['name'].'" value="'.$color_option['value'].'" id="'.$color_option['name'].'" class="mwp-color-field" />';
								
							endforeach;
							?>
		
						</div><!-- / section-body -->
					</div><!-- / section -->


				</div><!-- / tab-content -->	
				<div id="mwp-texts" class="tab-content">	
					<div class="section">
							<div class="section-head">
							<?php $option_name = 'mwp_diet_form_heading_text';
							$option_value = get_option($option_name); ?>
							<h2><?php _e('Form Heading Text', 'mwp-diet'); ?></h2>
						</div>
						<div class="heading-wrap">
							<input type="text" name="<?php echo $option_name; ?>" value="<?php echo $option_value; ?>" class="regular-text" />
						</div>	
					</div><!-- / section -->
					<div class="section">
							<div class="section-head">
							<?php $option_name = 'mwp_diet_submit_btn_text';
							$option_value = get_option($option_name); ?>
							<h2><?php _e('Submit Button Text', 'mwp-diet'); ?></h2>
						</div>
						<div class="heading-wrap">
							<input type="text" name="<?php echo $option_name; ?>" value="<?php echo $option_value; ?>" class="regular-text" />
						</div>	
					</div><!-- / section -->
					<div class="section">
							<div class="section-head">
							<?php $option_name = 'mwp_diet_required_field_text';
							$option_value = get_option($option_name); ?>
							<h2><?php _e('Required field text', 'mwp-diet'); ?></h2>
						</div>
						<div class="heading-wrap">
							<input type="text" name="<?php echo $option_name; ?>" value="<?php echo $option_value; ?>" class="regular-text" />
						</div>	
					</div><!-- / section -->
					<div class="section">
							<div class="section-head">
							<?php $option_name = 'mwp_diet_required_field_text_2';
							$option_value = get_option($option_name); ?>
							<h2><?php _e('Required field text 2', 'mwp-diet'); ?></h2>
						</div>
						<div class="heading-wrap">
							<input type="text" name="<?php echo $option_name; ?>" value="<?php echo $option_value; ?>" class="regular-text" />
						</div>	
					</div><!-- / section -->
					<div class="section">
						<div class="section-head">
							<?php $option_name = 'mwp_diet_bmr_popover_text';
							$popover_message = get_option($option_name) ? get_option($option_name) : ''; ?>
							<h2><?php _e('Result Popover Text For BMR', 'mwp-diet'); ?></h2>
		
						</div>
						<div class="section-body">
							<textarea cols="40" rows="5" name="<?php echo $option_name; ?>" class="field large"><?php echo $popover_message; ?></textarea>
						</div>
					</div><!-- / section -->
					<div class="section">
						<div class="section-head">
							<?php $option_name = 'mwp_diet_tdee_popover_text';
							$popover_message = get_option($option_name) ? get_option($option_name) : ''; ?>
							<h2><?php _e('Result Popover Text For TDEE', 'mwp-diet'); ?></h2>
		
						</div>
						<div class="section-body">
							<textarea cols="40" rows="5" name="<?php echo $option_name; ?>" class="field large"><?php echo $popover_message; ?></textarea>
						</div>
					</div><!-- / section -->
					<div class="section">
						<div class="section-head">
							<?php $option_name = 'mwp_diet_lbm_popover_text';
							$popover_message = get_option($option_name) ? get_option($option_name) : ''; ?>
							<h2><?php _e('Result Popover Text For LBM', 'mwp-diet'); ?></h2>
		
						</div>
						<div class="section-body">
							<textarea cols="40" rows="5" name="<?php echo $option_name; ?>" class="field large"><?php echo $popover_message; ?></textarea>
						</div>
					</div><!-- / section -->
					<div class="section">
						<div class="section-head">
							<?php $option_name = 'mwp_diet_fbm_popover_text';
							$popover_message = get_option($option_name) ? get_option($option_name) : ''; ?>
							<h2><?php _e('Result Popover Text For FBM', 'mwp-diet'); ?></h2>
		
						</div>
						<div class="section-body">
							<textarea cols="40" rows="5" name="<?php echo $option_name; ?>" class="field large"><?php echo $popover_message; ?></textarea>
						</div>
					</div><!-- / section -->
					<div class="section">
						<div class="section-head">
							<?php $option_name = 'mwp_diet_bmi_popover_text';
							$popover_message = get_option($option_name) ? get_option($option_name) : ''; ?>
							<h2><?php _e('Result Popover Text For BMI', 'mwp-diet'); ?></h2>
		
						</div>
						<div class="section-body">
							<textarea cols="40" rows="5" name="<?php echo $option_name; ?>" class="field large"><?php echo $popover_message; ?></textarea>
						</div>
					</div><!-- / section -->
					<div class="section">
						<div class="section-head">
							<?php $option_name = 'mwp_diet_wth_popover_text';
							$popover_message = get_option($option_name) ? get_option($option_name) : ''; ?>
							<h2><?php _e('Result Popover Text For WTH', 'mwp-diet'); ?></h2>
		
						</div>
						<div class="section-body">
							<textarea cols="40" rows="5" name="<?php echo $option_name; ?>" class="field large"><?php echo $popover_message; ?></textarea>
						</div>
					</div><!-- / section -->
					<div class="section">
						<div class="section-head">
							<?php $option_name = 'mwp_diet_mfm_popover_text';
							$popover_message = get_option($option_name) ? get_option($option_name) : ''; ?>
							<h2><?php _e('Result Popover Text For MFM', 'mwp-diet'); ?></h2>
		
						</div>
						<div class="section-body">
							<textarea cols="40" rows="5" name="<?php echo $option_name; ?>" class="field large"><?php echo $popover_message; ?></textarea>
						</div>
					</div><!-- / section -->
					<div class="section">
						<div class="section-head">
							<?php $option_name = 'mwp_diet_mrdc_popover_text';
							$popover_message = get_option($option_name) ? get_option($option_name) : ''; ?>
							<h2><?php _e('Result Popover Text For MRDC', 'mwp-diet');?></h2>
		
						</div>
						<div class="section-body">
							<textarea cols="40" rows="5" name="<?php echo $option_name; ?>" class="field large"><?php echo $popover_message; ?></textarea>
						</div>
					</div><!-- / section -->
					<div class="section">
						<div class="section-head">
							<?php $option_name = 'mwp_diet_wcals_popover_text';
							$popover_message = get_option($option_name) ? get_option($option_name) : ''; ?>
							<h2><?php _e('Result Popover Text For Workout Calories', 'mwp-diet'); ?></h2>
		
						</div>
						<div class="section-body">
							<textarea cols="40" rows="5" name="<?php echo $option_name; ?>" class="field large"><?php echo $popover_message; ?></textarea>
						</div>
					</div><!-- / section -->
					<div class="section">
						<div class="section-head">
							<?php $option_name = 'mwp_diet_rcals_popover_text';
							$popover_message = get_option($option_name) ? get_option($option_name) : ''; ?>
							<h2><?php _e('Result Popover Text For Rest Calories', 'mwp-diet'); ?></h2>
		
						</div>
						<div class="section-body">
							<textarea cols="40" rows="5" name="<?php echo $option_name; ?>" class="field large"><?php echo $popover_message; ?></textarea>
						</div>
					</div><!-- / section -->
					<div class="section">
						<div class="section-head">
							<?php $option_name = 'mwp_diet_wtg_popover_text';
							$popover_message = get_option($option_name) ? get_option($option_name) : ''; ?>
							<h2><?php _e('Result Popover Text For Weeks To Goal', 'mwp-diet'); ?></h2>
		
						</div>
						<div class="section-body">
							<textarea cols="40" rows="5" name="<?php echo $option_name; ?>" class="field large"><?php echo $popover_message; ?></textarea>
						</div>
					</div><!-- / section -->
					<div class="section">
						<div class="section-head">
							<?php $option_name = 'mwp_diet_fw_popover_text';
							$popover_message = get_option($option_name) ? get_option($option_name) : ''; ?>
							<h2><?php _e('Result Popover Text For Final Weight', 'mwp-diet'); ?></h2>
		
						</div>
						<div class="section-body">
							<textarea cols="40" rows="5" name="<?php echo $option_name; ?>" class="field large"><?php echo $popover_message; ?></textarea>
						</div>
					</div><!-- / section -->
					<div class="section">
						<div class="section-head">
							<?php $option_name = 'mwp_diet_bmi_class_popover_text';
							$popover_message = get_option($option_name) ? get_option($option_name) : ''; ?>
							<?php $section_title = __('Result Popover Text For ', 'mwp-diet'); ?>
							<h2><?php _e('Result Popover Text For Obesity', 'mwp-diet'); ?></h2>		
						</div>
						<div class="section-body">
							<textarea cols="40" rows="5" name="<?php echo $option_name; ?>" class="field large"><?php echo $popover_message; ?></textarea>
						</div>
					</div><!-- / section -->
					<div class="section">
						<div class="section-head">
							<?php $option_name = 'mwp_dc_nutritions_fat_popover_text';
							$popover_message = get_option($option_name) ? get_option($option_name) : ''; ?>
							<?php $section_title = __('Result Popover Text For ', 'mwp-diet'); ?>
							<h2><?php _e('Result Popover Text For Fat', 'mwp-diet'); ?></h2>		
						</div>
						<div class="section-body">
							<textarea cols="40" rows="5" name="<?php echo $option_name; ?>" class="field large"><?php echo $popover_message; ?></textarea>
						</div>
					</div><!-- / section -->
					<div class="section">
						<div class="section-head">
							<?php $option_name = 'mwp_dc_nutritions_carbs_popover_text';
							$popover_message = get_option($option_name) ? get_option($option_name) : ''; ?>
							<?php $section_title = __('Result Popover Text For ', 'mwp-diet'); ?>
							<h2><?php _e('Result Popover Text For Carbohydrates', 'mwp-diet'); ?></h2>		
						</div>
						<div class="section-body">
							<textarea cols="40" rows="5" name="<?php echo $option_name; ?>" class="field large"><?php echo $popover_message; ?></textarea>
						</div>
					</div><!-- / section -->
					<div class="section">
						<div class="section-head">
							<?php $option_name = 'mwp_dc_nutritions_protein_popover_text';
							$popover_message = get_option($option_name) ? get_option($option_name) : ''; ?>
							<?php $section_title = __('Result Popover Text For ', 'mwp-diet'); ?>
							<h2><?php _e('Result Popover Text For Protein', 'mwp-diet'); ?></h2>		
						</div>
						<div class="section-body">
							<textarea cols="40" rows="5" name="<?php echo $option_name; ?>" class="field large"><?php echo $popover_message; ?></textarea>
						</div>
					</div><!-- / section -->
				</div><!-- / tab-content -->
				
				<div id="mwp-sections" class="tab-content">	
					<div class="section">
						<div class="section-head">
							<h2><?php _e('Calculate all parameters', 'mwp-diet');  ?></h2>
						</div><!-- / section-head -->
		
						<div class="section-body">
							<?php $option_name = 'mwp_diet_section_all';
								$action_option_label = __('Enable', 'mwp-diet');
								$action_options = get_option($option_name) ? get_option($option_name) : ''; 
							?>
							<div class="checkbox">
								<label class="mwp-checkbox mwp-toggle">
									<input type="checkbox" id="<?php echo $option_name; ?>" name="<?php echo $option_name; ?>" value="enabled" <?php checked( 'enabled', $action_options, true ) ; ?> />
									<span class="data-trigger"  data-on="ON" data-off="OFF"></span>
								</label>
								<label for="<?php echo $option_name; ?>">
									<?php echo esc_attr($action_option_label); ?>
								</label>
							</div><!-- / checkbox -->
						</div><!-- / section-body -->
					</div><!-- / section -->
					<div class="section" id="mwp-diet-admin-select-sections">
						<div class="section-head">
							<h2><?php _e('What do you want to calculate ?', 'mwp-diet'); ?></h2>
						</div><!-- / section-head -->
							
						<div class="section-body">
							<?php $options_array = array(
														'mwp_diet_section_bmr' 	   => __('Basal Metabolic Rate', 'mwp-diet'),
														'mwp_diet_section_tdee'    => __('Total Daily Energy Expenditure', 'mwp-diet'),
														'mwp_diet_section_lbm' 	   => __('Body Mass Index', 'mwp-diet'),
														'mwp_diet_section_fbm' 	   => __('Lean Body Mass', 'mwp-diet'),
														'mwp_diet_section_bmi' 	   => __('Fat Body Mass', 'mwp-diet'),
														'mwp_diet_section_wth' 	   => __('Waist to Height', 'mwp-diet'),
														'mwp_diet_section_mfm' 	   => __('Maximum Fat Metabolism', 'mwp-diet'),
														'mwp_diet_section_mrdc'    => __('Minimum Recommended Daily Calories', 'mwp-diet'),
														'mwp_diet_section_wc' 	   => __('Workout Calories', 'mwp-diet'),
														'mwp_diet_section_rc' 	   => __('Rest Calories', 'mwp-diet'),
														'mwp_diet_section_wtg' 	   => __('Number of weeks to reach goal weight', 'mwp-diet'),
														'mwp_diet_section_fw' 	   => __('Final weight', 'mwp-diet'),
														'mwp_diet_section_obesity' => __('Obesity class', 'mwp-diet'),
														'mwp_diet_section_fat'     => __('Fat', 'mwp-diet'),
														'mwp_diet_section_carbs'   => __('Carbohydrates', 'mwp-diet'),
														'mwp_diet_section_protein' => __('Protein', 'mwp-diet'),
													);
								foreach($options_array as $option_name => $action_option_label){
									$action_options = get_option($option_name) ? get_option($option_name) : '';
								?>
									<div class="checkbox">
										<label class="mwp-checkbox mwp-toggle">
											<input type="checkbox" id="<?php echo $option_name; ?>" name="<?php echo $option_name; ?>" value="enabled" <?php checked( 'enabled', $action_options, true ) ; ?> />
											<span class="data-trigger"  data-on="ON" data-off="OFF"></span>
										</label>
										<label for="<?php echo $option_name; ?>">
											<?php echo esc_attr($action_option_label); ?>
										</label>
									</div><br/><!-- / checkbox -->
								<?php } ?>
						</div><!-- / section-body -->
					</div><!-- / section -->
				</div><!-- / tab-content -->
				<?php echo do_action('mwp_dc_admin_tab_section');?>
				<div class="section submit-section">
					<?php 
					$submit_attributes = array( 'id' => 'mwp-diet-admin-submit' );
					@submit_button( __('Save Settings', 'mwp-diet'), 'secondary', 'submit', true, $submit_attributes );
					?>
				</div>			
			</form>						
		</div><!-- / form-wrapper -->
	</div><!-- / mwp-admin-options-container -->
</div><!-- / mwp-settings-wrapper -->