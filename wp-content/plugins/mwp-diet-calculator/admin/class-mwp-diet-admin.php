<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://mwp-development.com/wordpress-diet-calories-calculator/
 * @uthor 	   MWP Development
 * @since      1.0.0
 *
 * @package    MWP_Diet
 * @subpackage MWP_Diet/admin
 */
class MWP_Diet_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;
	
	/**
	 * The options array of this plugin.
	 *
	 * @since    1.0.0
	 * @access   public
	 * @var      array 
	 */
	public $mwp_diet_options_array;
	
	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;


	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/mwp-diet-admin.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'wp-color-picker' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/mwp-diet-admin.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'wp-color-picker' );

	}
	
	/**
	 * Including custom css styles
	 * @since    1.0.0
	 */
	function mwp_diet_custom_css() {
		$custom_color_scheme   =  get_option('mwp_diet_custom_color_scheme', '') ;
		if ($custom_color_scheme == 'enabled'){
		   echo "<style type='text/css'>" ;
			require_once MWP_DIET_PATH.'includes/customcss.php';    
		   echo "</style>";  
		}
	 
	}
	
	/**
     * Creates the settings menu and sub menus for adding and listing licenses.
	 * @since    1.0.0
     */
    public function add_mwp_diet_menu_page() {
        add_menu_page(
            __( 'MWP Diet', $this->plugin_name ),
            __( 'MWP Diet', $this->plugin_name ),
            'edit_posts',
            'mwp-diet-settings',
            array( $this, 'render_settings_page' ),
            'dashicons-carrot',
            '80.1'
        );
    }
	
	/**
     * Renders the plugin's options page.
	 * @since    1.0.1
     */
    public function render_settings_page() {
		if(!current_user_can('manage_options')) {
				wp_die(__('You do not have sufficient permissions to access this page.', 'mwp-diet'));
		}
		$google_fonts_array = self::get_google_fonts_array();
        require plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/mwp-diet-admin-display.php';
    }
	
	/**
	 * Registering settings group
	 * @since    1.0.0
	 */
	public function mwp_diet_settings_init( ) { 
		$this->mwp_diet_options_array = self::get_options_array();

		foreach($this->mwp_diet_options_array as $option=>$value){
			register_setting( 'mwp_diet_group',$option);
		}
	}
	
	/**
	 * Get sections array
	 * @since    1.0.3
	 */
	public static function get_sections_array() {
		$section 			= array();
		
		$section['bmr']		    = esc_html( get_option('mwp_diet_section_bmr', '') );
		$section['tdee']	    = esc_html( get_option('mwp_diet_section_tdee', '') );
		$section['lbm']		    = esc_html( get_option('mwp_diet_section_lbm', '') );
		$section['fbm']		    = esc_html( get_option('mwp_diet_section_fbm', '') );
		$section['bmi']		    = esc_html( get_option('mwp_diet_section_bmi', '') );
		$section['wth']		    = esc_html( get_option('mwp_diet_section_wth', '') );
		$section['mfm']		    = esc_html( get_option('mwp_diet_section_mfm', '') );
		$section['mrdc']	    = esc_html( get_option('mwp_diet_section_mrdc', '') );
		$section['wc']		    = esc_html( get_option('mwp_diet_section_wc', '') );
		$section['rc']		    = esc_html( get_option('mwp_diet_section_rc', '') );
		$section['wtg']		    = esc_html( get_option('mwp_diet_section_wtg', '') );
		$section['fw']		    = esc_html( get_option('mwp_diet_section_fw', '') );	
		$section['obesity']	    = esc_html( get_option('mwp_diet_section_obesity', '') );
		$section['nfat']	    = esc_html( get_option('mwp_diet_section_fat', '') );
		$section['ncarbs']	    = esc_html( get_option('mwp_diet_section_carbs', '') );
		$section['nprotein']	= esc_html( get_option('mwp_diet_section_protein', '') );
		
		return $section;
	}
	
	/**
	 * Get filtered sections array
	 * @since    1.0.3
	 */
	public static function get_filtered_sections_array() {
		$result_sections 	= array();
		$result_sections 	= array_filter( self::get_sections_array() );
		
		return $result_sections;
	}
	
	/**
	 * Get resullts meta array
	 * @since    1.0.3
	 */
	public static function get_results_meta_array() {
		$result_meta 			= array();
		$result_meta['bmr']		= array( 'title' => __('BMR','mwp-diet'), 'text' => esc_html( get_option('mwp_diet_bmr_popover_text', '') ) );
		$result_meta['tdee']	= array( 'title' => __('TDEE','mwp-diet'), 'text' => esc_html( get_option('mwp_diet_tdee_popover_text', '') ) );
		$result_meta['lbm']		= array( 'title' => __('LBM','mwp-diet'), 'text' => esc_html( get_option('mwp_diet_lbm_popover_text', '') ) );
		$result_meta['fbm']		= array( 'title' => __('FBM','mwp-diet'), 'text' => esc_html( get_option('mwp_diet_fbm_popover_text', '') ) );
		$result_meta['bmi']		= array( 'title' => __('BMI','mwp-diet'), 'text' => esc_html( get_option('mwp_diet_bmi_popover_text', '') ) );
		$result_meta['wth']		= array( 'title' => __('Waist to Height','mwp-diet'), 'text' => esc_html( get_option('mwp_diet_wth_popover_text', '') ) );
		$result_meta['mfm']		= array( 'title' => __('MFM','mwp-diet'), 'text' => esc_html( get_option('mwp_diet_mfm_popover_text', '') ) );
		$result_meta['mrdc']	= array( 'title' => __('MRDC','mwp-diet'), 'text' => esc_html( get_option('mwp_diet_mrdc_popover_text', '') ) );
		$result_meta['wc']		= array( 'title' => __('Workout Calories','mwp-diet'), 'text' => esc_html( get_option('mwp_diet_wcals_popover_text', '') ) );
		$result_meta['rc']		= array( 'title' => __('Rest Calories','mwp-diet'), 'text' => esc_html( get_option('mwp_diet_rcals_popover_text', '') ) );
		$result_meta['wtg']		= array( 'title' => __('Weeks To Goal','mwp-diet'), 'text' => esc_html( get_option('mwp_diet_wtg_popover_text', '') ) );
		$result_meta['fw']		= array( 'title' => __('Final Weight','mwp-diet'), 'text' => esc_html( get_option('mwp_diet_fw_popover_text', '') ) );
		$result_meta['obesity']	= array( 'title' => __('Obesity','mwp-diet'), 'text' => esc_html( get_option('mwp_diet_bmi_class_popover_text', '') ) );		
		
		return apply_filters('mwp_dc_results_meta', $result_meta);
	}
	
	/**
	 * Get options array
	 * @since    1.0.5
	 */
	public static function get_options_array() { 
		load_plugin_textdomain(
			'mwp-diet',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);
		$mwp_diet_options_array = array(
			//general section
			'mwp_diet_metric_default'=>'',
			'mwp_diet_popover_off'=>'',
			'mwp_diet_disable_bootstrap'=>'',
			'mwp_diet_disable_bootstrap_select'=>'',
			//styling section
			'mwp_diet_google_fonts'=>'Open Sans',
			'mwp_diet_custom_color_scheme'=>'',
			'mwp_diet_labels_color'=>'#696560',
			'mwp_diet_form_heading_bg_color'=>'#59c4bc',
			'mwp_diet_form_heading_color'=>'#ffffff',
			'mwp_diet_form_body_color'=>'#696560',
			'mwp_diet_form_body_bg_color'=>'#ffffff',
			'mwp_diet_input_focus'=>'#59c4bc',
			'mwp_diet_input_error'=>'#ea6582',
			'mwp_diet_submit_btn_bg_color'=>'#ff9966',
			'mwp_diet_submit_btn_bg_hcolor'=>'#fea075',
			'mwp_diet_submit_btn_tcolor'=>'#ffffff',
			'mwp_diet_submit_btn_thcolor'=>'#ffffff',
			'mwp_diet_result_block_color'=>'#e8eeee',
			'mwp_diet_result_active_color'=>'#59c4bc',
			'mwp_diet_result_attention_color'=>'#ea6582',
			'mwp_diet_radio_btn_general_color'=>'#59c4bc',
			'mwp_diet_radio_btn_general_secondary_color'=>'#ffffff',
			'mwp_diet_radio_btn_color'=>'#ff9966',
			'mwp_diet_radio_btn_secondary_color'=>'#ffffff',
			//text section
			'mwp_diet_form_heading_text'=>__('MWP Diet Calorie Calculator','mwp-diet'),
			'mwp_diet_submit_btn_text'=>__('Calculate','mwp-diet'),
			'mwp_diet_required_field_text'=>__('The field is required!','mwp-diet'),
			'mwp_diet_required_field_text_2'=>__('Please fill in this field!','mwp-diet'),
			'mwp_diet_bmr_popover_text'=>__('Basal metabolic rate (BMR) the number of calories you\'d burn if you stayed motionless for a day.','mwp-diet'),
			'mwp_diet_tdee_popover_text'=>__('Total Daily Energy Expenditure (TDEE), a measure of how many calories you burn per day.','mwp-diet'),
			'mwp_diet_lbm_popover_text'=>__('Lean body Mass (LBM) is a component of body composition, calculated by subtracting body fat weight from total body weight.','mwp-diet'),
			'mwp_diet_fbm_popover_text'=>__('Fat Body Mass (FBM) is a fat weight.','mwp-diet'),
			'mwp_diet_bmi_popover_text'=>__('Body Mass Index (BMI)  is a value derived from the mass (weight) and height of an individual.','mwp-diet'),
			'mwp_diet_wth_popover_text'=>__('Waist To Height (WTH)','mwp-diet'),
			'mwp_diet_mfm_popover_text'=>__('Maximum Fat Metabolism (MFM) number of calories you can eat below your TDEE without losing muscle mass.','mwp-diet'),
			'mwp_diet_mrdc_popover_text'=>__('Minimum Recommended Daily Calories (MRDC)','mwp-diet'),
			'mwp_diet_wcals_popover_text'=>__('Workout Calories','mwp-diet'),
			'mwp_diet_rcals_popover_text'=>__('Rest Calories','mwp-diet'),
			'mwp_diet_wtg_popover_text'=>__('Weeks To Goal','mwp-diet'),
			'mwp_diet_fw_popover_text'=>__('Final Weight','mwp-diet'),
			'mwp_diet_bmi_class_popover_text'=>__('Obesity class','mwp-diet'),
			'mwp_dc_nutritions_fat_popover_text'=>__('Fat','mwp-diet'),
			'mwp_dc_nutritions_carbs_popover_text'=>__('Carbohydrates','mwp-diet'),
			'mwp_dc_nutritions_protein_popover_text'=>__('Protein','mwp-diet'),		
			//sections control
			'mwp_diet_section_all' => 'enabled',
			'mwp_diet_section_bmr' => '',
			'mwp_diet_section_tdee' => '',
			'mwp_diet_section_lbm' => '',
			'mwp_diet_section_fbm' => '',
			'mwp_diet_section_bmi' => '',
			'mwp_diet_section_wth' => '',
			'mwp_diet_section_mfm' => '',
			'mwp_diet_section_mrdc' => '',
			'mwp_diet_section_wc' => '',
			'mwp_diet_section_rc' => '',
			'mwp_diet_section_wtg' => '',
			'mwp_diet_section_fw' => '',
			'mwp_diet_section_obesity' => '',
			'mwp_diet_section_fat' => '',
			'mwp_diet_section_carbs' => '',
			'mwp_diet_section_protein' => '',
			'mwp-diet-plugin-version' => '',
		);
		return apply_filters('mwp_dc_options_array', $mwp_diet_options_array);
	}
	
	/**
	 * Get sections array
	 * @since    1.0.3
	 */
	public static function get_google_fonts_array() {
		$google_fonts_array = array(                          
							"Abel" => "Abel",
							"Abril Fatface" => "Abril Fatface",
							"Aclonica" => "Aclonica",
							"Acme" => "Acme",
							"Actor" => "Actor",
							"Adamina" => "Adamina",
							"Advent Pro" => "Advent Pro",
							"Aguafina Script" => "Aguafina Script",
							"Aladin" => "Aladin",
							"Aldrich" => "Aldrich",
							"Alegreya" => "Alegreya",
							"Alegreya SC" => "Alegreya SC",
							"Alex Brush" => "Alex Brush",
							"Alfa Slab One" => "Alfa Slab One",
							"Alice" => "Alice",
							"Alike" => "Alike",
							"Alike Angular" => "Alike Angular",
							"Allan" => "Allan",
							"Allerta" => "Allerta",
							"Allerta Stencil" => "Allerta Stencil",
							"Allura" => "Allura",
							"Almendra" => "Almendra",
							"Almendra SC" => "Almendra SC",
							"Amaranth" => "Amaranth",
							"Amatic SC" => "Amatic SC",
							"Amethysta" => "Amethysta",
							"Andada" => "Andada",
							"Andika" => "Andika",
							"Angkor" => "Angkor",
							"Annie Use Your Telescope" => "Annie Use Your Telescope",
							"Anonymous Pro" => "Anonymous Pro",
							"Antic" => "Antic",
							"Antic Didone" => "Antic Didone",
							"Antic Slab" => "Antic Slab",
							"Anton" => "Anton",
							"Arapey" => "Arapey",
							"Arbutus" => "Arbutus",
							"Architects Daughter" => "Architects Daughter",
							"Arimo" => "Arimo",
							"Arizonia" => "Arizonia",
							"Armata" => "Armata",
							"Artifika" => "Artifika",
							"Arvo" => "Arvo",
							"Asap" => "Asap",
							"Asset" => "Asset",
							"Astloch" => "Astloch",
							"Asul" => "Asul",
							"Atomic Age" => "Atomic Age",
							"Aubrey" => "Aubrey",
							"Audiowide" => "Audiowide",
							"Average" => "Average",
							"Averia Gruesa Libre" => "Averia Gruesa Libre",
							"Averia Libre" => "Averia Libre",
							"Averia Sans Libre" => "Averia Sans Libre",
							"Averia Serif Libre" => "Averia Serif Libre",
							"Bad Script" => "Bad Script",
							"Balthazar" => "Balthazar",
							"Bangers" => "Bangers",
							"Basic" => "Basic",
							"Battambang" => "Battambang",
							"Baumans" => "Baumans",
							"Bayon" => "Bayon",
							"Belgrano" => "Belgrano",
							"Belleza" => "Belleza",
							"Bentham" => "Bentham",
							"Berkshire Swash" => "Berkshire Swash",
							"Bevan" => "Bevan",
							"Bigshot One" => "Bigshot One",
							"Bilbo" => "Bilbo",
							"Bilbo Swash Caps" => "Bilbo Swash Caps",
							"Bitter" => "Bitter",
							"Black Ops One" => "Black Ops One",
							"Bokor" => "Bokor",
							"Bonbon" => "Bonbon",
							"Boogaloo" => "Boogaloo",
							"Bowlby One" => "Bowlby One",
							"Bowlby One SC" => "Bowlby One SC",
							"Brawler" => "Brawler",
							"Bree Serif" => "Bree Serif",
							"Bubblegum Sans" => "Bubblegum Sans",
							"Buda" => "Buda",
							"Buenard" => "Buenard",
							"Butcherman" => "Butcherman",
							"Butterfly Kids" => "Butterfly Kids",
							"Cabin" => "Cabin",
							"Cabin Condensed" => "Cabin Condensed",
							"Cabin Sketch" => "Cabin Sketch",
							"Caesar Dressing" => "Caesar Dressing",
							"Cagliostro" => "Cagliostro",
							"Calligraffitti" => "Calligraffitti",
							"Cambo" => "Cambo",
							"Candal" => "Candal",
							"Cantarell" => "Cantarell",
							"Cantata One" => "Cantata One",
							"Cardo" => "Cardo",
							"Carme" => "Carme",
							"Carter One" => "Carter One",
							"Caudex" => "Caudex",
							"Cedarville Cursive" => "Cedarville Cursive",
							"Ceviche One" => "Ceviche One",
							"Changa One" => "Changa One",
							"Chango" => "Chango",
							"Chau Philomene One" => "Chau Philomene One",
							"Chelsea Market" => "Chelsea Market",
							"Chenla" => "Chenla",
							"Cherry Cream Soda" => "Cherry Cream Soda",
							"Chewy" => "Chewy",
							"Chicle" => "Chicle",
							"Chivo" => "Chivo",
							"Coda" => "Coda",
							"Coda Caption" => "Coda Caption",
							"Codystar" => "Codystar",
							"Comfortaa" => "Comfortaa",
							"Coming Soon" => "Coming Soon",
							"Concert One" => "Concert One",
							"Condiment" => "Condiment",
							"Content" => "Content",
							"Contrail One" => "Contrail One",
							"Convergence" => "Convergence",
							"Cookie" => "Cookie",
							"Copse" => "Copse",
							"Corben" => "Corben",
							"Cousine" => "Cousine",
							"Coustard" => "Coustard",
							"Covered By Your Grace" => "Covered By Your Grace",
							"Crafty Girls" => "Crafty Girls",
							"Creepster" => "Creepster",
							"Crete Round" => "Crete Round",
							"Crimson Text" => "Crimson Text",
							"Crushed" => "Crushed",
							"Cuprum" => "Cuprum",
							"Cutive" => "Cutive",
							"Damion" => "Damion",
							"Dancing Script" => "Dancing Script",
							"Dangrek" => "Dangrek",
							"Dawning of a New Day" => "Dawning of a New Day",
							"Days One" => "Days One",
							"Delius" => "Delius",
							"Delius Swash Caps" => "Delius Swash Caps",
							"Delius Unicase" => "Delius Unicase",
							"Della Respira" => "Della Respira",
							"Devonshire" => "Devonshire",
							"Didact Gothic" => "Didact Gothic",
							"Diplomata" => "Diplomata",
							"Diplomata SC" => "Diplomata SC",
							"Doppio One" => "Doppio One",
							"Dorsa" => "Dorsa",
							"Dosis" => "Dosis",
							"Dr Sugiyama" => "Dr Sugiyama",
							"Droid Sans" => "Droid Sans",
							"Droid Sans Mono" => "Droid Sans Mono",
							"Droid Serif" => "Droid Serif",
							"Duru Sans" => "Duru Sans",
							"Dynalight" => "Dynalight",
							"EB Garamond" => "EB Garamond",
							"Eater" => "Eater",
							"Economica" => "Economica",
							"Electrolize" => "Electrolize",
							"Emblema One" => "Emblema One",
							"Emilys Candy" => "Emilys Candy",
							"Engagement" => "Engagement",
							"Enriqueta" => "Enriqueta",
							"Erica One" => "Erica One",
							"Esteban" => "Esteban",
							"Euphoria Script" => "Euphoria Script",
							"Ewert" => "Ewert",
							"Exo" => "Exo",
							"Expletus Sans" => "Expletus Sans",
							"Fanwood Text" => "Fanwood Text",
							"Fascinate" => "Fascinate",
							"Fascinate Inline" => "Fascinate Inline",
							"Federant" => "Federant",
							"Federo" => "Federo",
							"Felipa" => "Felipa",
							"Fjord One" => "Fjord One",
							"Flamenco" => "Flamenco",
							"Flavors" => "Flavors",
							"Fondamento" => "Fondamento",
							"Fontdiner Swanky" => "Fontdiner Swanky",
							"Forum" => "Forum",
							"Francois One" => "Francois One",
							"Fredericka the Great" => "Fredericka the Great",
							"Fredoka One" => "Fredoka One",
							"Freehand" => "Freehand",
							"Fresca" => "Fresca",
							"Frijole" => "Frijole",
							"Fugaz One" => "Fugaz One",
							"GFS Didot" => "GFS Didot",
							"GFS Neohellenic" => "GFS Neohellenic",
							"Galdeano" => "Galdeano",
							"Gentium Basic" => "Gentium Basic",
							"Gentium Book Basic" => "Gentium Book Basic",
							"Geo" => "Geo",
							"Geostar" => "Geostar",
							"Geostar Fill" => "Geostar Fill",
							"Germania One" => "Germania One",
							"Give You Glory" => "Give You Glory",
							"Glass Antiqua" => "Glass Antiqua",
							"Glegoo" => "Glegoo",
							"Gloria Hallelujah" => "Gloria Hallelujah",
							"Goblin One" => "Goblin One",
							"Gochi Hand" => "Gochi Hand",
							"Gorditas" => "Gorditas",
							"Goudy Bookletter 1911" => "Goudy Bookletter 1911",
							"Graduate" => "Graduate",
							"Gravitas One" => "Gravitas One",
							"Great Vibes" => "Great Vibes",
							"Gruppo" => "Gruppo",
							"Gudea" => "Gudea",
							"Habibi" => "Habibi",
							"Hammersmith One" => "Hammersmith One",
							"Handlee" => "Handlee",
							"Hanuman" => "Hanuman",
							"Happy Monkey" => "Happy Monkey",
							"Henny Penny" => "Henny Penny",
							"Herr Von Muellerhoff" => "Herr Von Muellerhoff",
							"Holtwood One SC" => "Holtwood One SC",
							"Homemade Apple" => "Homemade Apple",
							"Homenaje" => "Homenaje",
							"IM Fell DW Pica" => "IM Fell DW Pica",
							"IM Fell DW Pica SC" => "IM Fell DW Pica SC",
							"IM Fell Double Pica" => "IM Fell Double Pica",
							"IM Fell Double Pica SC" => "IM Fell Double Pica SC",
							"IM Fell English" => "IM Fell English",
							"IM Fell English SC" => "IM Fell English SC",
							"IM Fell French Canon" => "IM Fell French Canon",
							"IM Fell French Canon SC" => "IM Fell French Canon SC",
							"IM Fell Great Primer" => "IM Fell Great Primer",
							"IM Fell Great Primer SC" => "IM Fell Great Primer SC",
							"Iceberg" => "Iceberg",
							"Iceland" => "Iceland",
							"Imprima" => "Imprima",
							"Inconsolata" => "Inconsolata",
							"Inder" => "Inder",
							"Indie Flower" => "Indie Flower",
							"Inika" => "Inika",
							"Irish Grover" => "Irish Grover",
							"Istok Web" => "Istok Web",
							"Italiana" => "Italiana",
							"Italianno" => "Italianno",
							"Jim Nightshade" => "Jim Nightshade",
							"Jockey One" => "Jockey One",
							"Jolly Lodger" => "Jolly Lodger",
							"Josefin Sans" => "Josefin Sans",
							"Josefin Slab" => "Josefin Slab",
							"Judson" => "Judson",
							"Julee" => "Julee",
							"Junge" => "Junge",
							"Jura" => "Jura",
							"Just Another Hand" => "Just Another Hand",
							"Just Me Again Down Here" => "Just Me Again Down Here",
							"Kameron" => "Kameron",
							"Karla" => "Karla",
							"Kaushan Script" => "Kaushan Script",
							"Kelly Slab" => "Kelly Slab",
							"Kenia" => "Kenia",
							"Khmer" => "Khmer",
							"Knewave" => "Knewave",
							"Kotta One" => "Kotta One",
							"Koulen" => "Koulen",
							"Kranky" => "Kranky",
							"Kreon" => "Kreon",
							"Kristi" => "Kristi",
							"Krona One" => "Krona One",
							"La Belle Aurore" => "La Belle Aurore",
							"Lancelot" => "Lancelot",
							"Lato" => "Lato",
							"League Script" => "League Script",
							"Leckerli One" => "Leckerli One",
							"Ledger" => "Ledger",
							"Lekton" => "Lekton",
							"Lemon" => "Lemon",
							"Lilita One" => "Lilita One",
							"Limelight" => "Limelight",
							"Linden Hill" => "Linden Hill",
							"Lobster" => "Lobster",
							"Lobster Two" => "Lobster Two",
							"Londrina Outline" => "Londrina Outline",
							"Londrina Shadow" => "Londrina Shadow",
							"Londrina Sketch" => "Londrina Sketch",
							"Londrina Solid" => "Londrina Solid",
							"Lora" => "Lora",
							"Love Ya Like A Sister" => "Love Ya Like A Sister",
							"Loved by the King" => "Loved by the King",
							"Lovers Quarrel" => "Lovers Quarrel",
							"Luckiest Guy" => "Luckiest Guy",
							"Lusitana" => "Lusitana",
							"Lustria" => "Lustria",
							"Macondo" => "Macondo",
							"Macondo Swash Caps" => "Macondo Swash Caps",
							"Magra" => "Magra",
							"Maiden Orange" => "Maiden Orange",
							"Mako" => "Mako",
							"Marck Script" => "Marck Script",
							"Marko One" => "Marko One",
							"Marmelad" => "Marmelad",
							"Marvel" => "Marvel",
							"Mate" => "Mate",
							"Mate SC" => "Mate SC",
							"Maven Pro" => "Maven Pro",
							"Meddon" => "Meddon",
							"MedievalSharp" => "MedievalSharp",
							"Medula One" => "Medula One",
							"Megrim" => "Megrim",
							"Merienda One" => "Merienda One",
							"Merriweather" => "Merriweather",
							"Metal" => "Metal",
							"Metamorphous" => "Metamorphous",
							"Metrophobic" => "Metrophobic",
							"Michroma" => "Michroma",
							"Miltonian" => "Miltonian",
							"Miltonian Tattoo" => "Miltonian Tattoo",
							"Miniver" => "Miniver",
							"Miss Fajardose" => "Miss Fajardose",
							"Modern Antiqua" => "Modern Antiqua",
							"Molengo" => "Molengo",
							"Monofett" => "Monofett",
							"Monoton" => "Monoton",
							"Monsieur La Doulaise" => "Monsieur La Doulaise",
							"Montaga" => "Montaga",
							"Montez" => "Montez",
							"Montserrat" => "Montserrat",
							"Moul" => "Moul",
							"Moulpali" => "Moulpali",
							"Mountains of Christmas" => "Mountains of Christmas",
							"Mr Bedfort" => "Mr Bedfort",
							"Mr Dafoe" => "Mr Dafoe",
							"Mr De Haviland" => "Mr De Haviland",
							"Mrs Saint Delafield" => "Mrs Saint Delafield",
							"Mrs Sheppards" => "Mrs Sheppards",
							"Muli" => "Muli",
							"Mystery Quest" => "Mystery Quest",
							"Neucha" => "Neucha",
							"Neuton" => "Neuton",
							"News Cycle" => "News Cycle",
							"Niconne" => "Niconne",
							"Nixie One" => "Nixie One",
							"Nobile" => "Nobile",
							"Nokora" => "Nokora",
							"Norican" => "Norican",
							"Nosifer" => "Nosifer",
							"Nothing You Could Do" => "Nothing You Could Do",
							"Noticia Text" => "Noticia Text",
							"Nova Cut" => "Nova Cut",
							"Nova Flat" => "Nova Flat",
							"Nova Mono" => "Nova Mono",
							"Nova Oval" => "Nova Oval",
							"Nova Round" => "Nova Round",
							"Nova Script" => "Nova Script",
							"Nova Slim" => "Nova Slim",
							"Nova Square" => "Nova Square",
							"Numans" => "Numans",
							"Nunito" => "Nunito",
							"Odor Mean Chey" => "Odor Mean Chey",
							"Old Standard TT" => "Old Standard TT",
							"Oldenburg" => "Oldenburg",
							"Oleo Script" => "Oleo Script",
							"Open Sans" => "Open Sans",
							"Open Sans Condensed" => "Open Sans Condensed",
							"Orbitron" => "Orbitron",
							"Original Surfer" => "Original Surfer",
							"Oswald" => "Oswald",
							"Over the Rainbow" => "Over the Rainbow",
							"Overlock" => "Overlock",
							"Overlock SC" => "Overlock SC",
							"Ovo" => "Ovo",
							"Oxygen" => "Oxygen",
							"PT Mono" => "PT Mono",
							"PT Sans" => "PT Sans",
							"PT Sans Caption" => "PT Sans Caption",
							"PT Sans Narrow" => "PT Sans Narrow",
							"PT Serif" => "PT Serif",
							"PT Serif Caption" => "PT Serif Caption",
							"Pacifico" => "Pacifico",
							"Parisienne" => "Parisienne",
							"Passero One" => "Passero One",
							"Passion One" => "Passion One",
							"Patrick Hand" => "Patrick Hand",
							"Patua One" => "Patua One",
							"Paytone One" => "Paytone One",
							"Permanent Marker" => "Permanent Marker",
							"Petrona" => "Petrona",
							"Philosopher" => "Philosopher",
							"Piedra" => "Piedra",
							"Pinyon Script" => "Pinyon Script",
							"Plaster" => "Plaster",
							"Play" => "Play",
							"Playball" => "Playball",
							"Playfair Display" => "Playfair Display",
							"Podkova" => "Podkova",
							"Poiret One" => "Poiret One",
							"Poller One" => "Poller One",
							"Poly" => "Poly",
							"Pompiere" => "Pompiere",
							"Pontano Sans" => "Pontano Sans",
							"Port Lligat Sans" => "Port Lligat Sans",
							"Port Lligat Slab" => "Port Lligat Slab",
							"Prata" => "Prata",
							"Preahvihear" => "Preahvihear",
							"Press Start 2P" => "Press Start 2P",
							"Princess Sofia" => "Princess Sofia",
							"Prociono" => "Prociono",
							"Prosto One" => "Prosto One",
							"Puritan" => "Puritan",
							"Quantico" => "Quantico",
							"Quattrocento" => "Quattrocento",
							"Quattrocento Sans" => "Quattrocento Sans",
							"Questrial" => "Questrial",
							"Quicksand" => "Quicksand",
							"Qwigley" => "Qwigley",
							"Radley" => "Radley",
							"Raleway" => "Raleway",
							"Rammetto One" => "Rammetto One",
							"Rancho" => "Rancho",
							"Rationale" => "Rationale",
							"Redressed" => "Redressed",
							"Reenie Beanie" => "Reenie Beanie",
							"Revalia" => "Revalia",
							"Ribeye" => "Ribeye",
							"Ribeye Marrow" => "Ribeye Marrow",
							"Righteous" => "Righteous",
							"Rochester" => "Rochester",
							"Rock Salt" => "Rock Salt",
							"Rokkitt" => "Rokkitt",
							"Ropa Sans" => "Ropa Sans",
							"Rosario" => "Rosario",
							"Rosarivo" => "Rosarivo",
							"Rouge Script" => "Rouge Script",
							"Ruda" => "Ruda",
							"Ruge Boogie" => "Ruge Boogie",
							"Ruluko" => "Ruluko",
							"Ruslan Display" => "Ruslan Display",
							"Russo One" => "Russo One",
							"Ruthie" => "Ruthie",
							"Sail" => "Sail",
							"Salsa" => "Salsa",
							"Sancreek" => "Sancreek",
							"Sansita One" => "Sansita One",
							"Sarina" => "Sarina",
							"Satisfy" => "Satisfy",
							"Schoolbell" => "Schoolbell",
							"Seaweed Script" => "Seaweed Script",
							"Sevillana" => "Sevillana",
							"Shadows Into Light" => "Shadows Into Light",
							"Shadows Into Light Two" => "Shadows Into Light Two",
							"Shanti" => "Shanti",
							"Share" => "Share",
							"Shojumaru" => "Shojumaru",
							"Short Stack" => "Short Stack",
							"Siemreap" => "Siemreap",
							"Sigmar One" => "Sigmar One",
							"Signika" => "Signika",
							"Signika Negative" => "Signika Negative",
							"Simonetta" => "Simonetta",
							"Sirin Stencil" => "Sirin Stencil",
							"Six Caps" => "Six Caps",
							"Slackey" => "Slackey",
							"Smokum" => "Smokum",
							"Smythe" => "Smythe",
							"Sniglet" => "Sniglet",
							"Snippet" => "Snippet",
							"Sofia" => "Sofia",
							"Sonsie One" => "Sonsie One",
							"Sorts Mill Goudy" => "Sorts Mill Goudy",
							"Special Elite" => "Special Elite",
							"Spicy Rice" => "Spicy Rice",
							"Spinnaker" => "Spinnaker",
							"Spirax" => "Spirax",
							"Squada One" => "Squada One",
							"Stardos Stencil" => "Stardos Stencil",
							"Stint Ultra Condensed" => "Stint Ultra Condensed",
							"Stint Ultra Expanded" => "Stint Ultra Expanded",
							"Stoke" => "Stoke",
							"Sue Ellen Francisco" => "Sue Ellen Francisco",
							"Sunshiney" => "Sunshiney",
							"Supermercado One" => "Supermercado One",
							"Suwannaphum" => "Suwannaphum",
							"Swanky and Moo Moo" => "Swanky and Moo Moo",
							"Syncopate" => "Syncopate",
							"Tangerine" => "Tangerine",
							"Taprom" => "Taprom",
							"Telex" => "Telex",
							"Tenor Sans" => "Tenor Sans",
							"The Girl Next Door" => "The Girl Next Door",
							"Tienne" => "Tienne",
							"Tinos" => "Tinos",
							"Titan One" => "Titan One",
							"Trade Winds" => "Trade Winds",
							"Trocchi" => "Trocchi",
							"Trochut" => "Trochut",
							"Trykker" => "Trykker",
							"Tulpen One" => "Tulpen One",
							"Ubuntu" => "Ubuntu",
							"Ubuntu Condensed" => "Ubuntu Condensed",
							"Ubuntu Mono" => "Ubuntu Mono",
							"Ultra" => "Ultra",
							"Uncial Antiqua" => "Uncial Antiqua",
							"UnifrakturCook" => "UnifrakturCook",
							"UnifrakturMaguntia" => "UnifrakturMaguntia",
							"Unkempt" => "Unkempt",
							"Unlock" => "Unlock",
							"Unna" => "Unna",
							"VT323" => "VT323",
							"Varela" => "Varela",
							"Varela Round" => "Varela Round",
							"Vast Shadow" => "Vast Shadow",
							"Vibur" => "Vibur",
							"Vidaloka" => "Vidaloka",
							"Viga" => "Viga",
							"Voces" => "Voces",
							"Volkhov" => "Volkhov",
							"Vollkorn" => "Vollkorn",
							"Voltaire" => "Voltaire",
							"Waiting for the Sunrise" => "Waiting for the Sunrise",
							"Wallpoet" => "Wallpoet",
							"Walter Turncoat" => "Walter Turncoat",
							"Wellfleet" => "Wellfleet",
							"Wire One" => "Wire One",
							"Yanone Kaffeesatz" => "Yanone Kaffeesatz",
							"Yellowtail" => "Yellowtail",
							"Yeseva One" => "Yeseva One",
							"Yesteryear" => "Yesteryear",
							"Zeyada" => "Zeyada",
					); 
		return $google_fonts_array;
	}
}
