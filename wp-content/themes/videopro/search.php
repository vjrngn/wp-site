<?php get_header(); ?>

	<div id="content">
			<?php get_template_part('includes/breadcrumbs'); ?>
	    <?php $count = 1; ?>
		<?php if (have_posts()) : while ( have_posts() ) : the_post() ?>
			<?php include('includes/loop.php'); ?>
	        <?php
	            if($count%3 == 0){
	         ?>
	            <div class="clear"></div>
	        <?php
	            };
	            $count++;
	        ?>
		<?php endwhile; ?>
		<div class="clear"></div>
			<div class="pagination">
				<?php pagination( '&laquo; prev', 'next &raquo;') ?>
			</div><!-- .pagination -->
		<?php else : ?>
			<?php get_template_part('includes/not-found'); ?>
		<?php endif; ?>
	</div><!-- #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
