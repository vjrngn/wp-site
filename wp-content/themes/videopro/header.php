<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes( 'xhtml' ); ?>>
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php tj_custom_titles(); ?></title>
<?php tj_custom_description(); ?>
<?php tj_custom_keywords(); ?>
<?php tj_custom_canonical(); ?>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/colors/<?php echo get_option('videopro_theme_stylesheet');?>" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/custom.css" />
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:700|Open+Sans' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/includes/fancybox/jquery.fancybox.css" />

<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

<div id="wrapper">
	<div class="header-ad">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Header -->
<ins class="adsbygoogle"
     style="display:block;min-width:320px;max-width:970px;width:100%;height:100px"
     data-ad-client="ca-pub-8065322271274833"
     data-ad-slot="6552041129"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
		</div><!-- .header-ad -->
<div id="top">

<a href="http://hasfit.com"><img class="alignleft wp-image-9914" src="http://hasfit.com/wp-content/uploads/2016/03/hasfit-small.jpg" alt="hasfit-small" width="140" height="47" style="margin:0"/></a>

<a href="https://www.pinterest.com/hasfitness/" rel="attachment wp-att-9924"><img src="http://hasfit.com/wp-content/uploads/2016/04/social-icon-pinterest.jpg" alt="hasfit pinterest" width="36" height="31" style="margin:0" class="alignright size-full wp-image-9924" /></a>

<a href="https://twitter.com/HeartSoulFit" rel="attachment wp-att-9925"><img src="http://hasfit.com/wp-content/uploads/2016/04/social-icon-twitter.jpg" alt="hasfit twitter" width="36" height="31" style="margin:0" class="alignright size-full wp-image-9925" /></a>

<a href="https://www.facebook.com/HASFitness/" rel="attachment wp-att-9922"><img src="http://hasfit.com/wp-content/uploads/2016/04/social-icon-facebook.jpg" alt="hasfit facebook" width="36" height="31" style="margin:0" class="alignright size-full wp-image-9922" /></a>

<a href="https://www.instagram.com/hasfit_official/" rel="attachment wp-att-9923"><img src="http://hasfit.com/wp-content/uploads/2016/04/social-icon-instagram.jpg" alt="hasfit instagram" width="36" height="31" style="margin:0" class="alignright size-full wp-image-9923" /></a>

<a href="https://www.youtube.com/c/hasfit" rel="attachment wp-att-9926"><img src="http://hasfit.com/wp-content/uploads/2016/04/social-icon-youtube.jpg" alt="hasfit youtube" width="36" height="31" style="margin:0" class="alignright size-full wp-image-9926" /></a>
	
	</div><!-- #top -->

	
	<header>
	    

		    <div id="primary-nav">
			    <?php $menuClass = 'nav';
				$menuID = 'primary-navigation';
				$primaryNav = '';
				if (function_exists('wp_nav_menu')) {
					$primaryNav = wp_nav_menu( array( 'theme_location' => 'primary-nav', 'container' => '', 'fallback_cb' => '', 'menu_class' => $menuClass, 'menu_id' => $menuID, 'echo' => false ) );
				};
				if ($primaryNav == '') { ?>
					<ul id="<?php echo $menuID; ?>" class="<?php echo $menuClass; ?>">
						<?php if (get_option('videopro_home_link') == 'on') { ?>
							<li class="<?php if(!is_page()) echo('first');?> home-item"><a href="<?php echo home_url(); ?>"><?php _e('Home', 'themejunkie') ?></a></li>
						<?php } ?>						
						<?php show_page_menu($menuClass,false,false); ?>
					</ul>
				<?php }	else echo($primaryNav); ?>
			</div><!-- #primary-nav -->

		<div class="clear">
	

    </header><!-- header-->
    
    <div class="clear"></div>
    
 
		
	
    