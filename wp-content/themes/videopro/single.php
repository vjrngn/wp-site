<?php get_header(); ?>

<div id="content"> 


    <?php get_sidebar(); ?>

    <article class="has-sidebar">
    
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	   	<div class="entry-content">
	   
            <?php
                $embed = get_post_meta(get_the_ID(), 'tj_video_embed', TRUE);
                if($embed) {
            ?>
<div class="video-container">
                <div class="entry-embed">
                	<?php echo stripslashes(htmlspecialchars_decode($embed));?>
                </div><!-- .entry-embed -->
</div><!-- video-container -->
            <?php
                }
            ?>
            


		    <div class="entry-meta">
		  		<?php _e('Posted on', 'themejunkie'); ?> <?php the_time('M j, Y'); ?> <?php _e('at', 'themejunkie'); ?> <?php the_time(); ?> &middot; <?php _e('by', 'themejunkie'); ?> <?php the_author_posts_link(); ?>
		        <span class="entry-comment">
		            <?php comments_popup_link( __( '0', 'theme junkie' ), __( '1', 'theme junkie' ), __( '%', 'theme junkie' ) ); ?>
		        </span>
		    </div><!-- .entry-meta -->
		    
	        <h1 class="entry-title"><?php the_title(); ?></h1>
	        
			<?php if(get_option('videopro_integrate_singletop_enable') == 'on') echo (get_option('videopro_integration_single_top')); ?>
			
            <?php the_content(); ?>
            
<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'themejunkie' ), 'after' => '</div>' ) ); ?>	
			
			<?php if(get_option('videopro_integrate_singlebottom_enable') == 'on') echo (get_option('videopro_integration_single_bottom')); ?>
<center>			
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Single Post in Entry Content -->
<ins class="adsbygoogle"
     style="display:inline-block;width:336px;height:280px"
     data-ad-client="ca-pub-8065322271274833"
     data-ad-slot="6291470728"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</center>
<!-- Facebook Comments Shortcode -->
<?php
echo do_shortcode('[fbcomments]');
?>

	    	<div class="clear"></div>
	    	
			<?php edit_post_link('('.__('Edit', 'themejunkie').')', '<span class="entry-edit">', '</span>'); ?>
		
	  	</div><!-- .entry-content -->

		<div class="clear"></div>
		
		<?php if(get_option('videopro_show_author_box') == 'on') { ?> 
		
			<div class="entry-author-box clear">
			
				<div class="author-avatar">
					<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'themejunkie_author_bio_avatar_size', 60 ) ); ?>
				</div> <!-- .author-avatar -->
				
				<div class="author-description">
					<h3><?php the_author(); ?></h3>
					<?php the_author_meta( 'description' ); ?>
				</div> <!-- .author-description -->
				
				<div id="author-link">
					<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" title="<?php printf( esc_attr__( 'View all posts by %s', 'theme junkie' ), get_the_author() ); ?>"><?php _e( 'View all posts by ', 'theme junkie' ); ?><?php the_author(); ?> &rarr;</a>
				</div> <!-- .author-link -->
						
			</div><!-- .entry-author-box -->
			
		<?php } ?>
	
    	<?php if(get_option('videopro_show_post_comments') == 'on') { ?>
	  		<?php// comments_template(); ?>  	
	  	<?php } ?>

	<?php endwhile; else: ?>
	<?php endif; ?>
	
    </article><!-- article -->

    <div id="right-sidebar">
    
	    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
		<?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('right-sidebar-posts') ): ?>
	        <?php endif; ?>	

	        <section class="entry-category">
	    		<h3><?php _e('Filed under:', 'themejunkie'); ?></h3><?php the_category(' '); ?>
	    	</section><!-- .entry-category -->
	
	        <?php the_tags( '<section class="entry-tags"><h3>Tagged with:</h3>', ' ', '</section>'); ?>

		<?php endwhile; else: ?>
		<?php endif; ?>
    </div><!-- #right-sidebar -->  

</div> <!-- #content -->
<?php get_footer(); ?>