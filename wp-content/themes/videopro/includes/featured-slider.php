<div id="featured-slider" >
	<ul class="ui-tabs-nav">
		<?php
			$featured = get_option('videopro_featured_slider_tags');
		?>
		<?php
			query_posts( array(
				'tag' => $featured,
				'posts_per_page' => 3
				)
			);
		?>
		<?php if (have_posts()) : while ( have_posts() ) : the_post() ?>
		
			<li class="ui-tabs-nav-item ui-tabs-selected" id="nav-post-<?php the_ID(); ?>">
				<a href="#post-<?php the_ID(); ?>">
			        <?php if(has_post_thumbnail()){?>
			        	<?php the_post_thumbnail('featured-thumb-small', array('class' => 'entry-thumb')); ?>
			        <?php } else { ?>
			        <?php $img_url = get_post_meta(get_the_ID(), 'tj_video_img_url', TRUE); ?>
			            <?php if($img_url != null) { ?><img src="<?php echo $img_url; ?>" alt="<?php the_title(); ?>" class="entry-thumb"/><?php } ?>
			        <?php } ?>					
				</a>
				<h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
				<div class="entry-meta">
					<?php the_time('M j, Y'); ?> / <a href="<?php the_permalink(); ?>" rel="bookmark"><?php _e('Read more','themejunkie'); ?> &raquo;</a>
				</div><!-- .entry-meta -->
			</li>
		
		<?php endwhile; ?>
		<?php endif; ?>
		<?php wp_reset_query();?>
	</ul>
	
	<?php
		query_posts( array(
			'tag' => $featured,
			'posts_per_page' => 4
			)
		);
	?>
	<?php if (have_posts()) : while ( have_posts() ) : the_post() ?>

		<div id="post-<?php the_ID(); ?>" class="ui-tabs-panel ui-tabs-hide">
		
			<?php
				$embed = get_post_meta(get_the_ID(), 'tj_video_embed', TRUE);
				if ($embed != null) {
			?> 
			
				<div class="entry-embed">
					<?php echo stripslashes(htmlspecialchars_decode($embed)); ?>
				</div><!-- .entry-embed -->
			
			<?php 
				} else {
			?>
			
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail('featured-thumb-large', array('class' => 'entry-thumb')); ?></a>
				<div class="slide-caption" >
					<h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
					<div class="entry-excerpt">
						<?php tj_content_limit('180'); ?>
					</div><!-- .entry-excerpt -->
				</div><!-- .slide-caption -->
			
			<?php       
				}
			?>
		</div><!-- #post-<?php the_id(); ?> .ui-tabs-panel .ui-tabs-hide-->

	<?php endwhile; ?>
	<?php endif; ?>
	<?php wp_reset_query();?>

</div><!-- #featured-slider -->