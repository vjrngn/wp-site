/* Begin */
jQuery(document).ready(function(){
    
/* =Preload
----------------------------------------------- */
    function slider_init(){
        jQuery("#featured-slider").fadeIn(300);
    }
    addLoadEvent(slider_init);
    
    function addLoadEvent(func){
        var oldonload = window.onload;
        if(typeof window.onload != 'function'){
            window.onload = func;
        }else{
            window.onload = function(){
                oldonload();
                func();
            }
        }
    }
    
	$("#featured-slider > ul").tabs({fx:{opacity: 'toggle', duration: 'fast'}}).tabs("rotate", 5000000, true);   
   
	if(!$('.pagination .prev').length) $('.pagination').append('<span class="prev page-numbers inactive"></span>');
	if(!$('.pagination .next').length) $('.pagination').append('<span class="next page-numbers inactive"></span>');
	$('.pagination .page-numbers').not('.prev,.next,:last').after('<span class="seperator"></span>');
	$('.pagination .seperator').last().remove();
	
/* =Correct CSS
----------------------------------------------- */
    function correct_css(){
        jQuery('embed').each(function(){
            jQuery(this).attr('wmode','opaque');
        });
        
        $('.entry-list').last().css('border-bottom','0').css('padding-bottom','0');
        
    }
    correct_css();
    
/* =jQuery Superfish Menu
----------------------------------------------- */
    function init_nav(){
        jQuery('ul.nav').superfish({ 
	        delay:       100,                             // one second delay on mouse out 
	        animation:   {opacity:'show',height:'show'},  // fade-in and slide-down animation 
	        speed:       'fast'                           // faster animation speed 
    	});
    }
    init_nav();


/* =Iframe Correct
----------------------------------------------- */
    function iframe_correct(){
        jQuery(".entry-embed iframe").each(function(){
            var ifr_source = jQuery(this).attr('src');
            var wmode = "wmode=transparent";
            if(ifr_source.indexOf('?') != -1) {
                var getQString = ifr_source.split('?');
                var oldString = getQString[1];
                var newString = getQString[0];
                jQuery(this).attr('src',newString+'?'+wmode+'&'+oldString);
            }else{
                jQuery(this).attr('src',ifr_source+'?'+wmode);
            }
        });
    }
    iframe_correct();

/* =Fancy Box
----------------------------------------------- */
    function fancybox_init(){
        jQuery(".various").fancybox({
		    maxWidth	: 573,
		    maxHeight	: 325,
		    fitToView	: false,
		    width		: '70%',
		    height		: '70%',
		    autoSize	: false,
		    closeClick	: false,
		    openEffect	: 'none',
		    closeEffect	: 'none',
            arrows: false
	    });
    }
    fancybox_init();
    
});
