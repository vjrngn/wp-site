<?php get_header(); ?>

	<div id="content">
	    <article>
		<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<h1 class="page-title"><?php the_title(); ?></h1>
			<div class="entry-content">
				<?php the_content(''); ?>
				<div class="pagenumbers">
				<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'themejunkie' ), 'after' => '</div>' ) ); ?>
				<?php edit_post_link('('.__('Edit', 'themejunkie').')', '<span class="entry-edit">', '</span>'); ?>
				<div class="clear"></div>
	  		</div><!-- .entry-content -->
	    	<?php if(get_option('videopro_show_page_comments') == 'on') { ?>
		  		<?php// comments_template(); ?>
		  	<?php } ?>  		
		<?php endwhile; ?>
		<?php else : ?>
		<?php endif; ?>
	    </article><!-- article -->
	</div> <!-- #content -->
	
<?php get_sidebar(); ?>
<?php get_footer(); ?>