	<div class="clear"></div>
		        
		<footer>
		
			<?php if ( is_active_sidebar( 'footer-widget-area-1' ) || is_active_sidebar( 'footer-widget-area-2' ) || is_active_sidebar( 'footer-widget-area-3' ) || is_active_sidebar( 'footer-widget-area-4' )) { ?>
				
					<div class="footer-wrap">
					
						<div id="footer-widget-1">
							<?php if ( is_active_sidebar( 'footer-widget-area-1' ) ) :  dynamic_sidebar( 'footer-widget-area-1'); endif; ?>
						</div>
				
						<div id="footer-widget-2">
							<?php if ( is_active_sidebar( 'footer-widget-area-2' ) ) :  dynamic_sidebar( 'footer-widget-area-2'); endif; ?>
						</div>
						
						<div id="footer-widget-3">
							<?php if ( is_active_sidebar( 'footer-widget-area-3' ) ) :  dynamic_sidebar( 'footer-widget-area-3'); endif; ?>
						</div>
					
						<div id="footer-widget-4">				
							<?php if ( is_active_sidebar( 'footer-widget-area-4' ) ) :  dynamic_sidebar( 'footer-widget-area-4'); endif; ?>
						</div>
				
						<div class="clear"></div>
						
					</div><!-- .footer-wrap -->
		
			<?php } ?>
				
		
					
		</footer><!-- footer -->

	</div><!-- #wrapper -->
			
	<?php wp_footer(); ?>
	
</body>
</html>