<div id="sidebar">
	
    
	<?php if(is_page() || is_archive() || is_search() ):?>

	    <?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('left-sidebar-pages') ) :?>
	    	
	    	</div><!-- .widget .widget_text -->
	    <?php endif; ?>

	<?php elseif(is_single()) : ?>

	    <?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('left-sidebar-posts') ): ?>
	    
	    	</div><!-- .widget .widget_text -->
	    <?php endif; ?>

	<?php else :?>

	    <?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('left-sidebar-home') ): ?>
	    	
	    <?php endif; ?>

	<?php endif; ?>
	
</div><!-- #sidebar -->
