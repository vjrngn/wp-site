<?php //get_header(); ?>


			
	    <?php $count = 1; ?>
		<?php if ($query->have_posts()) : while ( $query->have_posts() ) : $query->the_post() ?>
			<?php  include(locate_template('includes/loop.php')); //get_template_part('includes/loop'); ?>
	        <?php

	            if($count%3 == 0){
	         ?>
	            <div class="clear"></div>
	        <?php
	            };
	            $count++;
	        ?>
		<?php endwhile; ?>
		<div class="clear"></div>
			<div class="pagination">
				<?php pagination( '&laquo; prev', 'next &raquo;',$query) ?>
			</div><!-- .pagination -->
		<?php else : ?>
			<?php get_template_part('includes/not-found'); ?>
		<?php endif; ?>

<?php //get_sidebar(); ?>
<?php //get_footer(); ?>
